<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 3.0
*/
namespace App\Http\Controllers\Web;
use App\User;
use Socialite;
//use Mail;
//validator is builtin class in laravel
use Validator;
use Services;
use File; 

use Illuminate\Contracts\Auth\Authenticatable;

use DB;
//for password encryption or hash protected
use Hash;

//for authenitcate login data
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;


//for requesting a value 
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
//for Carbon a value 
use Carbon;
use Illuminate\Support\Facades\Redirect;
use Session;
use Lang;

//email
use Illuminate\Support\Facades\Mail;

class VendorsController extends DataController
{
	//signup 
	public function signup(Request $request){	
		if(auth()->guard('vendor')->check()){
			return redirect('/');
		}
		else{
			$title = array('pageTitle' => Lang::get("website.Vendor Sign Up"));
			$result = array();						
			$result['commonContent'] = $this->commonContent();		
			return view("vendor-signup", $title)->with('result', $result);   
		} 			
	}

	//login 
	public function login(Request $request){	
		if(auth()->guard('vendor')->check()){
			return redirect('/');
		}
		else{
			
			$title = array('pageTitle' => Lang::get("website.Vendor Login"));
			$result = array();				
			$result['commonContent'] = $this->commonContent();		
			return view("vendor-login", $title)->with('result', $result);   
		} 		
				
	}

	//login
	public function processLogin(Request $request){		
		$old_session = Session::getId();		
		$result = array();		
		
		$validator = Validator::make(
			array(
					'email'    => $request->email,
					'password' => $request->password
				), 
			array(
					'email'    => 'required | email',
					'password' => 'required',
				)
		);
		
		
		//validate 
		if($validator->fails()){
			return redirect('vendor-login')->withErrors($validator)->withInput();
		}else{
			//check authentication of email and password
			$vendorInfo = array("email" => $request->email, "password" => $request->password);
			
			if(auth()->guard('vendor')->attempt($vendorInfo)) {
				$vendor = auth()->guard('vendor')->user();
				
				//set session				
				session(['vendor_id' => $vendor->vendor_id]);
				
				//insert device id
				if(!empty(session('device_id'))){					
					DB::table('devices')->where('device_id', session('device_id'))->update(['vendor_id'	=>	$vendor->vendor_id]);		
				}
						
				$result['vendors'] = DB::table('vendors')->where('vendor_id', $vendor->vendor_id)->get();					
				// return redirect()->intended('/vendor/dashboard')->with('result', $result);
				return view('vendor.dashboard');
			}else{
				return redirect('vendor-login')->with('loginError',Lang::get("website.Email or password is incorrect"));
			}
		}
	}

    public function signupProcess(Request $request){
		$old_session = Session::getId();
		
		$firstName = $request->firstName;
		$lastName = $request->lastName;
		$gender = $request->gender;
		$email = $request->email;
		$password = $request->password;
		//$token = $request->token;
		$date = date('y-md h:i:s');
		
		$extensions = array('gif','jpg','jpeg','png');
		if($request->hasFile('picture') and in_array($request->picture->extension(), $extensions)){
			$image = $request->picture;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/assets/images/user_profile/', $fileName);
			$profile_photo = 'resources/assets/images/user_profile/'.$fileName; 
		}	else{
			$profile_photo = 'resources/assets/images/user_profile/default_user.png';
		}	
		
//		//validation start
		$validator = Validator::make(
			array(
				'firstName' => $request->firstName,
				'lastName' => $request->lastName,
				'vendors_gender' => $request->gender,
				'email' => $request->email,
				'password' => $request->password,
				'company' => $request->company,
				'address' => $request->address,
				'city' => $request->city,
				'state' => $request->state,
				'country' => $request->country,
				'zip' => $request->zip
				//'re_password' => $request->re_password,
				
			),array(
				'firstName' => 'required ',
				'lastName'  => 'required',
				'vendors_gender' 	=> 'required',
				'email' 	=> 'required | email',
				'password'  => 'required',
				'company' =>'required',
				'address' => 'required',
				'city' => 'required',
				'state' => 'required',
				'country' => 'required',
				'zip' => 'required'
				//'re_password' => 'required | same:password',
			)
		);
		if($validator->fails()){
			return redirect('vendor-signup')->withErrors($validator)->withInput();
		}else{
			
			//echo "Value is completed";
			$data = array(
				'vendors_firstname' => $request->firstName,
				'vendors_lastname'  => $request->lastName,
				'vendors_gender' => $request->gender,
				'email' => $request->email,
				'password' => Hash::make($password),
				'vendors_picture' => $profile_photo,
				'company' => $request->company,
				'address' => $request->address,
				'city' => $request->city,
				'state' => $request->state,
				'country' => $request->country,
				'zip' => $request->zip,
				'created_at' => $date,
				'updated_at' => $date,
			);	
			
			
			//eheck email already exit
			$vendor_email = DB::table('vendors')->select('email')->where('email', $email)->get();	
			if(count($vendor_email)>0){
				return redirect('/vendor- ')->withInput($request->input())->with('error', Lang::get("website.Email already exist"));
			}else{
				if(DB::table('vendors')->insert($data)){					
					
					//check authentication of email and password
					$vendorInfo = array("email" => $request->email, "password" => $request->password);
										
					if(auth()->guard('vendor')->attempt($vendorInfo)) {
						$vendor = auth()->guard('vendor')->user();
						
						//set session
						session(['vendors_id' => $vendor->vendors_id]);

						//insert device id
						if(!empty(session('device_id'))){					
							DB::table('devices')->where('device_id', session('device_id'))->update(['vendors_id'	=>	$vendor->vendors_id]);		
						}
						
						$vendors = DB::table('vendors')->where('vendors_id', $vendor->vendors_id)->get();
						$result['vendors'] = $vendors;
						//email and notification			
						$myVar = new AlertController();
						$alertSetting = $myVar->createUserAlert($vendors);
						
						return redirect()->intended('/')->with('result', $result);
					}else{
						return redirect('vendor-login')->with('loginError', Lang::get("website.Email or password is incorrect"));
					}

					
				}else{
					return redirect('/vendor-signup')->with('error', Lang::get("website.something is wrong"));
				}
			}		
			
		}
	}

	//logout
	public function logout(REQUEST $request){
		Auth::logout();
		Auth::guard('vendor')->logout();
		session()->flush();
		$request->session()->forget('vendor_id');
		$request->session()->regenerate();		
		return redirect()->intended('/');
	}

	// public function profile(Request $request,$id) {
	// 	return redirect("vendor-profile", $id)->with('id', $id);   
	// }

	public function profile(Request $request) {
		$title = array('pageTitle' => Lang::get("website.Vendor Detail"));
		$result = array();						
		$result['commonContent'] = $this->commonContent();	
		$vendor = DB::table('vendors')->where('vendor_id', $request->id)->first();
		$products = DB::table('products')->where('vendor_id', $request->id)->get();
		$brands = DB::table('manufacturers')->where('vendor_id', $request->id)->get();
		$result['vendor'] = $vendor;
		$result['detail']['products'] = $products;
		$result['detail']['brands'] = $brands;
		return view("vendor-profile", $title)->with('result', $result);   
	}
}