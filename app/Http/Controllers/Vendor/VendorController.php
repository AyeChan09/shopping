<?php

/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 1.0
*/

namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;
use App;
use Lang;

// use App\Vendor;

use DB;
//for password encryption or hash protected
use Hash;
//use App\Vendor;

//for authenitcate login data
use Auth;
use Session;
//for requesting a value 
use Illuminate\Http\Request;


class VendorController extends Controller
{
	public function dashboards(Request $request){
		$title 			  = 	array('pageTitle' => Lang::get("ven.title_dashboard"));
		$language_id      = 	'1';
		$result 		  =		array();
		
		$reportBase		  = 	$request->reportBase;
		
		//recently order placed
		$orders = DB::table('orders')
			->LeftJoin('currencies', 'currencies.code', '=', 'orders.currency')
			->orderBy('date_purchased','DESC')
			->get();
		
			
		
		$index = 0;
		$total_price = array();
		foreach($orders as $orders_data){
			$orders_products = DB::table('orders_products')
				->select('final_price', DB::raw('SUM(final_price) as total_price'))
				->where('orders_id', '=' ,$orders_data->orders_id)
				->groupBy('final_price')
				->get();
				
			$orders[$index]->total_price = $orders_products[0]->total_price;
			
			$orders_status_history = DB::table('orders_status_history')
				->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
				->select('orders_status.orders_status_name', 'orders_status.orders_status_id')
				->where('orders_id', '=', $orders_data->orders_id)->orderby('orders_status_history.date_added', 'DESC')->limit(1)->get();
				
			$orders[$index]->orders_status_id = $orders_status_history[0]->orders_status_id;
			$orders[$index]->orders_status = $orders_status_history[0]->orders_status_name;
			
			$index++;				
		}
		
		$compeleted_orders = 0;
		$pending_orders = 0;
		foreach($orders as $orders_data){
			
			if($orders_data->orders_status_id=='2')
			{
				$compeleted_orders++;
			}
			if($orders_data->orders_status_id=='1')
			{
				$pending_orders++;
			}
		}
		
		$result['orders'] = $orders->chunk(10);
		$result['pending_orders'] = $pending_orders;
		$result['compeleted_orders'] = $compeleted_orders;
		$result['total_orders'] = count($orders);
		
		$result['inprocess'] = count($orders)-$pending_orders-$compeleted_orders;
		
		//Rencently added products
		$recentProducts = DB::table('products')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->where('products_description.language_id','=', $language_id)
			->orderBy('products.products_id', 'DESC')
			->paginate(8);
			
		$result['recentProducts'] = $recentProducts;
		
		//products
		$products = DB::table('products')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->where('products_description.language_id','=', $language_id)
			->orderBy('products.products_id', 'DESC')
			->get();
			
		//low products & out of stock
		$lowLimit = 0;
		$outOfStock = 0;
		foreach($products as $products_data){
			if($products_data->low_limit >= 1 && $products_data->products_quantity >= $products_data->low_limit){
				$lowLimit++;
			}elseif($products_data->products_quantity == 0){
				$outOfStock++;
			}
		}
		
		$result['lowLimit'] = $lowLimit;
		$result['outOfStock'] = $outOfStock;	
		$result['totalProducts'] = count($products);
		
		$result['reportBase'] = $reportBase;	
		
		$result['currency'] = $currency;
		
		return view("vendor.dashboard",$title)->with('result', $result);
	}
	
	
	public function login(){
		if (Auth::check()) {
		  return redirect('/vendor/dashboard/this_month');
		}else{
			$title = array('pageTitle' => Lang::get("labels.login_page_name"));
			return view("vendor.login",$title);
		}
	}
	
	public function vendorinfo(){
		$vendoristor = vendors::all();		
		return view("vendor.login",$title);
	}
	
	//login function
	public function checkLogin(Request $request){
		$validator = Validator::make(
			array(
					'email'    => $request->email,
					'password' => $request->password
				), 
			array(
					'email'    => 'required | email',
					'password' => 'required',
				)
		);
		//check validation
		if($validator->fails()){
			return redirect('vendor/login')->withErrors($validator)->withInput();
		}else{
			//check authentication of email and password
			$vendorInfo = array("email" => $request->email, "password" => $request->password);
			
			if(auth()->guard('vendor')->attempt($vendorInfo)) {
				$vendor = auth()->guard('vendor')->user();
				$vendors = DB::table('vendors')->where('vendor_id', $vendor->vendor_id)->get();	
				return redirect()->intended('vendor/dashboard/this_month')->with('vendors', $vendors);
			}else{
				return redirect('vendor/login')->with('loginError',Lang::get("labels.EmailPasswordIncorrectText"));
			}
			
		}
		
	}
	
	
	//logout
	public function logout(){
		Auth::guard('vendor')->logout();
		return redirect()->intended('vendor/login');
	}
	
	//vendor profile
	public function vendorProfile(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Profile"));		
		
		$result = array();
		
		$countries = DB::table('countries')->get();
		$zones = DB::table('zones')->where('zone_country_id', '=', auth()->guard('vendor')->user()->country)->get();
		
		$result['countries'] = $countries;
		$result['zones'] = $zones;
		
		return view("vendor.vendorProfile",$title)->with('result', $result);
	}
	
	//updateProfile
	public function updateProfile(Request $request){
		
		$updated_at	= date('y-m-d h:i:s');
		
		$myVar = new VendorSiteSettingController();
		$languages = $myVar->getLanguages();		
		$extensions = $myVar->imageType();
		
		if($request->hasFile('newImage') and in_array($request->newImage->extension(), $extensions)){
			$image = $request->newImage;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/views/vendor/images/vendor_profile/', $fileName);
			$uploadImage = 'resources/views/vendor/images/vendor_profile/'.$fileName; 
		}	else{
			$uploadImage = $request->oldImage;
		}	
		
		$orders_status = DB::table('vendors')->where('myid','=', auth()->guard('vendor')->user()->myid)->update([
				'user_name'		=>	$request->user_name,
				'first_name'	=>	$request->first_name,
				'last_name'		=>	$request->last_name,
				'address'		=>	$request->address,
				'city'			=>	$request->city,
				'state'			=>	$request->state,
				'zip'			=>	$request->zip,
				'country'		=>	$request->country,
				'phone'			=>	$request->phone,
				'image'			=>	$uploadImage,
				'updated_at'	=>	$updated_at
				]);
		
		$message = Lang::get("labels.ProfileUpdateMessage");
		return redirect()->back()->withErrors([$message]);
	}
	
	//updateProfile
	public function updateVendorPassword(Request $request){
		
		$orders_status = DB::table('vendors')->where('myid','=', auth()->guard('vendor')->user()->myid)->update([
				'password'		=>	Hash::make($request->password)
				]);
		
		$message = Lang::get("labels.PasswordUpdateMessage");
		return redirect()->back()->withErrors([$message]);
	}

}
