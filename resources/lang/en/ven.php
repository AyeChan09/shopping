<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //Dashboard labels
    'title_dashboard' => 'Dashboard',
    'dashboard' => 'dashboard',
    'gold' => 'Gold',
    'NewOrders' => 'New Orders',
    'viewAllOrders' => 'View All Orders',
    'outOfStock' => 'Out of Stock',
    'customerRegistrations' => 'Customer Registrations',
    'viewAllCustomers' => 'View All Customers',
	'totalProducts' => 'Total Products',
    'viewAllProducts' => 'View All Products',
    'addedSaleReport' => 'Added/Sale Report',
	'soldProducts' => 'Sold Products',
    'addedProducts' => 'Added Products',
    'lastYear' => 'Last Year',
	'LastMonth' => 'Last Month',
	'thisMonth' => 'This Month',
	
	//links labels
	'login_text' => 'Sign in to start your session',
	'welcome_message'	 =>	'Welcome',
	'welcome_message_to'=>'To Admin Panel',
	'login'=>'Login',
    'login_page_name'=>'Login',


    //sidebar labels
    'navigation' => 'NAVIGATION',
    'header_dashboard' => 'Dashboard',
    'online' => 'online',
    'link_products' => 'Products',
    'link_all_products'=>'Products',
    'products_attributes'=>'Products Attributes',
    'link_orders' => 'Orders',

    //common labels
    'breadcrumb_dashboard'=>'Dashboard',
    'Close' => 'Close',

    //header labels
    'toggle_navigation' => 'toggle_navigation',
    'profile_link' => 'profile_link',
    'sign_out' => 'sign_out',
    'new_orders' => 'new_orders',
    'you_have' => 'you_have',
    'products_are_in_low_quantity' => 'products_are_in_low_quantity',

    //product labels
    'Products' => 'Products',
    'ListingAllProducts' => 'Listing All Products',
    'AddNewProducts' => 'Add New Products',
    'FilterByCategory/Products'=>'Filter By Category/Products',
    'SelectCategory' => 'Select Category',
    'Search' => 'Search',
    'ClearSearch' => 'Clear Search',
    'ProductDescription' => 'Admin',
    'Manufacturer' => 'Manufacturer',
	'Quantity' => 'Quantity',
	'Price' => 'Price',
	'Weight' => 'Weight',
	'Viewed' => 'Viewed',
	'SpecialPrice' => 'Special Price',
    'Expired' => 'Expired',
    'DeleteProduct' => 'Delete Product',
    'DeleteThisProductDiloge' => 'Are you sure you want to delete this product',
    
    'ListingAllMainCategories' => 'Listing All Main Categories',
	'AddNewCategory' => 'Add New Category',
	'AddedLastModifiedDate' => 'Added/Last Modified Date',
	'ListAllCategories' => 'List All Categories',
	'AddCategory' => 'Add Category',
	'CategoryImageText' => 'Upload category image.',
	'CategoryIconText' => 'Upload category icon.',
	'CategoryName' => 'Category name',
	'MainCategory' => 'Main Category',
	'AddedDate' => 'Added Date',
	'ModifiedDate' => 'Modified Date',
	'AddSubCategories' => 'Add Sub Categories',
	'Category' => 'Category',
	'ChooseMainCategory' => 'Choose main category.',
	'SubCategoryName' => 'Sub category name',
	'UploadSubCategoryImage' => 'Upload sub category image',
	'UploadSubCategoryIcon' => 'Upload sub category icon.',
	'EditSubCategories' => 'Edit Sub Categories',

    //languages / labels	
	'ListingLanguages' => 'Listing Languages',
	'languages' => 'Languages',
	'ListingAllLanguages' => 'Listing All Languages',
	'ManageLabels' => 'Manage Labels',
	'AddLanguage' => 'Add Language',
	'ID' => 'ID',
	'Icon' => 'Icon',
	'Code' => 'Code',
    'Nolanguageexist' => 'No language exist.',
    'Image' => 'Image',

    
    'vendor' => 'Vendor',
    'NoRecordFound' => 'No Record found.',


    'EmailPasswordIncorrectText' => 'Email or password is incorrect!',

    'ListingOrders' => 'Listing Orders',
    
    	
	
];
