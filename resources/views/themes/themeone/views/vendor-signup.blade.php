@extends('layout')
@section('content')
<section class="site-content">
<div class="container">
<div class="breadcum-area">
    <div class="breadcum-inner">
        <h3>@lang('website.Vendor Signup')</h3>
        <ol class="breadcrumb">
            
            <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">@lang('website.Home')</a></li>
            <li class="breadcrumb-item active">@lang('website.Signup')</li>
        </ol>
    </div>
</div>

<div class="registration-area">

        <div class="heading">
            <h2>@lang('website.Create An Account')</h2>
            <hr>
        </div>
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 new-customers">
				<h5 class="title-h5">@lang('website.Vendor Information')</h5>
				<!-- <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p> -->

				<hr class="featurette-divider">
				@if( count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">@lang('website.Error'):</span>
                            {{ $error }}
                          	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
						</div>
					 @endforeach
				@endif

				@if(Session::has('error'))
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						  <span class="sr-only">@lang('website.Error'):</span>
						  {!! session('error') !!}
                          
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						  <span class="sr-only">@lang('website.Success'):</span>
						  {!! session('success') !!}
                          
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          		<span aria-hidden="true">&times;</span>
                          </button>
					</div>
				@endif

				<form name="signup" enctype="multipart/form-data" class="form-validate" action="{{ URL::to('/vendorSignupProcess')}}" method="post">
                
                	<div class="form-group row justify-content-center">
						
                        <div class="uploader">
                        	<h5 class="title-h5">@lang('website.Upload Vendor Photo')</h5>
                            <div class="upload-picture">
                                <div class="uploaded-image" id="uploaded_image"></div>
                                @if(empty('picture'))
                                <img class="upload-choose-icon" src="{{asset('').'public/images/default.png'}}" />
                                @endif
                                <div class="upload-choose-icon">
                                	<input name="picture" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
					</div>
                    
                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong>@lang('website.First Name')</label>
                            <div>
                                <input type="text" name="firstName" id="firstName" class="form-control field-validate" value="{{ old('firstName') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your first name')</span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inputPassword" class="col-form-label"><strong>*</strong>@lang('website.Last Name')</label>
                            <div>
                                <input type="text" name="lastName" id="lastName" class="form-control field-validate"  value="{{ old('lastName') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your last name')</span> 
                            </div>
                        </div>
					</div>

					<div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="inputPassword" class="col-form-label"><strong>*</strong>@lang('website.Email Adrress')</label>
                            <div>
                                <input type="text" name="email" id="email" class="form-control email-validate" value="{{ old('email') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your valid email address')</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inlineFormCustomSelect" class="col-form-label"><strong>*</strong>@lang('website.Gender')</label>
                            <div>
                                <select class="custom-select field-validate" name="gender" id="inlineFormCustomSelect">
                                    <option selected value="">@lang('website.Choose...')</option>
                                    <option value="0" @if(!empty(old('gender')) and old('gender')==0) selected @endif)>@lang('website.Male')</option>
                                    <option value="1" @if(!empty(old('gender')) and old('gender')==1) selected @endif>@lang('website.Female')</option>
                                </select>
                                <span class="help-block error-content" hidden>@lang('website.Please select your gender')</span>
                            </div>
                        </div>
					</div>
					
					<div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="inputPassword4" class="col-form-label"><strong>*</strong>@lang('website.Password')</label>
                            <div>
                                <input type="password" class="form-control password" name="password" id="password">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your password')</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inputPassword5" class="col-form-label"><strong>*</strong>@lang('website.Confirm Password')</label>
                            <div class="re-password-content">
                                <input type="password" class="form-control password" name="re_password" id="re_password">
                                <span class="help-block error-content" hidden>@lang('website.Please re-enter your password')</span>
                                <span class="help-block error-content-password" hidden>@lang('website.Password does not match the confirm password')</span>
                            </div>				  	
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-12">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong>@lang('website.Company')</label>
                            <div>
                                <input type="text" name="company" id="company" class="form-control field-validate" value="{{ old('company') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your company')</span> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-12">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong>@lang('website.Address')</label>
                            <div>
                                <input type="text" name="address" id="address" class="form-control field-validate" value="{{ old('address') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your address')</span> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="city" class="col-form-label"><strong>*</strong>@lang('website.City')</label>
                            <div>
                                <input type="text" name="city" id="city" class="form-control field-validate" value="{{ old('city') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your city')</span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="state" class="col-form-label"><strong>*</strong>@lang('website.State')</label>
                            <div>
                                <input type="text" name="state" id="state" class="form-control field-validate"  value="{{ old('state') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your state')</span> 
                            </div>
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="country" class="col-form-label"><strong>*</strong>@lang('website.Country')</label>
                            <div>
                                <input type="text" name="country" id="country" class="form-control field-validate" value="{{ old('country') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your country')</span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="zip" class="col-form-label"><strong>*</strong>@lang('website.Zip')</label>
                            <div>
                                <input type="text" name="zip" id="zip" class="form-control field-validate"  value="{{ old('zip') }}">
                                <span class="help-block error-content" hidden>@lang('website.Please enter your zip')</span> 
                            </div>
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
						<!-- <label class="col-sm-4 col-form-label"></label> -->
						<div class="col-sm-12">
							<div class="form-check checkbox-parent">
								<label class="form-check-label">
									<input class="form-check-input checkbox-validate" type="checkbox">@lang('website.Creating an account means you are okay with our')  @if(!empty($result['commonContent']['pages'][3]->slug))<a href="{{ URL::to('/page?name='.$result['commonContent']['pages'][3]->slug)}}">@endif @lang('website.Terms and Services')@if(!empty($result['commonContent']['pages'][3]->slug))</a>@endif, @if(!empty($result['commonContent']['pages'][1]->slug))<a href="{{ URL::to('/page?name='.$result['commonContent']['pages'][1]->slug)}}">@endif @lang('website.Privacy Policy')@if(!empty($result['commonContent']['pages'][1]->slug))</a> @endif and @if(!empty($result['commonContent']['pages'][2]->slug))<a href="{{ URL::to('/page?name='.$result['commonContent']['pages'][2]->slug)}}">@endif @lang('website.Refund Policy') @if(!empty($result['commonContent']['pages'][3]->slug))</a>@endif.
								</label>
								<span class="help-block error-content" hidden>@lang('website.Please accept our terms and conditions')</span>
							</div>
                            
						</div>
					</div>
					<div class="button">
                    	<button type="submit" class="btn btn-dark pull-right">@lang('website.Sign Up')</button>
                    </div>
				</form>
			</div>
		</div>
	</div>		
	</div>
   </section>
		
@endsection 	


