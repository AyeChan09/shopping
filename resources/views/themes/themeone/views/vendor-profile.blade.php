@extends('layout')
@section('content')
<section class="site-content">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-2"></div>
            <div class="col-md-auto" style="text-align:center;">
            <h3 style="color:gold;">@lang('website.Vendor Detail')</h3>
                            @if($result['vendor'])
                            
                                <img class="img-responsive" src="{{asset('').$result['vendor']->vendors_picture}}" alt="{{$result['vendor']->user_name}}" style="height:100px; width=100px;">
                        
                                <h4>{{$result['vendor']->vendors_firstname}} {{$result['vendor']->vendors_lastname}}</h4>
                            
                                <h6>{{$result['vendor']->company}}</h6>
                                <h6>{{$result['vendor']->address}}</h6>
                            @endif

            </div>
            <div class="col col-lg-2"></div>
    
        </div>



<div class="row">        
    <div class="col-xs-12 col-sm-12">
            <div class="row">
    <!-- Items -->
                <div class="products products-5x">
        <!-- Product --> 
        <h3 style="color:gold;">@lang('website.products')</h3>
                        @foreach($result['detail']['products'] as $key=>$product)
                        @if($key<=4)
                        <div class="product">
                            <article>
                                <div class="thumb"><img class="img-fluid" src="{{asset('').$product->products_image}}" alt=""></div>
                                    <?php
                                            $current_date = date("Y-m-d", strtotime("now"));
                    
                                    ?>
                                    <div class="price">
                        {{$web_setting[19]->value}}{{$product->products_price+0}}
                    </div>
                            </article>
                        </div>
                        @endif
                        @endforeach
        
                </div>
            </div>
    </div>
</div>





</div>
</section>







<div class="container-fuild">
    <div class="container">
        <div class="products-area"> 
            <!-- heading -->
            <div class="heading">
                <h3 style="color:gold;">@lang('website.Brands')</h3>
                        
                <hr>
            </div>
            <div class="row">         
                
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <!-- Items -->
                        <div class="products products-5x">
                            <!-- Product --> 
                
                            <div id="owl-demo" class="owl-carousel owl-theme">
                         @foreach($result['detail']['brands'] as $key=>$brand)
                        @if($key<=4)
                        <!-- <div class="product"> -->
                            
                            

                                 <div class="thumb"><img class="img-fluid" src="{{asset('').$brand->manufacturers_image}}" alt=""></div>
                                
                            @endif
                            @endforeach
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

          <script>
            $(document).ready(function() {
               var owl = $('.owl-carousel');
              owl.owlCarousel({
               loop: true,
                margin: 30,
                autoplay: true,
                autoplayTimeout: 5000,
                nav: true,
                
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                 "<i class='fa fa-angle-left'></i>",
                 "<i class='fa fa-angle-right'></i>"
             ],
				
				
				 responsiveClass: true,
                 responsive: {
            0: {items: 2},
            479: {items: 3},
            768: {items: 3},
            991: {items: 6},
            1024: {items: 6}
        }

				
				
              });
             
            })





          </script>
@endsection 	


