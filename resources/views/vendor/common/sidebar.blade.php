<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('').auth()->guard('vendor')->user()->vendors_picture}}" class="img-circle" alt="{{ auth()->guard('vendor')->user()->first_name }} {{ auth()->guard('vendor')->user()->last_name }} Image">
        </div>
        <div class="pull-left info">
          <p>{{ auth()->guard('vendor')->user()->vendors_firstname }} {{ auth()->guard('vendor')->user()->vendors_lastname }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('ven.online') }}</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">{{ trans('ven.navigation') }}</li>
        <li class="treeview {{ Request::is('vendor/dashboard/this_month') ? 'active' : '' }}">
          <a href="{{ URL::to('vendor/dashboard/this_month')}}">
            <i class="fa fa-dashboard"></i> <span>{{ trans('ven.header_dashboard') }}</span>
          </a>
        </li>
        
        <li class="treeview {{ Request::is('vendor/products') ? 'active' : '' }} {{ Request::is('vendor/addproduct') ? 'active' : '' }} {{ Request::is('vendor/editattributes/*') ? 'active' : '' }} {{ Request::is('vendor/attributes') ? 'active' : '' }}  {{ Request::is('vendor/addattributes') ? 'active' : '' }}  {{ Request::is('vendor/editattributes/*') ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-database"></i> <span>{{ trans('ven.link_products') }}</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::is('vendor/products') ? 'active' : '' }} {{ Request::is('vendor/addproduct') ? 'active' : '' }} {{ Request::is('vendor/editproduct/*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/products')}}"><i class="fa fa-circle-o"></i> {{ trans('ven.link_all_products') }}</a></li>
            
            <li class="{{ Request::is('vendor/attributes') ? 'active' : '' }}  {{ Request::is('vendor/addattributes') ? 'active' : '' }}  {{ Request::is('vendor/editattributes/*') ? 'active' : '' }}" ><a href="{{ URL::to('vendor/attributes' )}}"><i class="fa fa-circle-o"></i> {{ trans('ven.products_attributes') }}</a></li>
          </ul>
        </li>

        <li class="treeview {{ Request::is('admin/packages') ? 'active' : '' }} {{ Request::is('admin/editpackages/*') ? 'active' : '' }}">
          <a href="{{ URL::to('admin/packages')}}" ><i class="fa fa-tablet" aria-hidden="true"></i> <span>{{ trans('labels.link_packages') }}</span></a>
        </li>
        
        <li class="treeview {{ Request::is('admin/coupons') ? 'active' : '' }} {{ Request::is('admin/editcoupons/*') ? 'active' : '' }}">
          <a href="{{ URL::to('admin/coupons')}}" ><i class="fa fa-tablet" aria-hidden="true"></i> <span>{{ trans('labels.link_coupons') }}</span></a>
        </li>
        
        <li class="treeview {{ Request::is('vendor/orders') ? 'active' : '' }}  {{ Request::is('vendor/addOrders') ? 'active' : '' }}  {{ Request::is('vendor/vieworder/*') ? 'active' : '' }}">
          <a href="{{ URL::to('admin/orders')}}" ><i class="fa fa-list-ul" aria-hidden="true"></i> <span> {{ trans('ven.link_orders') }}</span>
          </a>
        </li>
        
        <li class="treeview {{ Request::is('admin/statscustomers') ? 'active' : '' }} {{ Request::is('admin/productsstock') ? 'active' : '' }} {{ Request::is('admin/statsproductspurchased') ? 'active' : '' }} {{ Request::is('admin/statsproductsliked') ? 'active' : '' }} ">
          <a href="#">
            <i class="fa fa-file-text-o" aria-hidden="true"></i>
<span>{{ trans('labels.link_reports') }}</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::is('admin/productsstock') ? 'active' : '' }} "><a href="{{ URL::to('admin/productsstock')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_stock') }}</a></li>  
            <li class="{{ Request::is('admin/statscustomers') ? 'active' : '' }} "><a href="{{ URL::to('admin/statscustomers')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_customer_orders_total') }}</a></li>             
            <li class="{{ Request::is('admin/statsproductspurchased') ? 'active' : '' }}"><a href="{{ URL::to('admin/statsproductspurchased')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_total_purchased') }}</a></li>
            <li class="{{ Request::is('admin/statsproductsliked') ? 'active' : '' }}"><a href="{{ URL::to('admin/statsproductsliked')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_liked') }}</a></li>
          </ul>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>