@extends('vendor.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ trans('ven.title_dashboard') }}  
        <small>{{ trans('ven.title_dashboard') }}</small>
      </h1>
      <ol class="breadcrumb">
        <small style="font-size: 14px;">{{ trans('ven.gold') }}: 0/3&nbsp;&nbsp;&nbsp;<button>Upgrade</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
        <li class="active" style="float: right; padding-top: 3px;"><i class="fa fa-dashboard"></i> {{ trans('ven.breadcrumb_dashboard') }}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">   
    <div class="row">
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>2</h3>
			        <p>{{ trans('ven.NewOrders') }}</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ URL::to('vendor/orders')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="View All Orders">{{ trans('ven.viewAllOrders') }} <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
        
          <div class="small-box bg-red">
            <div class="inner">
              <h3>0 </h3>
              <p>{{ trans('ven.outOfStock') }}</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ URL::to('vendor/productsstock?action=outofstock')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="Out of Stock">{{ trans('ven.outOfStock') }} <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>19</h3>

              <p>{{ trans('ven.totalProducts') }}</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{ URL::to('vendor/products')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="Out of Products">{{ trans('ven.viewAllProducts') }} <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <div class="row">
      <div class="col-sm-12">
          <div class="nav-tabs-custom">
          <div class="box-header with-border">
            <h3 class="box-title"> {{ trans('ven.addedSaleReport') }}</h3>
            <div class="box-tools pull-right">
               <p class="notify-colors"><span class="sold-content" data-toggle="tooltip" data-placement="bottom" title="Sold Products"></span> {{ trans('ven.soldProducts') }}  <span class="purchased-content" data-toggle="tooltip" data-placement="bottom" title="Added Products"></span>{{ trans('ven.addedProducts') }} </p>
               </div>
            </div>
            
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->
      
      <!-- /.row -->
    </section>
</div>
<script src="{!! asset('resources/views/vendor/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

<script src="{!! asset('resources/views/vendor/dist/js/pages/dashboard2.js') !!}"></script>
  @endsection