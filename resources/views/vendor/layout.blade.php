<!DOCTYPE html>
<html>

<!-- meta contains meta taga, css and fontawesome icons etc -->
@include('vendor.common.meta')
<!-- ./end of meta -->

<body class=" hold-transition skin-blue sidebar-mini">
	<!-- wrapper -->
    <div class="wrapper">
    
   		<!-- header contains top navbar -->
        @include('vendor.common.header')
        <!-- ./end of header -->
        
        <!-- left sidebar -->
        @include('vendor.common.sidebar')
        <!-- ./end of left sidebar -->
        
        <!-- dynamic content -->
        @yield('content')
        <!-- ./end of dynamic content -->
        
        <!-- right sidebar -->
        @include('vendor.common.controlsidebar')
        <!-- ./right sidebar -->
    	@include('vendor.common.footer')
    </div>
	<!-- ./wrapper -->

	<!-- all js scripts including custom js -->
	@include('vendor.common.scripts')
    <!-- ./end of js scripts -->
    
	</body>
</html>
