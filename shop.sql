-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for shop
CREATE DATABASE IF NOT EXISTS `shop` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `shop`;

-- Dumping structure for table shop.action_recorder
CREATE TABLE IF NOT EXISTS `action_recorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `success` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_action_recorder_module` (`module`),
  KEY `idx_action_recorder_user_id` (`user_id`),
  KEY `idx_action_recorder_identifier` (`identifier`),
  KEY `idx_action_recorder_date_added` (`date_added`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.action_recorder: ~0 rows (approximately)
/*!40000 ALTER TABLE `action_recorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_recorder` ENABLE KEYS */;

-- Dumping structure for table shop.address_book
CREATE TABLE IF NOT EXISTS `address_book` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `entry_gender` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id` (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.address_book: ~0 rows (approximately)
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
INSERT INTO `address_book` (`address_book_id`, `customers_id`, `entry_gender`, `entry_company`, `entry_firstname`, `entry_lastname`, `entry_street_address`, `entry_suburb`, `entry_postcode`, `entry_city`, `entry_state`, `entry_country_id`, `entry_zone_id`) VALUES
	(1, 1, NULL, NULL, 'U', 'Yaw Ya', '123 Street', NULL, '11025', 'Yangon', NULL, 146, 0);
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;

-- Dumping structure for table shop.address_format
CREATE TABLE IF NOT EXISTS `address_format` (
  `address_format_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address_summary` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_format_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.address_format: ~0 rows (approximately)
/*!40000 ALTER TABLE `address_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_format` ENABLE KEYS */;

-- Dumping structure for table shop.administrators
CREATE TABLE IF NOT EXISTS `administrators` (
  `myid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `adminType` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`myid`),
  UNIQUE KEY `administrators_user_name_unique` (`user_name`),
  UNIQUE KEY `administrators_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.administrators: ~4 rows (approximately)
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
INSERT INTO `administrators` (`myid`, `user_name`, `first_name`, `last_name`, `email`, `password`, `isActive`, `address`, `city`, `state`, `zip`, `country`, `phone`, `image`, `adminType`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'yawya', 'Admin', '', 'mr.yawya@gmail.com', '$2y$10$oQV2SxtH5rRmvbDpYvlDfOzbvU86LK5L6GxIsiWEctfEyAgJJcokW', 1, 'address', 'Nivada', '12', '38000', '115f89503138416a242f40fb7d7f338e', '0312 6545 874', 'resources/views/admin/images/admin_profile/1513671470.fast.jpg', 1, 'X2uumWYHWzISTy4YLafBATc21jftB1pRRCdbVSggkkniI7FhvUSncgOn3EHz', '0000-00-00 00:00:00', '2017-12-19 04:02:50'),
	(8, 'Admin', 'Admin', '', 'demo@android.com', '$2y$10$vbQE1Lbu1kXCAILSvaH0uOZ3oA6oZdCf/0kjQB16iGnjc3eTaFBeu', 1, 'address', 'Nivada', '12', '38000', '223', '', 'resources/views/admin/images/admin_profile/1513671470.fast.jpg', 1, 'resources/views/admin/images/admin_profile/1505132393.1486628854.fast.jpg', NULL, NULL),
	(9, 'admin2', 'Admin', '', 'demo@ionic.com', '$2y$10$vbQE1Lbu1kXCAILSvaH0uOZ3oA6oZdCf/0kjQB16iGnjc3eTaFBeu', 1, 'address', 'Nivada', '12', '38000', '223', '', 'resources/views/admin/images/admin_profile/1513671470.fast.jpg', 1, 'resources/views/admin/images/admin_profile/1505132393.1486628854.fast.jpg', NULL, NULL),
	(10, 'vectorcoder', 'Vector', 'Coder', 'vectorcoder@gmail.com', '$2y$10$TKJBNrT7bkFqz49XazJL7.mTa49DI9CeCcZipjuFer1h.OeZWsaHC', 1, 'address', 'Nivada', '12', '38000', '223', '', 'resources/views/admin/images/admin_profile/1513671470.fast.jpg', 1, 'resources/views/admin/images/admin_profile/1505132393.1486628854.fast.jpg', NULL, NULL),
	(11, 'ayechan', 'Admin', '', 'ayechanthaw@gmail.com', '$2y$10$oQV2SxtH5rRmvbDpYvlDfOzbvU86LK5L6GxIsiWEctfEyAgJJcokW', 1, 'address', 'Nivada', '12', '38000', '115f89503138416a242f40fb7d7f338e', '0312 6545 874', 'resources/views/admin/images/admin_profile/1513671470.fast.jpg', 1, 'X2uumWYHWzISTy4YLafBATc21jftB1pRRCdbVSggkkniI7FhvUSncgOn3EHz', '0000-00-00 00:00:00', '2017-12-19 04:02:50');
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;

-- Dumping structure for table shop.alert_settings
CREATE TABLE IF NOT EXISTS `alert_settings` (
  `alert_id` int(100) NOT NULL AUTO_INCREMENT,
  `create_customer_email` tinyint(1) NOT NULL DEFAULT '0',
  `create_customer_notification` tinyint(1) NOT NULL DEFAULT '0',
  `order_status_email` tinyint(1) NOT NULL DEFAULT '0',
  `order_status_notification` tinyint(1) NOT NULL DEFAULT '0',
  `new_product_email` tinyint(1) NOT NULL DEFAULT '0',
  `new_product_notification` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_email` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_notification` tinyint(1) NOT NULL DEFAULT '0',
  `news_email` tinyint(1) NOT NULL DEFAULT '0',
  `news_notification` tinyint(1) NOT NULL DEFAULT '0',
  `contact_us_email` tinyint(1) NOT NULL DEFAULT '0',
  `contact_us_notification` tinyint(1) NOT NULL DEFAULT '0',
  `order_email` tinyint(1) NOT NULL DEFAULT '0',
  `order_notification` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`alert_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table shop.alert_settings: 1 rows
/*!40000 ALTER TABLE `alert_settings` DISABLE KEYS */;
INSERT INTO `alert_settings` (`alert_id`, `create_customer_email`, `create_customer_notification`, `order_status_email`, `order_status_notification`, `new_product_email`, `new_product_notification`, `forgot_email`, `forgot_notification`, `news_email`, `news_notification`, `contact_us_email`, `contact_us_notification`, `order_email`, `order_notification`) VALUES
	(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1);
/*!40000 ALTER TABLE `alert_settings` ENABLE KEYS */;

-- Dumping structure for table shop.api_calls_list
CREATE TABLE IF NOT EXISTS `api_calls_list` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nonce` text NOT NULL,
  `url` varchar(64) NOT NULL,
  `device_id` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table shop.api_calls_list: 0 rows
/*!40000 ALTER TABLE `api_calls_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_calls_list` ENABLE KEYS */;

-- Dumping structure for table shop.banners
CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `banners_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `banners_html_text` mediumtext COLLATE utf8_unicode_ci,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `banners_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`banners_id`),
  KEY `idx_banners_group` (`banners_group`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.banners: ~5 rows (approximately)
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`banners_id`, `banners_title`, `banners_url`, `banners_image`, `banners_group`, `banners_html_text`, `expires_impressions`, `expires_date`, `date_scheduled`, `date_added`, `date_status_change`, `status`, `type`, `banners_slug`) VALUES
	(1, 'banner-1', '81', 'resources/assets/images/banner_images/1504099866.banner_1.jpg', '', NULL, 0, '2030-01-01 00:00:00', NULL, '2018-03-30 10:45:41', '2018-03-30 10:45:41', 1, 'product', ''),
	(2, 'Banner-2', '12', 'resources/assets/images/banner_images/1502370343.banner_2.jpg', '', NULL, 0, '2020-01-01 00:00:00', NULL, '2017-08-10 13:05:43', NULL, 1, 'category', ''),
	(3, 'Banner-3', '23', 'resources/assets/images/banner_images/1502370366.banner_3.jpg', '', NULL, 0, '2030-01-01 00:00:00', NULL, '2017-08-10 13:06:06', NULL, 1, 'category', ''),
	(4, 'Banner-4', '17', 'resources/assets/images/banner_images/1502370387.banner_4.jpg', '', NULL, 0, '2030-01-01 00:00:00', NULL, '2017-08-10 13:06:27', NULL, 1, 'category', ''),
	(5, 'Banner-5', '19', 'resources/assets/images/banner_images/1502370406.banner_5.jpg', '', NULL, 0, '2030-01-01 00:00:00', NULL, '2017-08-10 13:06:46', NULL, 1, 'category', '');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;

-- Dumping structure for table shop.banners_history
CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL,
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL,
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_history_banners_id` (`banners_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.banners_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `banners_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners_history` ENABLE KEYS */;

-- Dumping structure for table shop.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` mediumtext COLLATE utf8_unicode_ci,
  `categories_icon` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `categories_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categories_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.categories: ~33 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`categories_id`, `categories_image`, `categories_icon`, `parent_id`, `sort_order`, `date_added`, `last_modified`, `categories_slug`) VALUES
	(1, 'resources/assets/images/category_images/1502285429.men.jpg', 'resources/assets/images/category_icons/1528806598.shirt.png', 0, NULL, '2017-08-07 10:19:26', '2018-07-06 03:23:37', 'men-s-clothing'),
	(2, 'resources/assets/images/category_images/1502285278.women.jpg', 'resources/assets/images/category_icons/1528806661.dress.png', 0, NULL, '2017-08-07 10:24:45', '2018-07-10 12:46:38', 'women-s-clothing'),
	(3, 'resources/assets/images/category_images/1502285654.boys.jpg', 'resources/assets/images/category_icons/1502101936.man-standing-up.png', 0, NULL, '2017-08-07 10:32:16', '2018-07-10 12:46:45', 'boy-s-clothing'),
	(4, 'resources/assets/images/category_images/1502285903.girls.jpg', 'resources/assets/images/category_icons/1502103958.female-silhouette.png', 0, NULL, '2017-08-07 11:05:58', '2018-07-10 12:46:53', 'girl-s-clothing'),
	(5, 'resources/assets/images/category_images/1502285986.babies.jpg', 'resources/assets/images/category_icons/1528806779.dress (1).png', 0, NULL, '2017-08-07 11:07:21', '2018-07-10 12:47:11', 'baby-mother'),
	(6, 'resources/assets/images/category_images/1502286030.home.jpg', 'resources/assets/images/category_icons/1528806720.sofa.png', 0, NULL, '2017-08-07 11:10:26', '2018-07-10 12:47:30', 'household-merchandises'),
	(7, 'resources/assets/images/category_images/1502286458.polo_shirts.jpg', 'resources/assets/images/category_icons/1502105603.shirt.png', 1, NULL, '2017-08-07 11:33:23', '2018-07-10 12:49:07', 'men-polo-shirts'),
	(8, 'resources/assets/images/category_images/1502286584.casual.jpg', 'resources/assets/images/category_icons/1502178863.shirt.png', 1, NULL, '2017-08-08 07:54:23', '2018-07-10 12:49:31', 'men-polo-shirts-1'),
	(9, 'resources/assets/images/category_images/1502286701.jeans.jpg', 'resources/assets/images/category_icons/1502179389.jeans.png', 1, NULL, '2017-08-08 08:03:09', '2018-07-10 12:49:37', 'men-jeans'),
	(10, 'resources/assets/images/category_images/1502284855.landscape-1482456067-jordan-take-flight.jpg', 'resources/assets/images/category_icons/1502179895.sneaker.png', 1, NULL, '2017-08-08 08:11:35', '2018-07-10 12:49:47', 'men-shoes'),
	(11, 'resources/assets/images/category_images/1502284031.glasses.jpg', 'resources/assets/images/category_icons/1502180493.reading-glasses.png', 1, NULL, '2017-08-08 08:21:33', '2018-07-10 12:49:57', 'sunglasses-glasses'),
	(12, 'resources/assets/images/category_images/1502287711.wo_dresses.jpg', 'resources/assets/images/category_icons/1502194101.dress.png', 2, NULL, '2017-08-08 12:08:21', '2018-07-10 12:50:04', 'women-dresses'),
	(13, 'resources/assets/images/category_images/1502287196.wo_shirts.jpg', 'resources/assets/images/category_icons/1502194198.shirt.png', 2, NULL, '2017-08-08 12:09:58', '2018-07-10 12:50:14', 'women-shirts-tops'),
	(14, 'resources/assets/images/category_images/1502287459.wo_jans.jpg', 'resources/assets/images/category_icons/1502194291.jeans.png', 2, NULL, '2017-08-08 12:11:31', '2018-07-10 12:50:20', 'women-jeans'),
	(15, 'resources/assets/images/category_images/1502287533.wo_handbags.jpg', 'resources/assets/images/category_icons/1502194427.handbag.png', 2, NULL, '2017-08-08 12:13:47', '2018-07-10 12:50:38', 'women-hand-bags'),
	(16, 'resources/assets/images/category_images/1502287793.new_baby.jpg', 'resources/assets/images/category_icons/1502263460.smiling-baby.png', 5, NULL, '2017-08-09 07:24:20', '2018-07-10 12:50:45', 'new-born'),
	(17, 'resources/assets/images/category_images/1502287893.baby_dress.jpg', 'resources/assets/images/category_icons/1502267564.dress.png', 5, NULL, '2017-08-09 08:32:44', '2018-07-10 12:50:57', 'baby-dresses'),
	(18, 'resources/assets/images/category_images/1502288151.baby_blaket.jpg', 'resources/assets/images/category_icons/1502273393.blanket.png', 5, NULL, '2017-08-09 10:09:53', '2018-07-10 12:51:01', 'baby-blankets-swaddles'),
	(19, 'resources/assets/images/category_images/1502288250.bed_col.jpg', 'resources/assets/images/category_icons/1502274677.modern-double-bed.png', 6, NULL, '2017-08-09 10:31:17', '2018-07-10 12:51:05', 'bedding-collections'),
	(20, 'resources/assets/images/category_images/1502288368.pillows.jpg', 'resources/assets/images/category_icons/1502278859.pillow.png', 6, NULL, '2017-08-09 11:40:59', '2018-07-10 12:51:09', 'throws-pillows'),
	(21, 'resources/assets/images/category_images/1502346394.bath_robe.jpg', 'resources/assets/images/category_icons/1502280642.bath-robe.png', 6, NULL, '2017-08-09 12:10:42', '2018-07-10 12:51:13', 'bath-robes'),
	(22, 'resources/assets/images/category_images/1502346623.polo_shirts.jpg', 'resources/assets/images/category_icons/1502283220.shirt2.png', 3, NULL, '2017-08-09 12:53:40', '2018-07-10 12:51:17', 'boy-polo-shirts'),
	(23, 'resources/assets/images/category_images/1502346643.casual.jpg', 'resources/assets/images/category_icons/1502283252.shirt.png', 3, NULL, '2017-08-09 12:54:12', '2018-07-10 12:51:21', 'boy-casual-shirts'),
	(24, 'resources/assets/images/category_images/1502346665.jeans.jpg', 'resources/assets/images/category_icons/1502283334.jeans.png', 3, NULL, '2017-08-09 12:55:34', '2018-07-10 12:51:26', 'boy-pants-jeans'),
	(25, 'resources/assets/images/category_images/1502346742.shoes.jpg', 'resources/assets/images/category_icons/1502283383.sneaker.png', 3, NULL, '2017-08-09 12:56:23', '2018-07-10 12:51:35', 'boy-shoes'),
	(26, 'resources/assets/images/category_images/1502346940.rompers.jpg', 'resources/assets/images/category_icons/1502283482.dress.png', 4, NULL, '2017-08-09 12:58:02', '2018-07-10 12:51:39', 'dresses-rompers'),
	(27, 'resources/assets/images/category_images/1502347098.skirts.jpg', 'resources/assets/images/category_icons/1502283595.short-skirt.png', 4, NULL, '2017-08-09 12:59:55', '2018-07-10 12:58:53', 'shorts-skirts'),
	(28, 'resources/assets/images/category_images/1502347329.sweater.jpg', 'resources/assets/images/category_icons/1502283706.pullover.png', 4, NULL, '2017-08-09 01:01:46', '2018-07-10 12:58:57', 'sweaters'),
	(29, 'resources/assets/images/category_images/1531492780.healthandbeauty.jpg', 'resources/assets/images/category_icons/1528806373.heart.png', 0, NULL, '2018-06-12 12:26:13', '2018-07-18 12:38:58', 'health-beauty-hair'),
	(30, 'resources/assets/images/category_images/1531492795.automobiles.jpg', 'resources/assets/images/category_icons/1528806436.car.png', 0, NULL, '2018-06-12 12:27:16', '2018-07-18 12:39:22', 'automobiles-motorcycles'),
	(31, 'resources/assets/images/category_images/1531492822.watches.jpg', 'resources/assets/images/category_icons/1528806495.wedding-ring.png', 0, NULL, '2018-06-12 12:28:15', '2018-07-18 12:39:35', 'jewelry-watches'),
	(32, 'resources/assets/images/category_images/1531492844.mobiles.jpg', 'resources/assets/images/category_icons/1528806529.mobile-phone.png', 0, NULL, '2018-06-12 12:28:49', '2018-07-18 12:39:47', 'cellphones-accessories'),
	(33, '', 'resources/assets/images/category_icons/1528806833.computer.png', 0, NULL, '2018-06-12 12:33:53', '2018-07-18 12:40:03', 'computer-office-security');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table shop.categories_description
CREATE TABLE IF NOT EXISTS `categories_description` (
  `categories_description_id` int(100) NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categories_description_id`),
  KEY `idx_categories_name` (`categories_name`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.categories_description: ~33 rows (approximately)
/*!40000 ALTER TABLE `categories_description` DISABLE KEYS */;
INSERT INTO `categories_description` (`categories_description_id`, `categories_id`, `language_id`, `categories_name`) VALUES
	(1, 1, 1, 'Men\'s Clothing'),
	(4, 2, 1, 'Women\'s Clothing'),
	(7, 3, 1, 'Boy\'s Clothing'),
	(10, 4, 1, 'Girl\'s Clothing'),
	(13, 5, 1, 'Baby & Mother'),
	(16, 6, 1, 'Household Merchandises'),
	(19, 7, 1, 'Men Polo shirts'),
	(22, 8, 1, 'Men Polo shirts'),
	(25, 9, 1, 'Men Jeans'),
	(28, 10, 1, 'Men Shoes'),
	(31, 11, 1, 'Sunglasses & Glasses'),
	(34, 12, 1, 'Women Dresses'),
	(37, 13, 1, 'Women Shirts & Tops'),
	(40, 14, 1, 'Women Jeans'),
	(43, 15, 1, 'Women Hand Bags'),
	(46, 16, 1, 'New Born'),
	(49, 17, 1, 'Baby Dresses'),
	(52, 18, 1, 'Baby Blankets & Swaddles'),
	(55, 19, 1, 'Bedding Collections'),
	(58, 20, 1, 'Throws & Pillows'),
	(61, 21, 1, 'Bath Robes'),
	(64, 22, 1, 'Boy Polo shirts'),
	(67, 23, 1, 'Boy Casual Shirts'),
	(70, 24, 1, 'Boy Pants & Jeans'),
	(73, 25, 1, 'Boy Shoes'),
	(76, 26, 1, 'Dresses & Rompers'),
	(79, 27, 1, 'Shorts & Skirts'),
	(82, 28, 1, 'Sweaters'),
	(85, 29, 1, 'Health & Beauty, Hair'),
	(88, 30, 1, 'Automobiles & Motorcycles'),
	(91, 31, 1, 'Jewelry & Watches'),
	(94, 32, 1, 'Cellphones & Accessories'),
	(97, 33, 1, 'Computer, Office, Security');
/*!40000 ALTER TABLE `categories_description` ENABLE KEYS */;

-- Dumping structure for table shop.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `address_format_id` int(11) NOT NULL,
  PRIMARY KEY (`countries_id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.countries: ~239 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`) VALUES
	(1, 'Afghanistan', 'AF', 'AFG', 1),
	(2, 'Albania', 'AL', 'ALB', 1),
	(3, 'Algeria', 'DZ', 'DZA', 1),
	(4, 'American Samoa', 'AS', 'ASM', 1),
	(5, 'Andorra', 'AD', 'AND', 1),
	(6, 'Angola', 'AO', 'AGO', 1),
	(7, 'Anguilla', 'AI', 'AIA', 1),
	(8, 'Antarctica', 'AQ', 'ATA', 1),
	(9, 'Antigua and Barbuda', 'AG', 'ATG', 1),
	(10, 'Argentina', 'AR', 'ARG', 1),
	(11, 'Armenia', 'AM', 'ARM', 1),
	(12, 'Aruba', 'AW', 'ABW', 1),
	(13, 'Australia', 'AU', 'AUS', 1),
	(14, 'Austria', 'AT', 'AUT', 5),
	(15, 'Azerbaijan', 'AZ', 'AZE', 1),
	(16, 'Bahamas', 'BS', 'BHS', 1),
	(17, 'Bahrain', 'BH', 'BHR', 1),
	(18, 'Bangladesh', 'BD', 'BGD', 1),
	(19, 'Barbados', 'BB', 'BRB', 1),
	(20, 'Belarus', 'BY', 'BLR', 1),
	(21, 'Belgium', 'BE', 'BEL', 1),
	(22, 'Belize', 'BZ', 'BLZ', 1),
	(23, 'Benin', 'BJ', 'BEN', 1),
	(24, 'Bermuda', 'BM', 'BMU', 1),
	(25, 'Bhutan', 'BT', 'BTN', 1),
	(26, 'Bolivia', 'BO', 'BOL', 1),
	(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1),
	(28, 'Botswana', 'BW', 'BWA', 1),
	(29, 'Bouvet Island', 'BV', 'BVT', 1),
	(30, 'Brazil', 'BR', 'BRA', 1),
	(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1),
	(32, 'Brunei Darussalam', 'BN', 'BRN', 1),
	(33, 'Bulgaria', 'BG', 'BGR', 1),
	(34, 'Burkina Faso', 'BF', 'BFA', 1),
	(35, 'Burundi', 'BI', 'BDI', 1),
	(36, 'Cambodia', 'KH', 'KHM', 1),
	(37, 'Cameroon', 'CM', 'CMR', 1),
	(38, 'Canada', 'CA', 'CAN', 1),
	(39, 'Cape Verde', 'CV', 'CPV', 1),
	(40, 'Cayman Islands', 'KY', 'CYM', 1),
	(41, 'Central African Republic', 'CF', 'CAF', 1),
	(42, 'Chad', 'TD', 'TCD', 1),
	(43, 'Chile', 'CL', 'CHL', 1),
	(44, 'China', 'CN', 'CHN', 1),
	(45, 'Christmas Island', 'CX', 'CXR', 1),
	(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1),
	(47, 'Colombia', 'CO', 'COL', 1),
	(48, 'Comoros', 'KM', 'COM', 1),
	(49, 'Congo', 'CG', 'COG', 1),
	(50, 'Cook Islands', 'CK', 'COK', 1),
	(51, 'Costa Rica', 'CR', 'CRI', 1),
	(52, 'Cote D\'Ivoire', 'CI', 'CIV', 1),
	(53, 'Croatia', 'HR', 'HRV', 1),
	(54, 'Cuba', 'CU', 'CUB', 1),
	(55, 'Cyprus', 'CY', 'CYP', 1),
	(56, 'Czech Republic', 'CZ', 'CZE', 1),
	(57, 'Denmark', 'DK', 'DNK', 1),
	(58, 'Djibouti', 'DJ', 'DJI', 1),
	(59, 'Dominica', 'DM', 'DMA', 1),
	(60, 'Dominican Republic', 'DO', 'DOM', 1),
	(61, 'East Timor', 'TP', 'TMP', 1),
	(62, 'Ecuador', 'EC', 'ECU', 1),
	(63, 'Egypt', 'EG', 'EGY', 1),
	(64, 'El Salvador', 'SV', 'SLV', 1),
	(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1),
	(66, 'Eritrea', 'ER', 'ERI', 1),
	(67, 'Estonia', 'EE', 'EST', 1),
	(68, 'Ethiopia', 'ET', 'ETH', 1),
	(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1),
	(70, 'Faroe Islands', 'FO', 'FRO', 1),
	(71, 'Fiji', 'FJ', 'FJI', 1),
	(72, 'Finland', 'FI', 'FIN', 1),
	(73, 'France', 'FR', 'FRA', 1),
	(74, 'France, Metropolitan', 'FX', 'FXX', 1),
	(75, 'French Guiana', 'GF', 'GUF', 1),
	(76, 'French Polynesia', 'PF', 'PYF', 1),
	(77, 'French Southern Territories', 'TF', 'ATF', 1),
	(78, 'Gabon', 'GA', 'GAB', 1),
	(79, 'Gambia', 'GM', 'GMB', 1),
	(80, 'Georgia', 'GE', 'GEO', 1),
	(81, 'Germany', 'DE', 'DEU', 5),
	(82, 'Ghana', 'GH', 'GHA', 1),
	(83, 'Gibraltar', 'GI', 'GIB', 1),
	(84, 'Greece', 'GR', 'GRC', 1),
	(85, 'Greenland', 'GL', 'GRL', 1),
	(86, 'Grenada', 'GD', 'GRD', 1),
	(87, 'Guadeloupe', 'GP', 'GLP', 1),
	(88, 'Guam', 'GU', 'GUM', 1),
	(89, 'Guatemala', 'GT', 'GTM', 1),
	(90, 'Guinea', 'GN', 'GIN', 1),
	(91, 'Guinea-bissau', 'GW', 'GNB', 1),
	(92, 'Guyana', 'GY', 'GUY', 1),
	(93, 'Haiti', 'HT', 'HTI', 1),
	(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1),
	(95, 'Honduras', 'HN', 'HND', 1),
	(96, 'Hong Kong', 'HK', 'HKG', 1),
	(97, 'Hungary', 'HU', 'HUN', 1),
	(98, 'Iceland', 'IS', 'ISL', 1),
	(99, 'India', 'IN', 'IND', 1),
	(100, 'Indonesia', 'ID', 'IDN', 1),
	(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1),
	(102, 'Iraq', 'IQ', 'IRQ', 1),
	(103, 'Ireland', 'IE', 'IRL', 1),
	(104, 'Israel', 'IL', 'ISR', 1),
	(105, 'Italy', 'IT', 'ITA', 1),
	(106, 'Jamaica', 'JM', 'JAM', 1),
	(107, 'Japan', 'JP', 'JPN', 1),
	(108, 'Jordan', 'JO', 'JOR', 1),
	(109, 'Kazakhstan', 'KZ', 'KAZ', 1),
	(110, 'Kenya', 'KE', 'KEN', 1),
	(111, 'Kiribati', 'KI', 'KIR', 1),
	(112, 'Korea, Democratic People\'s Republic of', 'KP', 'PRK', 1),
	(113, 'Korea, Republic of', 'KR', 'KOR', 1),
	(114, 'Kuwait', 'KW', 'KWT', 1),
	(115, 'Kyrgyzstan', 'KG', 'KGZ', 1),
	(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', 1),
	(117, 'Latvia', 'LV', 'LVA', 1),
	(118, 'Lebanon', 'LB', 'LBN', 1),
	(119, 'Lesotho', 'LS', 'LSO', 1),
	(120, 'Liberia', 'LR', 'LBR', 1),
	(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1),
	(122, 'Liechtenstein', 'LI', 'LIE', 1),
	(123, 'Lithuania', 'LT', 'LTU', 1),
	(124, 'Luxembourg', 'LU', 'LUX', 1),
	(125, 'Macau', 'MO', 'MAC', 1),
	(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 1),
	(127, 'Madagascar', 'MG', 'MDG', 1),
	(128, 'Malawi', 'MW', 'MWI', 1),
	(129, 'Malaysia', 'MY', 'MYS', 1),
	(130, 'Maldives', 'MV', 'MDV', 1),
	(131, 'Mali', 'ML', 'MLI', 1),
	(132, 'Malta', 'MT', 'MLT', 1),
	(133, 'Marshall Islands', 'MH', 'MHL', 1),
	(134, 'Martinique', 'MQ', 'MTQ', 1),
	(135, 'Mauritania', 'MR', 'MRT', 1),
	(136, 'Mauritius', 'MU', 'MUS', 1),
	(137, 'Mayotte', 'YT', 'MYT', 1),
	(138, 'Mexico', 'MX', 'MEX', 1),
	(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1),
	(140, 'Moldova, Republic of', 'MD', 'MDA', 1),
	(141, 'Monaco', 'MC', 'MCO', 1),
	(142, 'Mongolia', 'MN', 'MNG', 1),
	(143, 'Montserrat', 'MS', 'MSR', 1),
	(144, 'Morocco', 'MA', 'MAR', 1),
	(145, 'Mozambique', 'MZ', 'MOZ', 1),
	(146, 'Myanmar', 'MM', 'MMR', 1),
	(147, 'Namibia', 'NA', 'NAM', 1),
	(148, 'Nauru', 'NR', 'NRU', 1),
	(149, 'Nepal', 'NP', 'NPL', 1),
	(150, 'Netherlands', 'NL', 'NLD', 1),
	(151, 'Netherlands Antilles', 'AN', 'ANT', 1),
	(152, 'New Caledonia', 'NC', 'NCL', 1),
	(153, 'New Zealand', 'NZ', 'NZL', 1),
	(154, 'Nicaragua', 'NI', 'NIC', 1),
	(155, 'Niger', 'NE', 'NER', 1),
	(156, 'Nigeria', 'NG', 'NGA', 1),
	(157, 'Niue', 'NU', 'NIU', 1),
	(158, 'Norfolk Island', 'NF', 'NFK', 1),
	(159, 'Northern Mariana Islands', 'MP', 'MNP', 1),
	(160, 'Norway', 'NO', 'NOR', 1),
	(161, 'Oman', 'OM', 'OMN', 1),
	(162, 'Pakistan', 'PK', 'PAK', 1),
	(163, 'Palau', 'PW', 'PLW', 1),
	(164, 'Panama', 'PA', 'PAN', 1),
	(165, 'Papua New Guinea', 'PG', 'PNG', 1),
	(166, 'Paraguay', 'PY', 'PRY', 1),
	(167, 'Peru', 'PE', 'PER', 1),
	(168, 'Philippines', 'PH', 'PHL', 1),
	(169, 'Pitcairn', 'PN', 'PCN', 1),
	(170, 'Poland', 'PL', 'POL', 1),
	(171, 'Portugal', 'PT', 'PRT', 1),
	(172, 'Puerto Rico', 'PR', 'PRI', 1),
	(173, 'Qatar', 'QA', 'QAT', 1),
	(174, 'Reunion', 'RE', 'REU', 1),
	(175, 'Romania', 'RO', 'ROM', 1),
	(176, 'Russian Federation', 'RU', 'RUS', 1),
	(177, 'Rwanda', 'RW', 'RWA', 1),
	(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1),
	(179, 'Saint Lucia', 'LC', 'LCA', 1),
	(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1),
	(181, 'Samoa', 'WS', 'WSM', 1),
	(182, 'San Marino', 'SM', 'SMR', 1),
	(183, 'Sao Tome and Principe', 'ST', 'STP', 1),
	(184, 'Saudi Arabia', 'SA', 'SAU', 1),
	(185, 'Senegal', 'SN', 'SEN', 1),
	(186, 'Seychelles', 'SC', 'SYC', 1),
	(187, 'Sierra Leone', 'SL', 'SLE', 1),
	(188, 'Singapore', 'SG', 'SGP', 4),
	(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1),
	(190, 'Slovenia', 'SI', 'SVN', 1),
	(191, 'Solomon Islands', 'SB', 'SLB', 1),
	(192, 'Somalia', 'SO', 'SOM', 1),
	(193, 'South Africa', 'ZA', 'ZAF', 1),
	(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1),
	(195, 'Spain', 'ES', 'ESP', 3),
	(196, 'Sri Lanka', 'LK', 'LKA', 1),
	(197, 'St. Helena', 'SH', 'SHN', 1),
	(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1),
	(199, 'Sudan', 'SD', 'SDN', 1),
	(200, 'Suriname', 'SR', 'SUR', 1),
	(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1),
	(202, 'Swaziland', 'SZ', 'SWZ', 1),
	(203, 'Sweden', 'SE', 'SWE', 1),
	(204, 'Switzerland', 'CH', 'CHE', 1),
	(205, 'Syrian Arab Republic', 'SY', 'SYR', 1),
	(206, 'Taiwan', 'TW', 'TWN', 1),
	(207, 'Tajikistan', 'TJ', 'TJK', 1),
	(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1),
	(209, 'Thailand', 'TH', 'THA', 1),
	(210, 'Togo', 'TG', 'TGO', 1),
	(211, 'Tokelau', 'TK', 'TKL', 1),
	(212, 'Tonga', 'TO', 'TON', 1),
	(213, 'Trinidad and Tobago', 'TT', 'TTO', 1),
	(214, 'Tunisia', 'TN', 'TUN', 1),
	(215, 'Turkey', 'TR', 'TUR', 1),
	(216, 'Turkmenistan', 'TM', 'TKM', 1),
	(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1),
	(218, 'Tuvalu', 'TV', 'TUV', 1),
	(219, 'Uganda', 'UG', 'UGA', 1),
	(220, 'Ukraine', 'UA', 'UKR', 1),
	(221, 'United Arab Emirates', 'AE', 'ARE', 1),
	(222, 'United Kingdom', 'GB', 'GBR', 1),
	(223, 'United States', 'US', 'USA', 2),
	(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1),
	(225, 'Uruguay', 'UY', 'URY', 1),
	(226, 'Uzbekistan', 'UZ', 'UZB', 1),
	(227, 'Vanuatu', 'VU', 'VUT', 1),
	(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1),
	(229, 'Venezuela', 'VE', 'VEN', 1),
	(230, 'Viet Nam', 'VN', 'VNM', 1),
	(231, 'Virgin Islands (British)', 'VG', 'VGB', 1),
	(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1),
	(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1),
	(234, 'Western Sahara', 'EH', 'ESH', 1),
	(235, 'Yemen', 'YE', 'YEM', 1),
	(236, 'Yugoslavia', 'YU', 'YUG', 1),
	(237, 'Zaire', 'ZR', 'ZAR', 1),
	(238, 'Zambia', 'ZM', 'ZMB', 1),
	(239, 'Zimbabwe', 'ZW', 'ZWE', 1);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table shop.coupons
CREATE TABLE IF NOT EXISTS `coupons` (
  `coupans_id` int(100) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Options: fixed_cart, percent, fixed_product and percent_product. Default: fixed_cart.',
  `amount` int(11) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `usage_count` int(100) NOT NULL,
  `individual_use` tinyint(1) NOT NULL DEFAULT '0',
  `product_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exclude_product_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usage_limit` int(100) NOT NULL,
  `usage_limit_per_user` int(100) NOT NULL,
  `limit_usage_to_x_items` int(100) NOT NULL,
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `product_categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excluded_product_categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exclude_sale_items` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount` decimal(10,2) NOT NULL,
  `maximum_amount` decimal(10,2) NOT NULL,
  `email_restrictions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `used_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`coupans_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.coupons: 4 rows
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
INSERT INTO `coupons` (`coupans_id`, `code`, `date_created`, `date_modified`, `description`, `discount_type`, `amount`, `expiry_date`, `usage_count`, `individual_use`, `product_ids`, `exclude_product_ids`, `usage_limit`, `usage_limit_per_user`, `limit_usage_to_x_items`, `free_shipping`, `product_categories`, `excluded_product_categories`, `exclude_sale_items`, `minimum_amount`, `maximum_amount`, `email_restrictions`, `used_by`) VALUES
	(1, 'cart_discount_fixed', '2018-02-14 11:49:30', '2018-02-19 11:06:04', '', 'fixed_cart', 10, '2018-06-07 00:00:00', 9, 0, '', '', 10, 3, 0, 0, '', '', 0, 5.00, 1000.00, '', ',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1'),
	(2, 'cart_discount_percentage', '2018-02-14 11:50:20', '2018-02-16 14:57:19', '', 'percent', 10, '2018-04-04 00:00:00', 0, 0, '', '', 0, 0, 0, 0, '', '', 0, 5.00, 0.00, 'test@gmail.com', ',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1'),
	(3, 'product_discount_fixed', '2018-02-16 14:52:33', '2018-07-13 15:21:00', '', 'fixed_product', 5, '2021-03-03 00:00:00', 0, 0, '', '', 0, 0, 0, 0, '', '', 0, 0.00, 0.00, 'balawalali3387@gmail.com', ',1,41,42,46,46'),
	(4, 'product_discount_percentage', '2018-02-16 14:53:08', '2018-02-19 06:42:56', '', 'percent_product', 5, '1970-01-01 00:00:00', 0, 0, '', '', 0, 0, 0, 0, '', '', 0, 0.00, 0.00, '', ',1,1,1,1,1,7,7,7,7,7,7,1,20,30,30,32,32');
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;

-- Dumping structure for table shop.currencies
CREATE TABLE IF NOT EXISTS `currencies` (
  `currencies_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_left` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thousands_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_places` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float(13,8) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`currencies_id`),
  KEY `idx_currencies_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.currencies: ~2 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES
	(1, 'U.S. Dollar', 'USD', '$', NULL, '.', '.', '2', NULL, '2017-02-09 00:00:00'),
	(2, 'Euro', 'EUR', NULL, '€', '.', '.', '2', NULL, NULL);
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;

-- Dumping structure for table shop.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_gender` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `customers_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_default_address_id` int(11) DEFAULT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `customers_newsletter` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `fb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_picture` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(100) NOT NULL,
  `updated_at` int(100) NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customers_id`),
  KEY `idx_customers_email_address` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.customers: ~5 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`customers_id`, `customers_gender`, `customers_firstname`, `customers_lastname`, `customers_dob`, `email`, `user_name`, `customers_default_address_id`, `customers_telephone`, `customers_fax`, `password`, `customers_newsletter`, `isActive`, `fb_id`, `google_id`, `customers_picture`, `created_at`, `updated_at`, `is_seen`, `remember_token`) VALUES
	(1, '1', 'Yaw', 'Ya', '', 'mr.yawya@gmail.com', '', 1, '', '', '$2y$10$fCCbefUVV/zWHDLYzNP2Gu2k64TDvh2OqVM6jWaeiZb0hjWWhDJD.', NULL, 1, NULL, NULL, 'resources/assets/images/user_profile/1536305127.im.jpg', 18, 18, 1, 'zXTc8IR2SgOY1MSAUHgVAGxrxcSCoKYVf6XX0BYbaZXwjufQm7MqynJHv56z'),
	(2, '1', 'AyeChan', 'Thaw', '', 'ayechanthaw@gmail.com', '', NULL, '', NULL, '$2y$10$kvQqVuFwDq/y/JeoINxS1OYMnsJlU6hWaUXAULCYAZbjFH1wZss0S', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/1537460288.empty.jpg', 18, 18, 0, '6YbRMHPdhogvtYrLLkjdyDZOLfarAXtbqjOHt9bCfuGyKdOGltNYFWniquM5'),
	(3, '0', 'Maria', 'Hmue', '', 'mariahmue@gmail.com', '', NULL, '', NULL, '$2y$10$kOyzxm2071uZ.PFlIBGLQOdHzHNTIdn4mgre5GFrP/tjBs80bWuMO', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/default_user.png', 18, 18, 0, 'xinnKbYaxGcAbWesy50RTDabEFu7q8EmHmlTw5tXl0mcd6aqEeGD6iNYoFMl'),
	(4, '0', 'Yan Hmue', 'Aung', '', 'yanhmueaung@gmail.com', '', NULL, '', NULL, '$2y$10$G1ia76ATpPSwch6U28oyMe0mGNgoeZphCKPsOU1VzQ26QfZ2RCdRe', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/default_user.png', 18, 18, 0, '8a95qDGGVXDOJt6ccDj0jKmTOHfUDLqFMVHpHISDApb5xE9lgtR4s8f6PBFZ'),
	(5, '0', 'Khant Zin', 'Win', '', 'khantzinwin@gmail.com', '', NULL, '', NULL, '$2y$10$5P4.8D0AHhbZlB/r2fjX3e5/xpykYQ.PFOLUaAFuERdK85U.xs.Hy', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/default_user.png', 18, 18, 0, 'FFWNWbCTtsdpeXGhD5nnzUacgkFgD3uLzuNH3nWbzt5PwEzzrKbTdfqdpIRD');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table shop.customers_basket
CREATE TABLE IF NOT EXISTS `customers_basket` (
  `customers_basket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `products_id` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_basket_quantity` int(2) NOT NULL,
  `final_price` decimal(15,2) DEFAULT NULL,
  `customers_basket_date_added` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_order` tinyint(1) NOT NULL DEFAULT '0',
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customers_basket_id`),
  KEY `idx_customers_basket_customers_id` (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.customers_basket: ~9 rows (approximately)
/*!40000 ALTER TABLE `customers_basket` DISABLE KEYS */;
INSERT INTO `customers_basket` (`customers_basket_id`, `customers_id`, `products_id`, `customers_basket_quantity`, `final_price`, `customers_basket_date_added`, `is_order`, `session_id`) VALUES
	(1, 1, '8', 1, 125.50, '2018-09-07', 1, '4pFTJTfN6v85EAaEVJJ2Mrz1VAc7g1XNtptggWuy'),
	(2, 1, '1', 1, 85.00, '2018-09-07', 1, 'qeUWokmLH5j7UI3Y3DLKZZa9PQUVzEybn1oqK59u'),
	(5, 1, '80', 1, 4099.99, '2018-09-07', 0, 'kxmalkoPCtS7qDiGihiq4kQmaqHCLD3GMxRkd0oo'),
	(6, 0, '80', 1, 99.99, '2018-09-07', 0, '01vsLEPfp9OL9a2Hia313ZOV2SKhOBad7oCG4Yuu'),
	(7, 0, '77', 2, 56.50, '2018-09-07', 0, '01vsLEPfp9OL9a2Hia313ZOV2SKhOBad7oCG4Yuu'),
	(8, 0, '79', 3, 45.00, '2018-09-07', 0, '01vsLEPfp9OL9a2Hia313ZOV2SKhOBad7oCG4Yuu'),
	(9, 1, '4', 1, 85.00, '2018-09-07', 0, 'f1oyJmgZvhEBU2NXaYkAPpHXevLzQcwkQ3JgbyPT'),
	(10, 0, '8', 1, 125.50, '2018-09-07', 0, '2V6WsAbSaeDqfIEOsgAxOpT2JKXtecqFneCocPaR'),
	(11, 1, '13', 1, 125.00, '2018-09-08', 0, 'CSS61zssD7NoX2lH0izLscrd8AUroWNHM8Vrn2z2'),
	(12, 0, '81', 1, 49.50, '2018-09-15', 0, 'jTq4upr2VQ4RxowDF2ncKQcPsPR4utHsLKIcxbaJ');
/*!40000 ALTER TABLE `customers_basket` ENABLE KEYS */;

-- Dumping structure for table shop.customers_basket_attributes
CREATE TABLE IF NOT EXISTS `customers_basket_attributes` (
  `customers_basket_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_basket_id` int(100) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `products_id` text COLLATE utf8_unicode_ci NOT NULL,
  `products_options_id` int(11) NOT NULL,
  `products_options_values_id` int(11) NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customers_basket_attributes_id`),
  KEY `idx_customers_basket_att_customers_id` (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.customers_basket_attributes: ~10 rows (approximately)
/*!40000 ALTER TABLE `customers_basket_attributes` DISABLE KEYS */;
INSERT INTO `customers_basket_attributes` (`customers_basket_attributes_id`, `customers_basket_id`, `customers_id`, `products_id`, `products_options_id`, `products_options_values_id`, `session_id`) VALUES
	(1, 1, 1, '8', 4, 29, '4pFTJTfN6v85EAaEVJJ2Mrz1VAc7g1XNtptggWuy'),
	(2, 2, 1, '1', 1, 1, 'qeUWokmLH5j7UI3Y3DLKZZa9PQUVzEybn1oqK59u'),
	(3, 2, 1, '1', 4, 29, 'qeUWokmLH5j7UI3Y3DLKZZa9PQUVzEybn1oqK59u'),
	(6, 5, 1, '80', 1, 9, 'kxmalkoPCtS7qDiGihiq4kQmaqHCLD3GMxRkd0oo'),
	(7, 6, 0, '80', 1, 155, '01vsLEPfp9OL9a2Hia313ZOV2SKhOBad7oCG4Yuu'),
	(8, 6, 0, '80', 4, 29, '01vsLEPfp9OL9a2Hia313ZOV2SKhOBad7oCG4Yuu'),
	(9, 9, 1, '4', 4, 29, 'f1oyJmgZvhEBU2NXaYkAPpHXevLzQcwkQ3JgbyPT'),
	(10, 10, 0, '8', 4, 29, '2V6WsAbSaeDqfIEOsgAxOpT2JKXtecqFneCocPaR'),
	(11, 11, 1, '13', 7, 41, 'CSS61zssD7NoX2lH0izLscrd8AUroWNHM8Vrn2z2'),
	(12, 11, 1, '13', 10, 65, 'CSS61zssD7NoX2lH0izLscrd8AUroWNHM8Vrn2z2');
/*!40000 ALTER TABLE `customers_basket_attributes` ENABLE KEYS */;

-- Dumping structure for table shop.customers_info
CREATE TABLE IF NOT EXISTS `customers_info` (
  `customers_info_id` int(11) NOT NULL,
  `customers_info_date_of_last_logon` datetime DEFAULT NULL,
  `customers_info_number_of_logons` int(5) DEFAULT NULL,
  `customers_info_date_account_created` datetime DEFAULT NULL,
  `customers_info_date_account_last_modified` datetime DEFAULT NULL,
  `global_product_notifications` int(1) DEFAULT '0',
  PRIMARY KEY (`customers_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.customers_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `customers_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_info` ENABLE KEYS */;

-- Dumping structure for table shop.devices
CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `device_id` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_id` int(100) NOT NULL DEFAULT '0',
  `device_type` text COLLATE utf8_unicode_ci NOT NULL,
  `register_date` int(100) NOT NULL DEFAULT '0',
  `update_date` int(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `isDesktop` tinyint(1) NOT NULL DEFAULT '0',
  `onesignal` tinyint(1) NOT NULL DEFAULT '0',
  `isEnableMobile` tinyint(1) NOT NULL DEFAULT '1',
  `isEnableDesktop` tinyint(1) NOT NULL DEFAULT '1',
  `ram` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_os` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_notify` tinyint(1) NOT NULL DEFAULT '1',
  `fcm` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.devices: 0 rows
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;

-- Dumping structure for table shop.fedex_shipping
CREATE TABLE IF NOT EXISTS `fedex_shipping` (
  `fedex_id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_height` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_width` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `person_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_package` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fedex_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.fedex_shipping: 1 rows
/*!40000 ALTER TABLE `fedex_shipping` DISABLE KEYS */;
INSERT INTO `fedex_shipping` (`fedex_id`, `title`, `user_name`, `password`, `parcel_height`, `parcel_width`, `person_name`, `company_name`, `phone_number`, `address_line_1`, `address_line_2`, `country`, `state`, `post_code`, `city`, `no_of_package`) VALUES
	(1, 'FedEx', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `fedex_shipping` ENABLE KEYS */;

-- Dumping structure for table shop.flate_rate
CREATE TABLE IF NOT EXISTS `flate_rate` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `flate_rate` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.flate_rate: 1 rows
/*!40000 ALTER TABLE `flate_rate` DISABLE KEYS */;
INSERT INTO `flate_rate` (`id`, `flate_rate`, `currency`) VALUES
	(1, '11', 'USD');
/*!40000 ALTER TABLE `flate_rate` ENABLE KEYS */;

-- Dumping structure for table shop.geo_zones
CREATE TABLE IF NOT EXISTS `geo_zones` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `geo_zone_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.geo_zones: ~0 rows (approximately)
/*!40000 ALTER TABLE `geo_zones` DISABLE KEYS */;
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES
	(1, 'Florida', 'Florida local sales tax zone', '2017-01-10 00:00:00', '2017-01-11 00:00:00');
/*!40000 ALTER TABLE `geo_zones` ENABLE KEYS */;

-- Dumping structure for table shop.hula_our_infos
CREATE TABLE IF NOT EXISTS `hula_our_infos` (
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.hula_our_infos: ~0 rows (approximately)
/*!40000 ALTER TABLE `hula_our_infos` DISABLE KEYS */;
/*!40000 ALTER TABLE `hula_our_infos` ENABLE KEYS */;

-- Dumping structure for table shop.labels
CREATE TABLE IF NOT EXISTS `labels` (
  `label_id` int(100) NOT NULL AUTO_INCREMENT,
  `label_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`label_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1047 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.labels: 175 rows
/*!40000 ALTER TABLE `labels` DISABLE KEYS */;
INSERT INTO `labels` (`label_id`, `label_name`) VALUES
	(2, 'Creating an account means you’re okay with shopify\'s Terms of Service, Privacy Policy'),
	(1031, 'Home'),
	(1, 'I\'ve forgotten my password?'),
	(1030, 'Menu'),
	(1029, 'Clear'),
	(1028, 'Apply'),
	(1027, 'Close'),
	(1026, 'Price Range'),
	(1025, 'Filters'),
	(1024, 'My Wish List'),
	(1023, 'Log Out'),
	(1022, 'Please login or create an account for free'),
	(1021, 'Login & Register'),
	(1020, 'Save Address'),
	(1018, 'State'),
	(1019, 'Update Address'),
	(1017, 'Post code'),
	(1016, 'City'),
	(1015, 'Zone'),
	(1014, 'other'),
	(1013, 'Country'),
	(1012, 'Shipping Address'),
	(1011, 'Proceed'),
	(1010, 'Remove'),
	(1008, 'by'),
	(1009, 'View'),
	(1007, 'Quantity'),
	(1006, 'Price'),
	(1005, 'continue shopping'),
	(1004, 'Your cart is empty'),
	(1003, 'My Cart'),
	(1002, 'Continue'),
	(1001, 'Error: invalid cvc number!'),
	(1000, 'Error: invalid expiry date!'),
	(999, 'Error: invalid card number!'),
	(998, 'Expiration'),
	(997, 'Expiration Date'),
	(996, 'Card Number'),
	(995, 'Payment'),
	(994, 'Order Notes'),
	(993, 'Shipping Cost'),
	(992, 'Tax'),
	(991, 'Products Price'),
	(990, 'SubTotal'),
	(989, 'Products'),
	(988, 'Shipping Method'),
	(987, 'Billing Address'),
	(986, 'Order'),
	(985, 'Next'),
	(984, 'Same as Shipping Address'),
	(981, 'Billing Info'),
	(982, 'Address'),
	(983, 'Phone'),
	(980, 'Already Memeber?'),
	(979, 'Last Name'),
	(978, 'First Name'),
	(977, 'Create an Account'),
	(976, 'Add new Address'),
	(975, 'Please add your new shipping address for the futher processing of the your order'),
	(969, 'Order Status'),
	(970, 'Orders ID'),
	(971, 'Product Price'),
	(972, 'No. of Products'),
	(973, 'Date'),
	(974, 'Customer Address'),
	(968, 'Customer Orders'),
	(967, 'Change Password'),
	(966, 'New Password'),
	(965, 'Current Password'),
	(964, 'Update'),
	(963, 'Date of Birth'),
	(962, 'Mobile'),
	(961, 'My Account'),
	(960, 'Likes'),
	(959, 'newest'),
	(958, 'top seller'),
	(957, 'special'),
	(956, 'most liked'),
	(955, 'Cancel'),
	(954, 'Sort Products'),
	(953, 'Special Products'),
	(952, 'Price : low - high'),
	(951, 'Price : high - low'),
	(950, 'Z - A'),
	(949, 'A - Z'),
	(948, 'All'),
	(947, 'Explore More'),
	(946, 'Note to the buyer'),
	(945, 'Coupon'),
	(944, 'coupon code'),
	(943, 'Coupon Amount'),
	(942, 'Coupon Code'),
	(941, 'Food Categories'),
	(940, 'Recipe of Day'),
	(939, 'Top Dishes'),
	(938, 'Skip'),
	(937, 'Term and Services'),
	(936, 'Privacy Policy'),
	(935, 'Refund Policy'),
	(934, 'Newest'),
	(933, 'OUT OF STOCK'),
	(932, 'Select Language'),
	(931, 'Reset'),
	(930, 'Shop'),
	(929, 'Settings'),
	(928, 'Enter keyword'),
	(927, 'News'),
	(926, 'Top Sellers'),
	(925, 'Go Back'),
	(924, 'Word Press Post Detail'),
	(923, 'Explore'),
	(922, 'Continue Adding'),
	(921, 'Your wish List is empty'),
	(920, 'Favourite'),
	(919, 'Continue Shopping'),
	(918, 'My Orders'),
	(917, 'Thank you for shopping with us.'),
	(916, 'Thank You'),
	(915, 'Shipping method'),
	(914, 'Sub Categories'),
	(913, 'Main Categories'),
	(912, 'Search'),
	(911, 'Reset Filters'),
	(910, 'No Products Found'),
	(909, 'OFF'),
	(908, 'Techincal details'),
	(907, 'Product Description'),
	(906, 'ADD TO CART'),
	(905, 'Add to Cart'),
	(904, 'In Stock'),
	(903, 'Out of Stock'),
	(902, 'New'),
	(901, 'Product Details'),
	(900, 'Shipping'),
	(899, 'Sub Total'),
	(898, 'Total'),
	(897, 'Price Detail'),
	(896, 'Order Detail'),
	(895, 'Got It!'),
	(894, 'Skip Intro'),
	(893, 'Intro'),
	(892, 'REMOVE'),
	(891, 'Deals'),
	(890, 'All Categories'),
	(889, 'Most Liked'),
	(888, 'Special Deals'),
	(887, 'Top Seller'),
	(886, 'Products are available.'),
	(885, 'Recently Viewed'),
	(884, 'Please connect to the internet'),
	(881, 'Contact Us'),
	(882, 'Name'),
	(883, 'Your Messsage'),
	(880, 'Categories'),
	(879, 'About Us'),
	(878, 'Send'),
	(877, 'Forgot Password'),
	(876, 'Register'),
	(875, 'Password'),
	(874, 'Email'),
	(873, 'or'),
	(872, 'Login with'),
	(1033, 'Creating an account means you’re okay with our'),
	(1034, 'Login'),
	(1035, 'Turn on/off Local Notifications'),
	(1036, 'Turn on/off Notifications'),
	(1037, 'Change Language'),
	(1038, 'Official Website'),
	(1039, 'Rate Us'),
	(1040, 'Share'),
	(1041, 'Edit Profile'),
	(1042, 'A percentage discount for the entire cart'),
	(1043, 'A fixed total discount for the entire cart'),
	(1044, 'A fixed total discount for selected products only'),
	(1045, 'A percentage discount for selected products only');
/*!40000 ALTER TABLE `labels` ENABLE KEYS */;

-- Dumping structure for table shop.label_value
CREATE TABLE IF NOT EXISTS `label_value` (
  `label_value_id` int(100) NOT NULL AUTO_INCREMENT,
  `label_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(100) DEFAULT NULL,
  `label_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`label_value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1501 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.label_value: 176 rows
/*!40000 ALTER TABLE `label_value` DISABLE KEYS */;
INSERT INTO `label_value` (`label_value_id`, `label_value`, `language_id`, `label_id`) VALUES
	(1372, 'Most Liked', 1, 956),
	(1371, 'Special', 1, 957),
	(1370, 'Top Seller', 1, 958),
	(1369, 'Newest ', 1, 959),
	(1368, 'Likes', 1, 960),
	(1366, 'Mobile', 1, 962),
	(1367, 'My Account', 1, 961),
	(1365, 'Date of Birth', 1, 963),
	(1364, 'Update', 1, 964),
	(1355, 'Orders ID', 1, 970),
	(1356, 'Product Price', 1, 971),
	(1357, 'No. of Products', 1, 972),
	(1358, 'Date', 1, 973),
	(1359, 'Customer Address', 1, 974),
	(1360, 'Customer Orders', 1, 968),
	(1361, 'Change Password', 1, 967),
	(1362, 'New Password', 1, 966),
	(1363, 'Current Password', 1, 965),
	(1354, 'Order Status', 1, 969),
	(1353, 'Please add your new shipping address for the futher processing of the your order', 1, 975),
	(1352, 'Add new Address', 1, 976),
	(1351, 'Create an Account', 1, 977),
	(1350, 'First Name', 1, 978),
	(1349, 'Last Name', 1, 979),
	(1348, 'Already Memeber?', 1, 980),
	(1341, 'Billing Address', 1, 987),
	(1342, 'Order', 1, 986),
	(1343, 'Next', 1, 985),
	(1344, 'Same as Shipping Address', 1, 984),
	(1345, 'Billing Info', 1, 981),
	(1346, 'Address', 1, 982),
	(1347, 'Phone', 1, 983),
	(1339, 'Products', 1, 989),
	(1340, 'Shipping Method', 1, 988),
	(1334, 'Order Notes', 1, 994),
	(1335, 'Shipping Cost', 1, 993),
	(1336, 'Tax', 1, 992),
	(1337, 'Products Price', 1, 991),
	(1338, 'SubTotal', 1, 990),
	(1333, 'Payment', 1, 995),
	(1332, 'Card Number', 1, 996),
	(1331, 'Expiration Date', 1, 997),
	(1330, 'Expiration', 1, 998),
	(1329, 'Error: invalid card number!', 1, 999),
	(1328, 'Error: invalid expiry date!', 1, 1000),
	(1327, 'Error: invalid cvc number!', 1, 1001),
	(1326, 'Continue', 1, 1002),
	(1325, 'My Cart', 1, 1003),
	(1324, 'Your cart is empty', 1, 1004),
	(1323, 'continue shopping', 1, 1005),
	(1322, 'Price', 1, 1006),
	(1318, 'Remove', 1, 1010),
	(1319, 'by', 1, 1008),
	(1320, 'View', 1, 1009),
	(1321, 'Quantity', 1, 1007),
	(1317, 'Proceed', 1, 1011),
	(1315, 'Country', 1, 1013),
	(1316, 'Shipping Address', 1, 1012),
	(1313, 'Zone', 1, 1015),
	(1314, 'other', 1, 1014),
	(1311, 'Post code', 1, 1017),
	(1312, 'City', 1, 1016),
	(1309, 'State', 1, 1018),
	(1310, 'Update Address', 1, 1019),
	(1307, 'login & Register', 1, 1021),
	(1308, 'Save Address', 1, 1020),
	(1306, 'Please login or create an account for free', 1, 1022),
	(1305, 'Log Out', 1, 1023),
	(1304, 'My Wish List', 1, 1024),
	(1303, 'Filters', 1, 1025),
	(1302, 'Price Range', 1, 1026),
	(1301, 'Close', 1, 1027),
	(1299, 'Clear', 1, 1029),
	(1300, 'Apply', 1, 1028),
	(1298, 'Menu', 1, 1030),
	(1297, 'Home', 1, 1031),
	(1373, 'Cancel', 1, 955),
	(1374, 'Sort Products', 1, 954),
	(1375, 'Special Products', 1, 953),
	(1376, 'Price : low - high', 1, 952),
	(1377, 'Price : high - low', 1, 951),
	(1378, 'Z - A', 1, 950),
	(1379, 'A - Z', 1, 949),
	(1380, 'All', 1, 948),
	(1381, 'Explore More', 1, 947),
	(1382, 'Note to the buyer', 1, 946),
	(1383, 'Coupon', 1, 945),
	(1384, 'coupon code', 1, 944),
	(1385, 'Coupon Amount', 1, 943),
	(1386, 'Coupon Code', 1, 942),
	(1387, 'Food Categories', 1, 941),
	(1388, 'Recipe of Day', 1, 940),
	(1389, 'Top Dishes', 1, 939),
	(1390, 'Skip', 1, 938),
	(1391, 'Term and Services', 1, 937),
	(1392, 'Privacy Policy', 1, 936),
	(1393, 'Refund Policy', 1, 935),
	(1394, 'Newest', 1, 934),
	(1395, 'OUT OF STOCK', 1, 933),
	(1396, 'Select Language', 1, 932),
	(1397, 'Reset', 1, 931),
	(1398, 'Shop', 1, 930),
	(1399, 'Settings', 1, 929),
	(1400, 'Enter keyword', 1, 928),
	(1401, 'News', 1, 927),
	(1402, 'Top Sellers', 1, 926),
	(1403, 'Go Back', 1, 925),
	(1404, 'Word Press Post Detail', 1, 924),
	(1405, 'Explore', 1, 923),
	(1406, 'Continue Adding', 1, 922),
	(1407, 'Your wish List is empty', 1, 921),
	(1408, 'Favourite', 1, 920),
	(1409, 'Continue Shopping', 1, 919),
	(1410, 'My Orders', 1, 918),
	(1411, 'Thank you for shopping with us.', 1, 917),
	(1412, 'Thank You', 1, 916),
	(1413, 'Shipping method', 1, 915),
	(1414, 'Sub Categories', 1, 914),
	(1415, 'Main Categories', 1, 913),
	(1416, 'Search', 1, 912),
	(1417, 'Reset Filters', 1, 911),
	(1418, 'No Products Found', 1, 910),
	(1419, 'OFF', 1, 909),
	(1420, 'Techincal details', 1, 908),
	(1421, 'Product Description', 1, 907),
	(1422, 'ADD TO CART', 1, 906),
	(1423, 'Add to Cart', 1, 905),
	(1424, 'In Stock', 1, 904),
	(1425, 'Out of Stock', 1, 903),
	(1426, 'New', 1, 902),
	(1427, 'Product Details', 1, 901),
	(1428, 'Shipping', 1, 900),
	(1429, 'Sub Total', 1, 899),
	(1430, 'Total', 1, 898),
	(1431, 'Price Detail', 1, 897),
	(1432, 'Order Detail', 1, 896),
	(1433, 'Got It!', 1, 895),
	(1434, 'Skip Intro', 1, 894),
	(1435, 'Intro', 1, 893),
	(1436, 'REMOVE', 1, 892),
	(1437, 'Deals', 1, 891),
	(1438, 'All Categories', 1, 890),
	(1439, 'Most Liked', 1, 889),
	(1440, 'Special Deals', 1, 888),
	(1441, 'Top Seller', 1, 887),
	(1442, 'Products are available.', 1, 886),
	(1443, 'Recently Viewed', 1, 885),
	(1444, 'Please connect to the internet', 1, 884),
	(1445, 'Contact Us', 1, 881),
	(1446, 'Name', 1, 882),
	(1447, 'Your Message', 1, 883),
	(1448, 'Categories', 1, 880),
	(1449, 'About Us', 1, 879),
	(1450, 'Send', 1, 878),
	(1451, 'Forgot Password', 1, 877),
	(1452, 'Register', 1, 876),
	(1453, 'Password', 1, 875),
	(1454, 'Email', 1, 874),
	(1455, 'or', 1, 873),
	(1456, 'Login with', 1, 872),
	(1457, 'Creating an account means you\'re okay with shopify\'s Terms of Service, Privacy Policy', 1, 2),
	(1458, 'I\'ve forgotten my password?', 1, 1),
	(1459, NULL, 1, NULL),
	(1462, 'Creating an account means you’re okay with our', 1, 1033),
	(1465, 'Login', 1, 1034),
	(1468, 'Turn on/off Local Notifications', 1, 1035),
	(1471, 'Turn on/off Notifications', 1, 1036),
	(1474, 'Change Language', 1, 1037),
	(1477, 'Official Website', 1, 1038),
	(1480, 'Rate Us', 1, 1039),
	(1483, 'Share', 1, 1040),
	(1486, 'Edit Profile', 1, 1041),
	(1489, 'A percentage discount for the entire cart', 1, 1042),
	(1492, 'A fixed total discount for the entire cart', 1, 1043),
	(1495, 'A fixed total discount for selected products only', 1, 1044),
	(1498, 'A percentage discount for selected products only', 1, 1045);
/*!40000 ALTER TABLE `label_value` ENABLE KEYS */;

-- Dumping structure for table shop.languages
CREATE TABLE IF NOT EXISTS `languages` (
  `languages_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `image` mediumtext COLLATE utf8_unicode_ci,
  `directory` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `direction` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`languages_id`),
  KEY `IDX_LANGUAGES_NAME` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.languages: ~2 rows (approximately)
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `direction`, `is_default`) VALUES
	(1, 'English', 'en', 'resources/assets/images/language_flags/1486556365.503984030_english.jpg', 'english', 1, 'ltr', 1),
	(5, 'ျမန္မာ', 'MY', '', '', NULL, 'rtl', 0);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

-- Dumping structure for table shop.liked_products
CREATE TABLE IF NOT EXISTS `liked_products` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `liked_products_id` int(100) NOT NULL,
  `liked_customers_id` int(100) NOT NULL,
  `date_liked` datetime DEFAULT NULL,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.liked_products: ~84 rows (approximately)
/*!40000 ALTER TABLE `liked_products` DISABLE KEYS */;
INSERT INTO `liked_products` (`like_id`, `liked_products_id`, `liked_customers_id`, `date_liked`) VALUES
	(3, 10, 1, '2018-01-23 06:44:30'),
	(5, 8, 1, '2018-01-23 07:02:38'),
	(32, 78, 1, '2018-01-24 10:48:06'),
	(41, 76, 1, '2018-03-21 14:24:21'),
	(42, 75, 1, '2018-03-21 14:24:22'),
	(43, 77, 1, '2018-03-21 14:24:23'),
	(44, 74, 1, '2018-03-21 14:24:25'),
	(45, 73, 1, '2018-03-21 14:24:26'),
	(46, 72, 1, '2018-03-21 14:24:28'),
	(48, 70, 1, '2018-03-21 14:24:30'),
	(49, 71, 1, '2018-03-21 14:24:31'),
	(50, 64, 1, '2018-03-21 14:24:39'),
	(51, 65, 1, '2018-03-21 14:24:40'),
	(52, 63, 1, '2018-03-21 14:24:41'),
	(54, 61, 1, '2018-03-21 14:24:43'),
	(55, 62, 1, '2018-03-21 14:24:44'),
	(56, 67, 1, '2018-03-21 14:25:19'),
	(57, 68, 1, '2018-03-21 14:25:20'),
	(58, 66, 1, '2018-03-21 14:25:21'),
	(68, 48, 1, '2018-03-21 14:25:39'),
	(81, 13, 1, '2018-03-26 12:58:22'),
	(82, 82, 1, '2018-03-26 13:05:27'),
	(86, 1, 1, '2018-05-09 15:37:00'),
	(87, 4, 1, '2018-05-21 10:22:50'),
	(89, 17, 12, '2018-05-21 11:51:52'),
	(90, 15, 1, '2018-05-21 12:37:29'),
	(93, 43, 9, '2018-05-22 10:36:16'),
	(94, 44, 9, '2018-05-22 10:36:17'),
	(95, 48, 9, '2018-05-22 10:36:18'),
	(96, 62, 9, '2018-05-22 10:36:20'),
	(97, 39, 9, '2018-05-22 10:36:21'),
	(101, 77, 9, '2018-05-22 10:36:29'),
	(102, 72, 9, '2018-05-22 10:36:32'),
	(103, 73, 9, '2018-05-22 10:36:33'),
	(104, 76, 9, '2018-05-22 10:36:57'),
	(105, 75, 9, '2018-05-22 10:36:58'),
	(106, 74, 9, '2018-05-22 10:37:00'),
	(107, 65, 9, '2018-05-22 10:37:44'),
	(108, 67, 9, '2018-05-22 10:37:46'),
	(109, 70, 9, '2018-05-22 10:37:47'),
	(111, 8, 9, '2018-05-22 10:37:59'),
	(112, 1, 9, '2018-05-22 10:38:00'),
	(113, 13, 9, '2018-05-22 11:00:27'),
	(114, 2, 9, '2018-05-24 09:50:25'),
	(115, 17, 9, '2018-05-25 10:39:18'),
	(118, 17, 20, '2018-06-13 11:45:31'),
	(119, 70, 20, '2018-06-13 11:45:42'),
	(120, 62, 20, '2018-06-13 11:45:45'),
	(121, 81, 20, '2018-06-13 11:45:46'),
	(122, 6, 26, '2018-06-13 13:07:31'),
	(123, 5, 26, '2018-06-13 13:07:34'),
	(125, 4, 29, '2018-06-21 12:52:24'),
	(126, 8, 32, '2018-06-26 13:58:50'),
	(128, 4, 32, '2018-06-26 13:58:55'),
	(129, 77, 32, '2018-06-26 13:59:00'),
	(138, 83, 32, '2018-06-26 15:19:24'),
	(140, 48, 32, '2018-06-27 13:08:29'),
	(143, 73, 32, '2018-06-28 15:10:49'),
	(145, 70, 32, '2018-06-28 15:10:52'),
	(146, 67, 32, '2018-06-28 15:10:54'),
	(147, 65, 32, '2018-06-28 15:10:55'),
	(148, 62, 32, '2018-06-28 15:10:57'),
	(149, 44, 32, '2018-06-28 15:11:00'),
	(150, 43, 32, '2018-06-28 15:11:01'),
	(152, 82, 32, '2018-06-28 15:11:08'),
	(153, 81, 32, '2018-06-28 15:11:10'),
	(154, 79, 32, '2018-06-28 15:11:13'),
	(155, 78, 32, '2018-06-28 15:11:16'),
	(157, 74, 32, '2018-06-28 15:11:49'),
	(158, 75, 32, '2018-06-28 15:11:50'),
	(159, 76, 32, '2018-06-28 15:11:52'),
	(160, 2, 32, '2018-06-28 15:12:54'),
	(161, 5, 32, '2018-06-28 15:12:55'),
	(162, 6, 32, '2018-06-28 15:12:58'),
	(163, 7, 32, '2018-06-28 15:13:01'),
	(164, 9, 32, '2018-06-28 15:13:03'),
	(165, 1, 32, '2018-07-03 11:38:19'),
	(166, 17, 41, '2018-07-16 14:53:24'),
	(168, 8, 47, '2018-07-17 15:40:11'),
	(169, 73, 47, '2018-07-17 15:53:44'),
	(170, 13, 32, '2018-07-17 16:07:55'),
	(174, 80, 32, '2018-07-18 13:42:03'),
	(175, 17, 32, '2018-07-18 13:42:09'),
	(178, 69, 1, '2018-09-08 02:40:10');
/*!40000 ALTER TABLE `liked_products` ENABLE KEYS */;

-- Dumping structure for table shop.manufacturers
CREATE TABLE IF NOT EXISTS `manufacturers` (
  `manufacturers_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturers_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturers_image` mediumtext COLLATE utf8_unicode_ci,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `manufacturers_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturers_id`),
  KEY `IDX_MANUFACTURERS_NAME` (`manufacturers_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.manufacturers: ~6 rows (approximately)
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` (`manufacturers_id`, `manufacturers_name`, `manufacturers_image`, `date_added`, `last_modified`, `manufacturers_slug`, `vendor_id`) VALUES
	(1, 'Shwe', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 4),
	(2, 'DG', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 5),
	(3, 'Charles & Kidth', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 6),
	(4, 'Levis', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 4),
	(5, 'Acer', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 5),
	(6, 'Dell', 'resources/assets/images/manufacturers_images/1536310170.05.png', '2018-09-07 08:49:30', NULL, 'shwe', 6);
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;

-- Dumping structure for table shop.manufacturers_info
CREATE TABLE IF NOT EXISTS `manufacturers_info` (
  `manufacturers_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `manufacturers_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  PRIMARY KEY (`manufacturers_id`,`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.manufacturers_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturers_info` DISABLE KEYS */;
INSERT INTO `manufacturers_info` (`manufacturers_id`, `languages_id`, `manufacturers_url`, `url_clicked`, `date_last_click`) VALUES
	(1, 1, '', 0, NULL);
/*!40000 ALTER TABLE `manufacturers_info` ENABLE KEYS */;

-- Dumping structure for table shop.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.migrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table shop.news
CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_image` mediumtext COLLATE utf8_unicode_ci,
  `news_date_added` datetime NOT NULL,
  `news_last_modified` datetime DEFAULT NULL,
  `news_status` tinyint(1) NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `news_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`news_id`),
  KEY `idx_products_date_added` (`news_date_added`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.news: ~41 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`news_id`, `news_image`, `news_date_added`, `news_last_modified`, `news_status`, `is_feature`, `news_slug`) VALUES
	(6, 'resources/assets/images/news_images/1531930680.blog-img-1.jpg', '2017-08-22 06:36:32', '2018-07-18 04:18:00', 1, 1, 'tools-and-technology'),
	(7, 'resources/assets/images/news_images/1531930697.blog-img-3.jpg', '2017-08-22 07:00:29', '2018-07-18 04:18:17', 1, 1, 'why-to-choose-this-app-for-your-project'),
	(8, 'resources/assets/images/news_images/1504092360.about.svg', '2017-08-22 07:03:16', '2018-07-10 01:51:31', 1, 0, 'about-us'),
	(9, 'resources/assets/images/news_images/1531932512.1504092640.laravel2.svg', '2017-08-22 07:57:33', '2018-07-18 04:48:32', 1, 0, 'laravel'),
	(11, 'resources/assets/images/news_images/1504091780.restaurant.svg', '2017-08-22 08:04:29', '2018-07-10 01:52:00', 1, 0, 'resturant'),
	(12, 'resources/assets/images/news_images/1504091963.fashion.svg', '2017-08-22 08:06:23', '2018-07-10 01:52:07', 1, 0, 'fashion'),
	(13, 'resources/assets/images/news_images/1504092054.electronics.svg', '2017-08-22 08:07:23', '2018-07-10 01:52:17', 1, 0, 'electronics'),
	(14, 'resources/assets/images/news_images/1504091642.language.svg', '2017-08-22 08:10:39', '2018-07-10 01:52:25', 1, 0, 'multi-language'),
	(15, 'resources/assets/images/news_images/1504091465.payment.svg', '2017-08-22 08:15:16', '2018-07-10 01:52:32', 1, 0, 'multiple-payment-gateways'),
	(16, 'resources/assets/images/news_images/1504091072.push_notifications.svg', '2017-08-22 08:17:28', '2018-07-10 01:53:47', 1, 0, 'push-notifications'),
	(17, 'resources/assets/images/news_images/1504091049.local_notifications.svg', '2017-08-22 08:18:08', '2018-07-10 01:54:16', 1, 0, 'local-notifications'),
	(18, 'resources/assets/images/news_images/1504091024.products.svg', '2017-08-22 08:18:51', '2018-07-10 01:54:23', 1, 0, 'all-types-of-products-friendly'),
	(19, 'resources/assets/images/news_images/1504091001.social.svg', '2017-08-22 08:19:35', '2018-07-10 01:54:31', 1, 0, 'log-in-via-social-accounts'),
	(20, 'resources/assets/images/news_images/1504090986.shipping_method.svg', '2017-08-22 08:22:33', '2018-07-10 01:54:38', 1, 0, 'multiple-shipping-methods'),
	(21, 'resources/assets/images/news_images/1504090941.theme.svg', '2017-08-22 08:23:22', '2018-07-10 01:54:54', 1, 0, 'interactive-theme-easy-customization-with-sass'),
	(22, 'resources/assets/images/news_images/1504090926.coupon_support.svg', '2017-08-22 10:52:53', '2018-07-10 01:55:02', 1, 0, 'coupon-support'),
	(23, 'resources/assets/images/news_images/1504090906.profile_pic.svg', '2017-08-22 10:53:45', '2018-07-10 01:55:07', 1, 0, 'profile-picture'),
	(25, 'resources/assets/images/news_images/1504090868.wishlist.svg', '2017-08-22 10:55:13', '2018-07-10 01:55:16', 1, 0, 'wish-list'),
	(28, 'resources/assets/images/news_images/1504088865.price_filter.svg', '2017-08-22 10:59:38', '2018-07-10 01:56:43', 1, 0, 'price-keyword-filters'),
	(29, 'resources/assets/images/news_images/1504088836.sorting.svg', '2017-08-22 11:03:06', '2018-07-10 01:56:47', 1, 0, 'product-sorting'),
	(30, 'resources/assets/images/news_images/1504088735.product_searching.svg', '2017-08-22 11:03:53', '2018-07-10 01:56:55', 1, 0, 'product-searching'),
	(31, 'resources/assets/images/news_images/1504088705.fully_customizable.svg', '2017-08-22 11:04:34', '2018-07-10 01:57:00', 1, 0, 'fully-customizable'),
	(32, 'resources/assets/images/news_images/1504087261.horizontal_Slider.svg', '2017-08-22 11:09:25', '2018-07-10 01:57:05', 1, 0, 'horizontal-product-slider'),
	(33, 'resources/assets/images/news_images/1504087219.customization.svg', '2017-08-22 11:13:38', '2018-07-10 01:57:09', 1, 0, 'customizable-features-functionalities'),
	(34, 'resources/assets/images/news_images/1504087179.grid_list.svg', '2017-08-22 11:14:16', '2018-07-10 01:57:15', 1, 0, 'product-grid-list-view'),
	(36, 'resources/assets/images/news_images/1504015398.shop_page.svg', '2017-08-22 11:16:24', '2018-07-10 01:57:24', 1, 0, 'beautiful-single-shop-page-for-complete-catalog'),
	(37, 'resources/assets/images/news_images/1504015381.my_orders.svg', '2017-08-22 11:17:04', '2018-07-10 01:57:28', 1, 0, 'my-orders'),
	(38, 'resources/assets/images/news_images/1504015363.about_contact_pages.svg', '2017-08-22 11:17:49', '2018-07-10 01:57:33', 1, 0, 'about-contact-pages'),
	(39, 'resources/assets/images/news_images/1504083589.Asset 2.svg', '2017-08-22 11:18:14', '2018-07-10 01:57:43', 1, 0, 'laravel-blog-pages'),
	(40, 'resources/assets/images/news_images/1504015347.info_pages.svg', '2017-08-22 11:18:53', '2018-07-10 01:57:48', 1, 0, 'info-pages'),
	(42, 'resources/assets/images/news_images/1504015307.recently_item.svg', '2017-08-22 11:24:05', '2018-07-10 01:57:57', 1, 0, 'recently-item-viewed-block-on-home-page'),
	(43, 'resources/assets/images/news_images/1504015288.move_to_top.svg', '2017-08-22 11:24:47', '2018-07-10 01:58:03', 1, 0, 'move-to-top-slider-button'),
	(44, 'resources/assets/images/news_images/1504015272.product_price_discount.svg', '2017-08-22 11:25:49', '2018-07-10 01:58:08', 1, 0, 'product-price-discount'),
	(45, 'resources/assets/images/news_images/1504015246.inventory_management.svg', '2017-08-22 11:26:24', '2018-07-10 01:58:13', 1, 0, 'inventory-management'),
	(46, 'resources/assets/images/news_images/1504013177.horizontal_Slider.svg', '2017-08-22 11:26:59', '2018-07-18 04:41:59', 1, 0, 'horizontal-slider-on-home-page'),
	(48, 'resources/assets/images/news_images/1504013140.product_additional_attributes.svg', '2017-08-22 11:36:02', '2018-07-18 04:42:17', 1, 0, 'product-additional-attributes-commerce-pricing-attributes'),
	(49, 'resources/assets/images/news_images/1504012761.multi_product_images.svg', '2017-08-22 11:36:36', '2018-07-18 04:42:35', 1, 0, 'multiple-product-images'),
	(50, 'resources/assets/images/news_images/1503928378.cart_page.svg', '2017-08-22 11:37:11', '2018-07-18 04:43:02', 1, 0, 'beautiful-cart-page'),
	(51, 'resources/assets/images/news_images/1503928065.shipping_managment.svg', '2017-08-22 11:37:48', '2018-07-18 04:43:17', 1, 0, 'shipping-address-management'),
	(53, 'resources/assets/images/news_images/1503927733.animtions.svg', '2017-08-22 11:38:58', '2018-07-18 04:37:38', 1, 0, 'animations'),
	(54, 'resources/assets/images/news_images/1531930664.blog-img-2.jpg', '2018-03-30 11:39:20', '2018-07-18 04:17:44', 1, 1, 'fashion-1');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table shop.newsletters
CREATE TABLE IF NOT EXISTS `newsletters` (
  `newsletters_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL,
  `date_sent` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `locked` int(1) DEFAULT '0',
  PRIMARY KEY (`newsletters_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.newsletters: ~0 rows (approximately)
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;

-- Dumping structure for table shop.news_categories
CREATE TABLE IF NOT EXISTS `news_categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` mediumtext COLLATE utf8_unicode_ci,
  `categories_icon` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `news_categories_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categories_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.news_categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `news_categories` DISABLE KEYS */;
INSERT INTO `news_categories` (`categories_id`, `categories_image`, `categories_icon`, `parent_id`, `sort_order`, `date_added`, `last_modified`, `news_categories_slug`) VALUES
	(6, 'resources/assets/images/news_categories_images/1504092793.app_features.svg', '', 0, NULL, '2017-08-22 06:20:50', '2018-07-10 01:14:35', 'app-features'),
	(7, 'resources/assets/images/news_categories_images/1504092906.introduction.svg', '', 0, NULL, '2017-08-22 06:22:50', '2018-07-10 01:14:48', 'introduction'),
	(8, 'resources/assets/images/news_categories_images/1504092995.platform.svg', '', 0, NULL, '2017-08-22 06:30:31', '2018-07-10 01:14:59', 'platforms'),
	(9, 'resources/assets/images/news_categories_images/1504093080.screenshots.svg', '', 0, NULL, '2017-08-22 06:31:50', '2018-07-10 01:15:03', 'screen-shots');
/*!40000 ALTER TABLE `news_categories` ENABLE KEYS */;

-- Dumping structure for table shop.news_categories_description
CREATE TABLE IF NOT EXISTS `news_categories_description` (
  `categories_description_id` int(100) NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categories_description_id`),
  KEY `idx_categories_name` (`categories_name`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.news_categories_description: ~12 rows (approximately)
/*!40000 ALTER TABLE `news_categories_description` DISABLE KEYS */;
INSERT INTO `news_categories_description` (`categories_description_id`, `categories_id`, `language_id`, `categories_name`) VALUES
	(16, 6, 1, 'App Features'),
	(17, 6, 2, 'App Functies'),
	(18, 6, 4, 'ميزات التطبيق'),
	(19, 7, 1, 'Introduction'),
	(20, 7, 2, 'Invoering'),
	(21, 7, 4, 'المقدمة'),
	(22, 8, 1, 'Platforms'),
	(23, 8, 2, 'Platforms'),
	(24, 8, 4, 'منصات'),
	(25, 9, 1, 'Screen Shots'),
	(26, 9, 2, 'Schermafbeeldingen'),
	(27, 9, 4, 'لقطات الشاشة');
/*!40000 ALTER TABLE `news_categories_description` ENABLE KEYS */;

-- Dumping structure for table shop.news_description
CREATE TABLE IF NOT EXISTS `news_description` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `news_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `news_description` text COLLATE utf8_unicode_ci,
  `news_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `news_viewed` int(5) DEFAULT '0',
  PRIMARY KEY (`news_id`,`language_id`),
  KEY `products_name` (`news_name`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.news_description: ~123 rows (approximately)
/*!40000 ALTER TABLE `news_description` DISABLE KEYS */;
INSERT INTO `news_description` (`news_id`, `language_id`, `news_name`, `news_description`, `news_url`, `news_viewed`) VALUES
	(6, 1, 'Tools and Technology', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(6, 2, 'Gereedschap en technologie', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(6, 4, 'الأدوات والتكنولوجيا', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(7, 1, 'Why To choose this App for your Project?', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(7, 2, 'Waarom deze applicatie?', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(7, 4, 'لماذا هذا التطبيق؟', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(8, 1, 'About Us', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(8, 2, 'Over ons', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(8, 4, 'معلومات عنا', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(9, 1, 'Laravel', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(9, 2, 'Laravel', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(9, 4, 'Laravel', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(11, 1, 'Resturant', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(11, 2, 'Resturant', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(11, 4, 'المطعم', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(12, 1, 'Fashion', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(12, 2, 'Mode', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(12, 4, 'موضه', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(13, 1, 'Electronics', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(13, 2, 'Elektronica', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(13, 4, 'إلكترونيات', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(14, 1, 'Multi Language', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(14, 2, 'Multi Language', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(14, 4, 'متعدد اللغات', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(15, 1, 'Multiple Payment Gateways', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(15, 2, 'Meerdere betalingsgateways', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(15, 4, 'بوابات الدفع المتعددة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(16, 1, 'Push Notifications', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(16, 2, 'Push Notifications', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(16, 4, 'دفع الإخطارات', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \\"ليتراسيت\\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \\"ألدوس بايج مايكر\\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \\"ليتراسيت\\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \\"ألدوس بايج مايكر\\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \\"ليتراسيت\\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \\"ألدوس بايج مايكر\\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(17, 1, 'Local Notifications', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(17, 2, 'Lokale meldingen', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(17, 4, 'الإشعارات المحلية', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(18, 1, 'All Types of Products Friendly', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(18, 2, 'Alle soorten producten vriendelijk', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(18, 4, 'جميع أنواع المنتجات ودية', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(19, 1, 'Log-in via Social Accounts', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(19, 2, 'Inloggen via sociale accounts', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(19, 4, 'تسجيل الدخول عبر الحسابات الاجتماعية', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(20, 1, 'Multiple Shipping Methods', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(20, 2, 'Meerdere verzendmethoden', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(20, 4, 'طرق الشحن متعددة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(21, 1, 'Interactive Theme & Easy Customization with SaSS', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(21, 2, 'Interactief thema en gemakkelijke aanpassing met SaSS', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(21, 4, 'موضوع التفاعلية وسهولة التخصيص مع ساس', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(22, 1, 'Coupon Support', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(22, 2, 'Coupon Ondersteuning', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(22, 4, 'دعم القسيمة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(23, 1, 'Profile Picture', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(23, 2, 'Profielfoto', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(23, 4, 'الصوره الشخصيه', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(25, 1, 'Wish List', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(25, 2, 'Wenslijst', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(25, 4, 'الأماني', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(28, 1, 'Price & Keyword Filters', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(28, 2, 'Prijs- en sleutelwoordfilters', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(28, 4, 'السعر والكلمات الرئيسية الفلاتر', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(29, 1, 'Product Sorting', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(29, 2, 'Product sorteren', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(29, 4, 'فرز المنتجات', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(30, 1, 'Product Searching', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(30, 2, 'Product zoeken', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(30, 4, 'البحث عن المنتج', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(31, 1, 'Fully Customizable', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(31, 2, 'Volledig klantgericht', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(31, 4, 'تماما للتخصيص', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(32, 1, 'Horizontal Product Slider', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(32, 2, 'Horizontale Product Slider', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(32, 4, 'أفقي المنتج المنزلق', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(33, 1, 'Customizable Features & Functionalities', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(33, 2, 'Aanpasbare eigenschappen en functionaliteit', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(33, 4, 'الميزات والتخصيص وظائف', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(34, 1, 'Product Grid & List View', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(34, 2, 'Product Grid & Lijstweergave', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(34, 4, 'شبكة المنتج وعرض القائمة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(36, 1, 'Beautiful Single Shop Page For Complete Catalog', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(36, 2, 'Mooie single shop pagina voor complete catalogus', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(36, 4, 'جميلة صفحة واحدة متجر للكتالوج الكامل', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(37, 1, 'My Orders', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(37, 2, 'mijn bestellingen', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(37, 4, 'طلباتي', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(38, 1, 'About & Contact Pages', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(38, 2, 'Over & Contactpagina\'s', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(38, 4, 'حول الصفحات والاتصال', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(39, 1, 'Laravel Blog Pages', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(39, 2, 'Laravel blog pagina\'s', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(39, 4, 'صفحات لارافيل المدونة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(40, 1, 'Info Pages', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(40, 2, 'Info pagina\'s', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(40, 4, 'صفحات المعلومات', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(42, 1, 'Recently Item Viewed Block on Home Page', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(42, 2, 'Onlangs Item bekeken Blok op Startpagina', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(42, 4, 'تم مؤخرا عرض العنصر بلوك أون هوم بادج', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(43, 1, 'Move to Top Slider Button', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(43, 2, 'Ga naar de bovenste schuifknop', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(43, 4, 'الانتقال إلى أعلى زر المنزلق', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(44, 1, 'Product Price Discount', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(44, 2, 'Productprijs korting', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(44, 4, 'خصم سعر المنتج', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(45, 1, 'Inventory Management', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(45, 2, 'ادارة المخزون', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(45, 4, 'Voorraadbeheer', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(46, 1, 'Horizontal Slider on Home Page', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(46, 2, 'Horizontale schuifregelaar op de startpagina', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(46, 4, 'أفقي المنزلق على الصفحة الرئيسية', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(48, 1, 'Product Additional Attributes / Commerce Pricing Attributes', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(48, 2, 'Product Aanvullende Attributen / Handelsprijzen Attributen', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(48, 4, 'سمات المنتج الإضافية / سمات التسعير التجاري', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(49, 1, 'Multiple Product Images', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(49, 2, 'Meerdere productafbeeldingen', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(49, 4, 'صور المنتج متعددة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(50, 1, 'Beautiful Cart Page', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(50, 2, 'Mooie winkelwagen pagina', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(50, 4, 'صفحة العربة الجميلة', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(51, 1, 'Shipping Address Management', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(51, 2, 'Verzendadresbeheer', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(51, 4, 'إدارة عنوان الشحن', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(53, 1, 'Animations', '<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(53, 2, 'animaties', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(53, 4, 'الرسوم المتحركة', '<p>لها أن جنوب أجزاء. ثم وفي اعتداء الجنوبي. تم الشرقي بمعارضة عدم, فقد أن حاول الآلاف إستعمل. مكن لعدم فهرست الخاطفة و, من تعديل لفرنسا الشهير عرض, أم مدينة السيطرة وصل. كان وقبل لعدم لم, جعل معزّزة والمانيا استطاعوا هو, مع بعض ألمانيا الأراضي. تحت وأزيز وفنلندا بـ, دأبوا أعلنت الإنذار، نفس تم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لها أن جنوب أجزاء. ثم وفي اعتداء الجنوبي. تم الشرقي بمعارضة عدم, فقد أن حاول الآلاف إستعمل. مكن لعدم فهرست الخاطفة و, من تعديل لفرنسا الشهير عرض, أم مدينة السيطرة وصل. كان وقبل لعدم لم, جعل معزّزة والمانيا استطاعوا هو, مع بعض ألمانيا الأراضي. تحت وأزيز وفنلندا بـ, دأبوا أعلنت الإنذار، نفس تم.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لها أن جنوب أجزاء. ثم وفي اعتداء الجنوبي. تم الشرقي بمعارضة عدم, فقد أن حاول الآلاف إستعمل. مكن لعدم فهرست الخاطفة و, من تعديل لفرنسا الشهير عرض, أم مدينة السيطرة وصل. كان وقبل لعدم لم, جعل معزّزة والمانيا استطاعوا هو, مع بعض ألمانيا الأراضي. تحت وأزيز وفنلندا بـ, دأبوا أعلنت الإنذار، نفس تم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(54, 1, 'Fashion', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>\r\n\r\n<p>&nbsp;</p>', NULL, 0),
	(54, 2, 'Mode', '<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.<br />\r\n<img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, persecuti neglegentur ei sit, assum accusata atomorum duo ne, timeam philosophia ex sea. Pri malorum blandit splendide id, est ea autem docendi interesset. Et vivendo lobortis has, te ius summo epicurei atomorum, an usu novum officiis intellegebat. Ne ridens dicunt eos, vel ad atqui mazim oratio. At vix nisl dolore similique, vidit dicat elitr eum te. Id eum mentitum nominavi, velit oporteat referrentur mei ei, et sea legimus suscipit. Quis augue altera mei et.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0),
	(54, 4, 'موضه', '<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ionic\\" src=\\"https://ionicframework.com/img/ionic-meta.jpg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for angularjs\\" src=\\"http://paislee.io/content/images/2014/Aug/angular_js.svg\\" /></p>\r\n\r\n<p>لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق &quot;ليتراسيت&quot; (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل &quot;ألدوس بايج مايكر&quot; (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.</p>\r\n\r\n<p><img alt=\\"Image result for ngcordova\\" src=\\"http://ngcordova.com/img/cta-image.png\\" /></p>', NULL, 0);
/*!40000 ALTER TABLE `news_description` ENABLE KEYS */;

-- Dumping structure for table shop.news_to_news_categories
CREATE TABLE IF NOT EXISTS `news_to_news_categories` (
  `news_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`,`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.news_to_news_categories: ~41 rows (approximately)
/*!40000 ALTER TABLE `news_to_news_categories` DISABLE KEYS */;
INSERT INTO `news_to_news_categories` (`news_id`, `categories_id`) VALUES
	(6, 7),
	(7, 7),
	(8, 7),
	(9, 8),
	(11, 9),
	(12, 9),
	(13, 9),
	(14, 6),
	(15, 6),
	(16, 6),
	(17, 6),
	(18, 6),
	(19, 6),
	(20, 6),
	(21, 6),
	(22, 6),
	(23, 6),
	(25, 6),
	(28, 6),
	(29, 6),
	(30, 6),
	(31, 6),
	(32, 6),
	(33, 6),
	(34, 6),
	(36, 6),
	(37, 6),
	(38, 6),
	(39, 6),
	(40, 6),
	(42, 6),
	(43, 6),
	(44, 6),
	(45, 6),
	(46, 6),
	(48, 6),
	(49, 6),
	(50, 6),
	(51, 6),
	(53, 6),
	(54, 6);
/*!40000 ALTER TABLE `news_to_news_categories` ENABLE KEYS */;

-- Dumping structure for table shop.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `total_tax` decimal(7,2) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_address_format_id` int(5) DEFAULT NULL,
  `delivery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address_format_id` int(5) DEFAULT NULL,
  `billing_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_format_id` int(5) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_expires` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_price` decimal(10,2) NOT NULL,
  `shipping_cost` decimal(10,2) NOT NULL,
  `shipping_method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_duration` int(100) DEFAULT NULL,
  `order_information` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `coupon_code` text COLLATE utf8_unicode_ci NOT NULL,
  `coupon_amount` int(100) NOT NULL,
  `exclude_product_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excluded_product_categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `product_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordered_source` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Website, 2: App',
  PRIMARY KEY (`orders_id`),
  KEY `idx_orders_customers_id` (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders: ~2 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`orders_id`, `total_tax`, `customers_id`, `customers_name`, `customers_company`, `customers_street_address`, `customers_suburb`, `customers_city`, `customers_postcode`, `customers_state`, `customers_country`, `customers_telephone`, `email`, `customers_address_format_id`, `delivery_name`, `delivery_company`, `delivery_street_address`, `delivery_suburb`, `delivery_city`, `delivery_postcode`, `delivery_state`, `delivery_country`, `delivery_address_format_id`, `billing_name`, `billing_company`, `billing_street_address`, `billing_suburb`, `billing_city`, `billing_postcode`, `billing_state`, `billing_country`, `billing_address_format_id`, `payment_method`, `cc_type`, `cc_owner`, `cc_number`, `cc_expires`, `last_modified`, `date_purchased`, `orders_date_finished`, `currency`, `currency_value`, `order_price`, `shipping_cost`, `shipping_method`, `shipping_duration`, `order_information`, `is_seen`, `coupon_code`, `coupon_amount`, `exclude_product_ids`, `product_categories`, `excluded_product_categories`, `free_shipping`, `product_ids`, `ordered_source`) VALUES
	(1, 0.00, 1, 'Ko Ko Lah', NULL, 'Street', '', 'Yangon', '11022', 'other', 'Myanmar', '', 'mr.yawya@gmail.com', NULL, 'Ko Ko Lah', NULL, 'Street', '', 'Yangon', '11022', 'other', 'Myanmar', NULL, 'Ko Ko Lah', NULL, 'Street', '', 'Yangon', '11022', 'other', 'Myanmar', 0, 'Cash on Delivery', '', '', '', '', '2018-09-07 08:03:09', '2018-09-07 08:03:09', NULL, '$', NULL, 125.50, 0.00, 'Free Shipping', NULL, '[]', 1, '', 0, '', '', '', 0, '', 1),
	(2, 0.00, 1, 'U Yaw Ya', NULL, '123 Street', '', 'Yangon', '11025', 'other', 'Myanmar', '', 'mr.yawya@gmail.com', NULL, 'U Yaw Ya', NULL, '123 Street', '', 'Yangon', '11025', 'other', 'Myanmar', NULL, 'U Yaw Ya', NULL, '123 Street', '', 'Yangon', '11025', 'other', 'Myanmar', 0, 'Cash on Delivery', '', '', '', '', '2018-09-07 08:39:45', '2018-09-07 08:39:45', NULL, '$', NULL, 96.00, 11.00, 'Flat Rate', NULL, '[]', 1, '', 0, '', '', '', 0, '', 1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table shop.orders_products
CREATE TABLE IF NOT EXISTS `orders_products` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `products_model` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_price` decimal(15,2) NOT NULL,
  `final_price` decimal(15,2) NOT NULL,
  `products_tax` decimal(7,0) NOT NULL,
  `products_quantity` int(2) NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_products_orders_id` (`orders_id`),
  KEY `idx_orders_products_products_id` (`products_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_products: ~2 rows (approximately)
/*!40000 ALTER TABLE `orders_products` DISABLE KEYS */;
INSERT INTO `orders_products` (`orders_products_id`, `orders_id`, `products_id`, `products_model`, `products_name`, `products_price`, `final_price`, `products_tax`, `products_quantity`) VALUES
	(1, 1, 8, NULL, 'STANDARD FIT COTTON POPOVER', 125.50, 125.50, 1, 1),
	(2, 2, 1, NULL, 'CLASSIC FIT SOFT-TOUCH POLO', 85.00, 85.00, 1, 1);
/*!40000 ALTER TABLE `orders_products` ENABLE KEYS */;

-- Dumping structure for table shop.orders_products_attributes
CREATE TABLE IF NOT EXISTS `orders_products_attributes` (
  `orders_products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `orders_products_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `products_options` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_values` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `options_values_price` decimal(15,2) NOT NULL,
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_products_attributes_id`),
  KEY `idx_orders_products_att_orders_id` (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_products_attributes: ~3 rows (approximately)
/*!40000 ALTER TABLE `orders_products_attributes` DISABLE KEYS */;
INSERT INTO `orders_products_attributes` (`orders_products_attributes_id`, `orders_id`, `orders_products_id`, `products_id`, `products_options`, `products_options_values`, `options_values_price`, `price_prefix`) VALUES
	(1, 1, 1, 8, 'Size', 'Small', 0.00, '+'),
	(2, 2, 2, 1, 'Colors', 'Brown', 0.00, '+'),
	(3, 2, 2, 1, 'Size', 'Small', 0.00, '+');
/*!40000 ALTER TABLE `orders_products_attributes` ENABLE KEYS */;

-- Dumping structure for table shop.orders_products_download
CREATE TABLE IF NOT EXISTS `orders_products_download` (
  `orders_products_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `download_maxdays` int(2) NOT NULL DEFAULT '0',
  `download_count` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_products_download_id`),
  KEY `idx_orders_products_download_orders_id` (`orders_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_products_download: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders_products_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_products_download` ENABLE KEYS */;

-- Dumping structure for table shop.orders_status
CREATE TABLE IF NOT EXISTS `orders_status` (
  `orders_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `public_flag` int(11) DEFAULT '0',
  `downloads_flag` int(11) DEFAULT '0',
  PRIMARY KEY (`orders_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`orders_status_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_status: ~7 rows (approximately)
/*!40000 ALTER TABLE `orders_status` DISABLE KEYS */;
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`) VALUES
	(1, 1, 'Pending', 1, 0),
	(2, 1, 'Completed', 0, 0),
	(3, 1, 'Cancel', 0, 0),
	(4, 4, 'قيد الانتظار', 1, 0),
	(5, 4, 'منجز', 0, 0),
	(6, 4, 'إلغاء', 0, 0),
	(7, 1, 'Good', 0, 0);
/*!40000 ALTER TABLE `orders_status` ENABLE KEYS */;

-- Dumping structure for table shop.orders_status_description
CREATE TABLE IF NOT EXISTS `orders_status_description` (
  `orders_status_description_id` int(100) NOT NULL AUTO_INCREMENT,
  `orders_status_id` int(100) NOT NULL,
  `orders_status_name` varchar(255) NOT NULL,
  `language_id` int(100) NOT NULL,
  PRIMARY KEY (`orders_status_description_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table shop.orders_status_description: 0 rows
/*!40000 ALTER TABLE `orders_status_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_status_description` ENABLE KEYS */;

-- Dumping structure for table shop.orders_status_history
CREATE TABLE IF NOT EXISTS `orders_status_history` (
  `orders_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `orders_status_id` int(5) NOT NULL,
  `date_added` datetime NOT NULL,
  `customer_notified` int(1) DEFAULT '0',
  `comments` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`orders_status_history_id`),
  KEY `idx_orders_status_history_orders_id` (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_status_history: ~4 rows (approximately)
/*!40000 ALTER TABLE `orders_status_history` DISABLE KEYS */;
INSERT INTO `orders_status_history` (`orders_status_history_id`, `orders_id`, `orders_status_id`, `date_added`, `customer_notified`, `comments`) VALUES
	(1, 1, 1, '2018-09-07 08:03:09', 1, ''),
	(2, 1, 2, '2018-09-07 08:04:40', 1, ''),
	(3, 2, 1, '2018-09-07 08:39:45', 1, ''),
	(4, 2, 3, '2018-09-07 08:41:07', 1, '');
/*!40000 ALTER TABLE `orders_status_history` ENABLE KEYS */;

-- Dumping structure for table shop.orders_total
CREATE TABLE IF NOT EXISTS `orders_total` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(15,4) NOT NULL,
  `class` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_orders_total_orders_id` (`orders_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.orders_total: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders_total` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_total` ENABLE KEYS */;

-- Dumping structure for table shop.packages
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('on','off') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.packages: ~0 rows (approximately)
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;

-- Dumping structure for table shop.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(100) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.pages: 8 rows
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`page_id`, `slug`, `status`, `type`) VALUES
	(1, 'privacy-policy', 1, 1),
	(2, 'term-services', 1, 1),
	(3, 'refund-policy', 1, 1),
	(4, 'about-us', 1, 1),
	(5, 'privacy-policy', 1, 2),
	(6, 'term-services', 1, 2),
	(7, 'refund-policy', 1, 2),
	(8, 'about-us', 1, 2);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table shop.pages_description
CREATE TABLE IF NOT EXISTS `pages_description` (
  `page_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(100) NOT NULL,
  `page_id` int(100) NOT NULL,
  PRIMARY KEY (`page_description_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.pages_description: 8 rows
/*!40000 ALTER TABLE `pages_description` DISABLE KEYS */;
INSERT INTO `pages_description` (`page_description_id`, `name`, `description`, `language_id`, `page_id`) VALUES
	(1, 'Privacy Policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>\r\n', 1, 1),
	(4, 'Term & Services', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>\r\n', 1, 2),
	(7, 'Refund Policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>\r\n', 1, 3),
	(10, 'About Us', '<h2><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></h2>\r\n\r\n<p>Cras non justo sed nulla finibus pulvinar sit amet et neque. Duis et odio vitae orci mattis gravida. Nullam vel tincidunt ex. Praesent vel neque egestas arcu feugiat venenatis. Donec eget dolor quis justo tempus mattis. Phasellus dictum nunc ut dapibus dictum. Etiam vel leo nulla. Ut eu mi hendrerit, eleifend lacus sed, dictum neque.</p>\r\n\r\n<p>Aliquam non convallis nibh. Donec luctus purus magna, et commodo urna fermentum sed. Cras vel ex blandit, pretium nulla id, efficitur massa. Suspendisse potenti. Maecenas vel vehicula velit. Etiam quis orci molestie, elementum nisl eget, ultricies felis. Ut condimentum quam ut mi scelerisque accumsan. Suspendisse potenti. Etiam orci purus, iaculis sit amet ornare sit amet, finibus sed ligula. Quisque et mollis libero, sit amet consectetur augue. Vestibulum posuere pellentesque enim, in facilisis diam dictum eget. Phasellus sed vestibulum urna, in aliquet felis. Vivamus quis lacus id est ornare faucibus at id nulla.</p>\r\n\r\n<h2>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</h2>\r\n\r\n<p>Nulla justo lectus, sollicitudin at lorem eu, sollicitudin molestie augue. Maecenas egestas facilisis dolor mattis feugiat. Donec sed orci tellus. Maecenas tortor ipsum, varius vel dolor nec, bibendum porttitor felis. Mauris rutrum tristique vehicula. Sed ullamcorper nisl non erat pharetra, sit amet mattis enim posuere. Nulla convallis fringilla tortor, at vestibulum mauris cursus eget. Ut semper sollicitudin odio, sed molestie libero luctus quis. Integer viverra rutrum diam non maximus. Maecenas pellentesque tortor et sapien fermentum laoreet non et est. Phasellus felis quam, laoreet rhoncus erat eget, tempor condimentum massa. Integer gravida turpis id suscipit bibendum. Phasellus pellentesque venenatis erat, ut maximus justo vulputate sed. Vestibulum maximus enim ligula, non suscipit lectus dignissim vel. Suspendisse eleifend lorem egestas, tristique ligula id, condimentum est.</p>\r\n\r\n<p>Mauris nulla nulla, laoreet at auctor quis, bibendum rutrum neque. Donec eu ligula mi. Nam cursus vulputate semper. Phasellus facilisis mollis tellus, interdum laoreet justo rutrum pulvinar. Praesent molestie, nibh sed ultrices porttitor, nulla tortor volutpat enim, quis auctor est sem et orci. Proin lacinia vestibulum ex ut convallis. Phasellus tempus odio purus.</p>\r\n\r\n<ul>\r\n	<li>Nam lacinia urna eu arcu auctor, eget euismod metus sagittis.</li>\r\n	<li>Etiam eleifend ex eu interdum varius.</li>\r\n	<li>Nunc dapibus purus eu felis tincidunt, vel rhoncus erat tristique.</li>\r\n	<li>Aenean nec augue sit amet lorem blandit ultrices.</li>\r\n	<li>Nullam at lacus eleifend, pulvinar velit tempor, auctor nisi.</li>\r\n</ul>\r\n\r\n<p>Nunc accumsan tincidunt augue sed blandit. Duis et dignissim nisi. Phasellus sed ligula velit. Etiam rhoncus aliquet magna, nec volutpat nulla imperdiet et. Nunc vel tincidunt magna. Vestibulum lacinia odio a metus placerat maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et faucibus neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean et metus malesuada, ullamcorper dui vel, convallis est. Donec congue libero sed turpis porta consequat. Suspendisse potenti. Aliquam pharetra nibh in magna iaculis, non elementum ipsum luctus.</p>\r\n\r\n<p>&nbsp;</p>', 1, 4),
	(13, 'Privacy Policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>', 1, 5),
	(16, 'Term & Services', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>', 1, 6),
	(19, 'Refund Policy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>', 1, 7),
	(22, 'About Us', '<h2><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></h2>\r\n\r\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></p>\r\n\r\n<p>Cras non justo sed nulla finibus pulvinar sit amet et neque. Duis et odio vitae orci mattis gravida. Nullam vel tincidunt ex. Praesent vel neque egestas arcu feugiat venenatis. Donec eget dolor quis justo tempus mattis. Phasellus dictum nunc ut dapibus dictum. Etiam vel leo nulla. Ut eu mi hendrerit, eleifend lacus sed, dictum neque.</p>\r\n\r\n<p>Aliquam non convallis nibh. Donec luctus purus magna, et commodo urna fermentum sed. Cras vel ex blandit, pretium nulla id, efficitur massa. Suspendisse potenti. Maecenas vel vehicula velit. Etiam quis orci molestie, elementum nisl eget, ultricies felis. Ut condimentum quam ut mi scelerisque accumsan. Suspendisse potenti. Etiam orci purus, iaculis sit amet ornare sit amet, finibus sed ligula. Quisque et mollis libero, sit amet consectetur augue. Vestibulum posuere pellentesque enim, in facilisis diam dictum eget. Phasellus sed vestibulum urna, in aliquet felis. Vivamus quis lacus id est ornare faucibus at id nulla.</p>\r\n\r\n<h2>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</h2>\r\n\r\n<p>Nulla justo lectus, sollicitudin at lorem eu, sollicitudin molestie augue. Maecenas egestas facilisis dolor mattis feugiat. Donec sed orci tellus. Maecenas tortor ipsum, varius vel dolor nec, bibendum porttitor felis. Mauris rutrum tristique vehicula. Sed ullamcorper nisl non erat pharetra, sit amet mattis enim posuere. Nulla convallis fringilla tortor, at vestibulum mauris cursus eget. Ut semper sollicitudin odio, sed molestie libero luctus quis. Integer viverra rutrum diam non maximus. Maecenas pellentesque tortor et sapien fermentum laoreet non et est. Phasellus felis quam, laoreet rhoncus erat eget, tempor condimentum massa. Integer gravida turpis id suscipit bibendum. Phasellus pellentesque venenatis erat, ut maximus justo vulputate sed. Vestibulum maximus enim ligula, non suscipit lectus dignissim vel. Suspendisse eleifend lorem egestas, tristique ligula id, condimentum est.</p>\r\n\r\n<p>Mauris nulla nulla, laoreet at auctor quis, bibendum rutrum neque. Donec eu ligula mi. Nam cursus vulputate semper. Phasellus facilisis mollis tellus, interdum laoreet justo rutrum pulvinar. Praesent molestie, nibh sed ultrices porttitor, nulla tortor volutpat enim, quis auctor est sem et orci. Proin lacinia vestibulum ex ut convallis. Phasellus tempus odio purus.</p>\r\n\r\n<ul>\r\n	<li>Nam lacinia urna eu arcu auctor, eget euismod metus sagittis.</li>\r\n	<li>Etiam eleifend ex eu interdum varius.</li>\r\n	<li>Nunc dapibus purus eu felis tincidunt, vel rhoncus erat tristique.</li>\r\n	<li>Aenean nec augue sit amet lorem blandit ultrices.</li>\r\n	<li>Nullam at lacus eleifend, pulvinar velit tempor, auctor nisi.</li>\r\n</ul>\r\n\r\n<p>Nunc accumsan tincidunt augue sed blandit. Duis et dignissim nisi. Phasellus sed ligula velit. Etiam rhoncus aliquet magna, nec volutpat nulla imperdiet et. Nunc vel tincidunt magna. Vestibulum lacinia odio a metus placerat maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et faucibus neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean et metus malesuada, ullamcorper dui vel, convallis est. Donec congue libero sed turpis porta consequat. Suspendisse potenti. Aliquam pharetra nibh in magna iaculis, non elementum ipsum luctus.</p>', 1, 8);
/*!40000 ALTER TABLE `pages_description` ENABLE KEYS */;

-- Dumping structure for table shop.payments_setting
CREATE TABLE IF NOT EXISTS `payments_setting` (
  `payments_id` int(100) NOT NULL AUTO_INCREMENT,
  `braintree_enviroment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `braintree_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `braintree_merchant_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `braintree_public_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `braintree_private_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brantree_active` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_enviroment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secret_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publishable_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_active` tinyint(1) NOT NULL DEFAULT '0',
  `cash_on_delivery` tinyint(1) NOT NULL DEFAULT '0',
  `cod_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_status` tinyint(1) NOT NULL DEFAULT '0',
  `paypal_enviroment` tinyint(1) DEFAULT '0',
  `payment_currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_enviroment` tinyint(1) NOT NULL,
  `instamojo_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_api_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_auth_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_client_id` text COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_client_secret` text COLLATE utf8_unicode_ci NOT NULL,
  `instamojo_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payments_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.payments_setting: 1 rows
/*!40000 ALTER TABLE `payments_setting` DISABLE KEYS */;
INSERT INTO `payments_setting` (`payments_id`, `braintree_enviroment`, `braintree_name`, `braintree_merchant_id`, `braintree_public_key`, `braintree_private_key`, `brantree_active`, `stripe_enviroment`, `stripe_name`, `secret_key`, `publishable_key`, `stripe_active`, `cash_on_delivery`, `cod_name`, `paypal_name`, `paypal_id`, `paypal_status`, `paypal_enviroment`, `payment_currency`, `instamojo_enviroment`, `instamojo_name`, `instamojo_api_key`, `instamojo_auth_token`, `instamojo_client_id`, `instamojo_client_secret`, `instamojo_active`) VALUES
	(1, '0', 'Braintree', 'wrv3cwbft6n3bg5g', '2bz5kxcj2gs3hdbx', '55ae08cb061e36dca59aaf2a883190bf', 1, '0', 'Stripe', 'sk_test_Gsz7jL4wRikI8YFaNzbwxKOF', 'pk_test_cCAEC6EejawuAvsvR9bhKrGR', 1, 1, 'Cash On Delivery', 'Paypal', 'AULJ0Q_kdXwEbi7PCBunUBJc4Kkg2vvdazF8kJoywAV9_i7iJMQphB9NLwdR0v2BAUlLF974iTrynbys', 1, 0, 'USD', 0, 'Instamojo', 'c5a348bd5fcb4c866074c48e9c77c6c4', '99448897defb4423b921fe47e0851b86', 'test_9l7MW8I7c2bwIw7q0koc6B1j5NrvzyhasQh', 'test_m9Ey3Jqp9AfmyWKmUMktt4K3g1uMIdapledVRRYJha7WinxOsBVV5900QMlwvv3l2zRmzcYDEOKPh1cvnVedkAKtHOFFpJbqcoNCNrjx1FtZhtDMkEJFv9MJuXD', 1);
/*!40000 ALTER TABLE `payments_setting` ENABLE KEYS */;

-- Dumping structure for table shop.payment_description
CREATE TABLE IF NOT EXISTS `payment_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `payment_name` varchar(32) NOT NULL,
  `sub_name_1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sub_name_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table shop.payment_description: 10 rows
/*!40000 ALTER TABLE `payment_description` DISABLE KEYS */;
INSERT INTO `payment_description` (`id`, `name`, `language_id`, `payment_name`, `sub_name_1`, `sub_name_2`) VALUES
	(1, 'Braintree', 1, 'Braintree', 'Credit Card', 'Paypal'),
	(2, 'Braintree', 2, 'Braintree', 'Kreditkarte', 'Paypal'),
	(4, 'Stripe', 1, 'Stripe', '', ''),
	(5, 'Paypal', 1, 'Paypal', '', ''),
	(6, 'Cash on Delivery', 1, 'Cash On Delivery', '', ''),
	(7, 'Stripe', 2, 'Stripe', '', ''),
	(8, 'Paypal', 2, 'Paypal', '', ''),
	(9, 'Nachnahme', 2, 'Cash On Delivery', '', ''),
	(20, 'Instamojo', 2, 'Instamojo', '', ''),
	(19, 'Instamojo', 1, 'Instamojo', '', '');
/*!40000 ALTER TABLE `payment_description` ENABLE KEYS */;

-- Dumping structure for table shop.products
CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_quantity` int(4) NOT NULL,
  `products_model` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image` mediumtext COLLATE utf8_unicode_ci,
  `products_price` decimal(15,2) NOT NULL,
  `products_date_added` datetime NOT NULL,
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_weight_unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_status` tinyint(1) NOT NULL,
  `products_tax_class_id` int(11) NOT NULL,
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_liked` int(100) NOT NULL,
  `low_limit` int(4) NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT '0',
  `products_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` int(11) NOT NULL,
  PRIMARY KEY (`products_id`),
  KEY `idx_products_model` (`products_model`),
  KEY `idx_products_date_added` (`products_date_added`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products: ~81 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`products_id`, `products_quantity`, `products_model`, `products_image`, `products_price`, `products_date_added`, `products_last_modified`, `products_date_available`, `products_weight`, `products_weight_unit`, `products_status`, `products_tax_class_id`, `manufacturers_id`, `products_ordered`, `products_liked`, `low_limit`, `is_feature`, `products_slug`, `vendor_id`) VALUES
	(1, 9997, '', 'resources/assets/images/product_images/1502174889.pPOLO2-26314766_standard_v400.jpg', 85.00, '2017-08-07 11:44:10', '2018-07-17 02:42:41', NULL, '0.500', 'Gram', 1, 1, 0, 12, 5, 0, 0, 'classic-fit-soft-touch-polo', 4),
	(2, 9999, '', 'resources/assets/images/product_images/1502114036.pPOLO2-26316336_standard_v400.jpg', 98.50, '2017-08-07 01:53:56', '2018-07-06 03:42:43', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 5, 0, 0, 'hampton-classic-fit-shirt', 5),
	(3, 9999, '', 'resources/assets/images/product_images/1502174346.pPOLO2-26314826_standard_v400.jpg', 85.00, '2017-08-08 06:39:06', NULL, NULL, '0.500', 'Kilogram', 1, 1, NULL, 0, 0, 0, 0, '', 6),
	(4, 9998, '', 'resources/assets/images/product_images/1502174364.pPOLO2-26314826_standard_v400.jpg', 85.00, '2017-08-08 06:39:24', '2018-07-06 03:42:59', NULL, '0.500', 'Kilogram', 1, 1, 0, 4, 4, 0, 0, 'classic-fit-cotton-polo-shirt', 4),
	(5, 9999, '', 'resources/assets/images/product_images/1502176579.pPOLO2-26316348_standard_v400.jpg', 98.50, '2017-08-08 07:16:19', '2018-07-06 03:43:32', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'custom-slim-fit-cotton-shirt', 5),
	(6, 9999, '', 'resources/assets/images/product_images/1502177321.pPOLO2-26314634_standard_v400.jpg', 89.50, '2017-08-08 07:28:41', '2018-07-06 03:43:57', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 2, 0, 0, 'custom-slim-fit-weathered-polo', 6),
	(7, 9999, '', 'resources/assets/images/product_images/1502180946.pPOLO2-26008917_standard_v400.jpg', 98.50, '2017-08-08 08:29:06', '2018-07-06 03:44:27', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'standard-fit-cotton-shirt', 4),
	(8, 9994, '', 'resources/assets/images/product_images/1502181584.pPOLO2-26008953_standard_v400.jpg', 125.50, '2017-08-08 08:39:44', '2018-07-06 03:44:50', NULL, '0.500', 'Kilogram', 1, 1, 0, 9, 3, 0, 0, 'standard-fit-cotton-popover', 5),
	(9, 9999, '', 'resources/assets/images/product_images/1502182426.pPOLO2-26008935_standard_v400.jpg', 89.50, '2017-08-08 08:53:46', '2018-07-06 03:55:34', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 4, 0, 0, 'standard-fit-madras-popover', 6),
	(10, 9999, '', 'resources/assets/images/product_images/1502186978.pPOLO2-26315018_standard_v400.jpg', 165.00, '2017-08-08 10:09:38', '2018-07-06 03:55:29', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'standard-fit-cotton-workshirt', 4),
	(11, 9999, '', 'resources/assets/images/product_images/1502187824.pPOLO2-26317497_standard_v400.jpg', 145.00, '2017-08-08 10:23:44', '2018-07-06 03:55:23', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'eldridge-super-slim-jean', 5),
	(12, 9999, '', 'resources/assets/images/product_images/1502189779.pPOLO2-26316198_standard_v360x480.jpg', 165.00, '2017-08-08 10:56:19', '2018-07-06 03:55:18', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'sullivan-slim-fit-jean', 6),
	(13, 9999, '', 'resources/assets/images/product_images/1502190187.pPOLO2-26315541_standard_v400.jpg', 125.00, '2017-08-08 11:03:07', '2018-07-06 03:55:15', NULL, '0.500', 'Kilogram', 1, 1, 0, 6, 3, 0, 0, 'hampton-straight-fit-jean', 4),
	(14, 9999, '', 'resources/assets/images/product_images/1502190590.pPOLO2-26404754_standard_v400.jpg', 90.00, '2017-08-08 11:09:50', '2018-07-06 03:54:46', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'laxman-tech-suede-sneaker-1', 5),
	(15, 9999, '', 'resources/assets/images/product_images/1502191191.pPOLO2-26256326_standard_v400.jpg', 69.50, '2017-08-08 11:19:51', '2018-07-06 03:54:41', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 2, 0, 0, 'vaughn-suede-slip-on-sneaker', 6),
	(16, 9999, '', 'resources/assets/images/product_images/1502191373.pPOLO2-21857429_standard_v400.jpg', 175.00, '2017-08-08 11:22:53', '2018-07-06 03:54:37', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'workington-leather-driver', 4),
	(17, 9999, '', 'resources/assets/images/product_images/1502191568.pPOLO2-26256404_standard_v400.jpg', 275.00, '2017-08-08 11:26:08', '2018-07-17 02:46:39', NULL, '0.500', 'Gram', 1, 1, 0, 0, 6, 0, 1, 'dillian-ii-suede-chelsea-boot', 5),
	(18, 9999, '', 'resources/assets/images/product_images/1502191856.pPOLO2-24354359_standard_v400.jpg', 559.00, '2017-08-08 11:30:56', '2018-07-06 03:54:27', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'brunel-leather-work-boot', 6),
	(19, 9999, '', 'resources/assets/images/product_images/1502192066.pPOLO2-25784541_standard_v400.jpg', 89.00, '2017-08-08 11:34:26', '2018-07-06 03:54:23', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 1, 0, 0, 'laxman-tech-suede-sneaker', 4),
	(20, 9999, 'Polo', 'resources/assets/images/product_images/1502192365.pPOLO2-12181663_standard_v400.jpg', 250.00, '2017-08-08 11:39:25', '2018-07-06 03:54:18', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'round-sunglasses', 5),
	(21, 9999, 'Polo', 'resources/assets/images/product_images/1502193410.pPOLO2-19116009_standard_v400.jpg', 150.00, '2017-08-08 11:56:50', '2018-07-06 03:54:14', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'nautical-striped-sunglasses', 6),
	(22, 9999, '', 'resources/assets/images/product_images/1502193577.pPOLO2-24933842_standard_v400.jpg', 129.00, '2017-08-08 11:59:37', '2018-07-06 03:54:08', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'polo-square-sunglasses', 4),
	(23, 9999, '', 'resources/assets/images/product_images/1502193710.pPOLO2-24128696_standard_v400.jpg', 229.00, '2017-08-08 12:01:50', '2018-07-06 03:54:04', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'polo-aviator-sunglasses', 5),
	(24, 9999, '', 'resources/assets/images/product_images/1502194893.pPOLO2-25759503_alternate1_v400.jpg', 198.00, '2017-08-08 12:21:33', '2018-07-06 03:53:24', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'strapless-jersey-maxidress', 6),
	(25, 9999, '', 'resources/assets/images/product_images/1502195102.pPOLO2-25759495_alternate1_v400.jpg', 258.00, '2017-08-08 12:25:02', '2018-07-06 03:53:20', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'madras-fit-and-flare-dress', 4),
	(26, 9999, '', 'resources/assets/images/product_images/1502195452.pPOLO2-26059809_alternate1_v400.jpg', 298.00, '2017-08-08 12:30:52', '2018-07-06 03:53:15', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'silk-crepe-shirtdress', 5),
	(27, 9999, '', 'resources/assets/images/product_images/1502195642.pPOLO2-25854363_alternate1_v400.jpg', 198.00, '2017-08-08 12:34:02', '2018-07-06 03:53:11', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'beaded-jersey-gown', 6),
	(28, 9999, '', 'resources/assets/images/product_images/1502196660.pPOLO2-25759710_standard_v400.jpg', 98.00, '2017-08-08 12:51:00', '2018-07-06 03:53:06', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'striped-cotton-crewneck-tee', 4),
	(29, 9999, '', 'resources/assets/images/product_images/1502197951.pPOLO2-25759868_standard_v400.jpg', 145.00, '2017-08-08 01:12:31', '2018-07-06 03:53:01', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'cotton-blend-crewneck-pullover', 5),
	(30, 9999, '', 'resources/assets/images/product_images/1502198422.pPOLO2-26060127_standard_v400.jpg', 85.00, '2017-08-08 01:20:22', '2018-07-06 03:52:57', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'crewneck-long-sleeve-top', 6),
	(31, 9999, '', 'resources/assets/images/product_images/1502199326.pPOLO2-26451235_standard_v400.jpg', 205.00, '2017-08-08 01:35:26', '2018-07-06 03:52:51', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'tompkins-skinny-crop-jean', 4),
	(32, 9999, '', 'resources/assets/images/product_images/1502200730.pPOLO2-26328182_standard_v400.jpg', 145.00, '2017-08-08 01:58:50', '2018-07-06 03:52:47', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'tompkins-skinny-crop-jeans', 5),
	(33, 9999, '', 'resources/assets/images/product_images/1502201105.pPOLO2-26328155_standard_v400.jpg', 215.00, '2017-08-08 02:05:05', '2018-07-06 03:52:45', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'tompkins-skinny-jean', 6),
	(34, 9999, '', 'resources/assets/images/product_images/1502261147.pPOLO2-25480910_alternate2_v360x480.jpg', 468.00, '2017-08-09 06:45:47', '2018-07-06 03:51:51', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'small-sullivan-saddle-bag', 4),
	(35, 9999, '', 'resources/assets/images/product_images/1502261648.pPOLO2-26161986_standard_v400.jpg', 128.00, '2017-08-09 06:54:08', '2018-07-06 03:51:46', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'patchwork-canvas-big-pony-tote', 5),
	(36, 9999, '', 'resources/assets/images/product_images/1502261990.pPOLO2-26161985_standard_v400.jpg', 98.00, '2017-08-09 06:59:50', '2018-07-06 03:51:41', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'color-blocked-big-pony-tote', 6),
	(37, 9999, '', 'resources/assets/images/product_images/1502262425.pPOLO2-25480914_standard_v400.jpg', 398.00, '2017-08-09 07:07:05', '2018-07-06 03:51:36', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'laser-cut-floral-leather-tote', 4),
	(38, 9999, '', 'resources/assets/images/product_images/1502263616.pPOLO2-11724079_lifestyle_v400.jpg', 29.50, '2017-08-09 07:26:56', '2018-07-06 03:51:30', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'bear-print-cotton-coverall', 5),
	(39, 9999, '', 'resources/assets/images/product_images/1502264917.pPOLO2-21465903_lifestyle_v400.jpg', 29.50, '2017-08-09 07:48:37', '2018-07-06 03:51:24', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'striped-cotton-henley-coverall', 6),
	(40, 9999, '', 'resources/assets/images/product_images/1502265209.pPOLO2-21466203_lifestyle_v400.jpg', 29.50, '2017-08-09 07:53:29', '2018-07-06 03:51:19', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'striped-cotton-henley-bodysuit', 4),
	(41, 9999, '', 'resources/assets/images/product_images/1502265614.pPOLO2-22839467_lifestyle_v400.jpg', 103.50, '2017-08-09 08:00:14', '2018-07-06 03:51:15', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'abc-block-4-piece-gift-basket', 5),
	(42, 9999, '', 'resources/assets/images/product_images/1502267748.pPOLO2-26397584_standard_v400.jpg', 25.00, '2017-08-09 08:35:48', '2018-07-06 03:51:05', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'cotton-polo-dress-bloomer', 6),
	(43, 9999, '', 'resources/assets/images/product_images/1502268005.pPOLO2-25655666_standard_v400.jpg', 35.00, '2017-08-09 08:40:05', '2018-07-06 03:51:00', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 3, 0, 0, 'striped-polo-dress-bloomer', 4),
	(44, 9999, '', 'resources/assets/images/product_images/1502268706.pPOLO2-25240665_standard_v400.jpg', 55.50, '2017-08-09 08:51:46', '2018-07-06 03:50:29', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 2, 0, 0, 'ruffled-floral-dress-bloomer', 5),
	(45, 9999, '', 'resources/assets/images/product_images/1502273498.pPOLO2-26000954_standard_v400.jpg', 29.50, '2017-08-09 10:11:38', '2018-07-06 03:50:23', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'striped-cotton-blanket', 6),
	(46, 9999, '', 'resources/assets/images/product_images/1502273672.pPOLO2-23450031_lifestyle_v400.jpg', 75.00, '2017-08-09 10:14:32', '2018-07-06 03:50:17', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'crepe-swaddling-blanket-set', 4),
	(47, 9999, '', 'resources/assets/images/product_images/1502273766.pPOLO2-23700424_standard_v400.jpg', 45.00, '2017-08-09 10:16:06', '2018-07-06 03:50:07', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'plush-blanket', 5),
	(48, 9999, '', 'resources/assets/images/product_images/1502274870.pPOLO2-25426585_alternate7_v360x480.jpg', 470.00, '2017-08-09 10:34:30', '2018-07-06 03:50:00', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'rl-bowery-sateen-duvet-cover', 6),
	(49, 9999, '', 'resources/assets/images/product_images/1502275397.pPOLO2-23742156_standard_v360x480.jpg', 500.00, '2017-08-09 10:43:17', '2018-07-06 03:49:54', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'bedford-jacquard-duvet-cover', 4),
	(50, 9999, '', 'resources/assets/images/product_images/1502275538.pPOLO2-25426632_alternate7_v400.jpg', 160.00, '2017-08-09 10:45:38', '2018-07-06 03:49:50', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'monaco-sateen-duvet-cover', 5),
	(51, 9999, '', 'resources/assets/images/product_images/1502276177.pPOLO2-18063379_mailer_v360x480.jpg', 130.00, '2017-08-09 10:56:17', '2018-07-06 03:49:45', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'pink-palmer-sham', 6),
	(52, 9999, '', 'resources/assets/images/product_images/1502278983.pPOLO2-16519324_lifestyle_v400.jpg', 255.00, '2017-08-09 11:43:03', '2018-07-06 03:49:41', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'rl-beaded-dylan-pillow', 4),
	(53, 9999, '', 'resources/assets/images/product_images/1502279135.pPOLO2-13318847_lifestyle_v400.jpg', 595.00, '2017-08-09 11:45:35', '2018-07-06 03:49:33', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'great-basin-throw-pillow', 5),
	(54, 9999, '', 'resources/assets/images/product_images/1502279578.pPOLO2-18086941_lifestyle_v400.jpg', 395.00, '2017-08-09 11:52:58', '2018-07-06 03:48:36', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'cabled-cashmere-travel-set', 6),
	(55, 9999, '', 'resources/assets/images/product_images/1502282050.pPOLO2-18177063_lifestyle_v360x480.jpg', 5.00, '2017-08-09 12:34:10', '2018-07-06 03:48:31', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'langdon-embroidered-bathrobe', 4),
	(56, 9999, '', 'resources/assets/images/product_images/1502347627.pPOLO2-25995642_standard_v400.jpg', 49.50, '2017-08-10 06:47:07', '2018-07-06 03:48:26', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'cotton-mesh-polo-shirt-1', 5),
	(57, 9999, '', 'resources/assets/images/product_images/1502348404.pPOLO2-23625768_standard_v400.jpg', 29.50, '2017-08-10 07:00:04', '2018-07-06 03:48:22', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'cotton-mesh-polo-shirt', 6),
	(58, 9999, '', 'resources/assets/images/product_images/1502349078.pPOLO2-25961612_standard_v400.jpg', 55.00, '2017-08-10 07:11:18', '2018-07-06 03:48:18', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 0, 0, 0, 'banner-cotton-mesh-polo', 4),
	(59, 9999, '', 'resources/assets/images/product_images/1502349501.pPOLO2-25563187_standard_v400.jpg', 45.00, '2017-08-10 07:18:21', '2018-07-06 03:48:13', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'cotton-mesh-long-sleeve-polo', 5),
	(60, 9999, '', 'resources/assets/images/product_images/1502350673.pPOLO2-25961171_standard_v400.jpg', 45.00, '2017-08-10 07:37:53', '2018-07-06 03:48:09', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 0, 0, 0, 'plaid-cotton-twill-workshirt', 6),
	(61, 9999, '', 'resources/assets/images/product_images/1502351678.pPOLO2-25961083_standard_v400.jpg', 45.00, '2017-08-10 07:54:38', '2018-07-06 03:48:04', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'cotton-oxford-sport-shirt', 4),
	(62, 9999, '', 'resources/assets/images/product_images/1502351882.pPOLO2-24921004_standard_v400.jpg', 55.00, '2017-08-10 07:58:02', '2018-07-06 03:48:00', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 4, 0, 0, 'cotton-mesh-workshirt', 5),
	(63, 9999, '', 'resources/assets/images/product_images/1502352055.pPOLO2-25363631_standard_v400.jpg', 50.00, '2017-08-10 08:00:55', '2018-07-06 03:47:51', NULL, '0.500', 'Kilogram', 1, 0, 0, 0, 1, 0, 0, 'striped-cotton-shirt-1', 6),
	(64, 9999, '', 'resources/assets/images/product_images/1502352545.pPOLO2-25363631_standard_v400.jpg', 55.00, '2017-08-10 08:09:05', '2018-07-06 03:46:40', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'striped-cotton-shirt', 4),
	(65, 9999, '', 'resources/assets/images/product_images/1502353123.pPOLO2-10531663_standard_v400.jpg', 45.00, '2017-08-10 08:18:43', '2018-07-06 03:46:35', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'slim-mott-wash-jean', 5),
	(66, 9999, '', 'resources/assets/images/product_images/1502359349.pPOLO2-25961644_lifestyle_v400.jpg', 39.50, '2017-08-10 10:02:29', '2018-07-06 03:46:29', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'eldridge-stretch-skinny-jean', 6),
	(67, 9999, '', 'resources/assets/images/product_images/1502362089.pPOLO2-14689748_standard_v400.jpg', 550.00, '2017-08-10 10:48:09', '2018-07-06 03:46:25', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'polo-i-wool-twill-suit', 4),
	(68, 9999, '', 'resources/assets/images/product_images/1502362408.pPOLO2-24922918_standard_v400.jpg', 49.50, '2017-08-10 10:53:28', '2018-07-06 03:46:19', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'belted-stretch-cotton-chino', 5),
	(69, 9999, '', 'resources/assets/images/product_images/1502362738.pPOLO2-25464515_lifestyle_v400.jpg', 40.00, '2017-08-10 10:58:58', '2018-07-06 03:46:14', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'batten-canvas-ez-boat-shoe', 6),
	(70, 9999, '', 'resources/assets/images/product_images/1502363119.pPOLO2-25464682_standard_v400.jpg', 45.00, '2017-08-10 11:05:19', '2018-07-06 03:46:09', NULL, '0.500', 'g', 1, 1, 0, 0, 4, 0, 0, 'propell-ii-sneaker', 4),
	(71, 9999, '', 'resources/assets/images/product_images/1502363378.pPOLO2-25464575_standard_v400.jpg', 30.00, '2017-08-10 11:09:38', '2018-07-06 03:46:04', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 1, 0, 0, 'banks-sandal', 5),
	(72, 9999, '', 'resources/assets/images/product_images/1502364150.pPOLO2-26091141_standard_v400.jpg', 59.00, '2017-08-10 11:22:30', '2018-07-06 03:45:59', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 2, 0, 0, 'madras-cotton-shirtdress', 6),
	(73, 9999, '', 'resources/assets/images/product_images/1502364697.pPOLO2-23643008_standard_v400.jpg', 55.00, '2017-08-10 11:31:37', '2018-07-06 03:46:45', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 5, 0, 0, 'cotton-chino-belted-shirtdress', 4),
	(74, 9999, '', 'resources/assets/images/product_images/1502364932.pPOLO2-25835439_standard_v400.jpg', 45.00, '2017-08-10 11:35:32', '2018-07-06 03:45:23', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'striped-off-the-shoulder-dress', 5),
	(75, 9999, '', 'resources/assets/images/product_images/1502365189.pPOLO2-26091856_standard_v400.jpg', 49.50, '2017-08-10 11:39:49', '2018-07-06 03:45:19', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'button-front-denim-skirt', 6),
	(76, 9999, '', 'resources/assets/images/product_images/1502365515.pPOLO2-26091862_alternate1_v400.jpg', 49.50, '2017-08-10 11:45:15', '2018-07-10 10:55:27', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 6, 0, 0, 'floral-skirt', 4),
	(77, 9996, '', 'resources/assets/images/product_images/1502366105.pPOLO2-26091049_alternate1_v400.jpg', 56.50, '2017-08-10 11:55:05', '2018-07-06 03:41:22', NULL, '0.500', 'Kilogram', 1, 1, 0, 3, 4, 0, 0, 'pleated-madras-skirt', 5),
	(78, 9999, '', 'resources/assets/images/product_images/1502366342.pPOLO2-26090785_standard_v400.jpg', 195.00, '2017-08-10 11:59:02', '2018-07-06 03:40:21', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 6, 0, 0, 'cable-knit-cashmere-sweater', 6),
	(79, 9999, '', 'resources/assets/images/product_images/1502366462.pPOLO2-26090829_standard_v400.jpg', 45.00, '2017-08-10 12:01:02', '2018-07-06 03:37:12', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 4, 0, 0, 'fair-isle-hooded-sweater', 4),
	(80, 9999, '', 'resources/assets/images/product_images/1502366586.pPOLO2-25834797_standard_v400.jpg', 125.00, '2017-08-10 12:03:06', '2018-07-06 03:36:56', NULL, '0.500', 'Kilogram', 1, 1, 0, 0, 3, 0, 0, 'flag-combed-cotton-sweater', 5),
	(81, 9998, '', 'resources/assets/images/product_images/1502366686.pPOLO2-25207761_standard_v400.jpg', 49.50, '2017-08-10 12:04:46', '2018-07-06 03:35:40', NULL, '0.500', 'Kilogram', 1, 0, 0, 1, 3, 0, 0, 'ruffled-cotton-cardigan', 6);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table shop.products_attributes
CREATE TABLE IF NOT EXISTS `products_attributes` (
  `products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `options_id` int(11) NOT NULL,
  `options_values_id` int(11) NOT NULL,
  `options_values_price` decimal(15,2) NOT NULL,
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_attributes_id`),
  KEY `idx_products_attributes_products_id` (`products_id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_attributes: ~420 rows (approximately)
/*!40000 ALTER TABLE `products_attributes` DISABLE KEYS */;
INSERT INTO `products_attributes` (`products_attributes_id`, `products_id`, `options_id`, `options_values_id`, `options_values_price`, `price_prefix`, `is_default`) VALUES
	(1, 1, 1, 1, 0.00, '+', 0),
	(2, 1, 1, 2, 0.00, '+', 0),
	(3, 1, 1, 3, 0.00, '+', 0),
	(4, 1, 1, 4, 0.00, '+', 0),
	(5, 1, 1, 5, 0.00, '+', 0),
	(6, 1, 1, 6, 0.00, '+', 0),
	(7, 1, 1, 7, 0.00, '+', 0),
	(8, 1, 1, 8, 0.00, '+', 0),
	(9, 1, 1, 9, 0.00, '+', 0),
	(10, 1, 4, 29, 0.00, '+', 0),
	(11, 1, 4, 32, 0.00, '+', 0),
	(12, 1, 4, 35, 0.00, '+', 0),
	(13, 1, 4, 38, 0.00, '+', 0),
	(14, 1, 2, 20, 0.00, '+', 0),
	(15, 1, 2, 21, 0.00, '+', 0),
	(16, 1, 2, 22, 0.00, '+', 0),
	(17, 1, 2, 23, 0.00, '+', 0),
	(18, 1, 2, 24, 0.00, '+', 0),
	(19, 1, 2, 25, 0.00, '+', 0),
	(20, 1, 2, 26, 0.00, '+', 0),
	(21, 1, 2, 27, 0.00, '+', 0),
	(22, 1, 2, 28, 0.00, '+', 0),
	(23, 1, 5, 30, 0.00, '+', 0),
	(24, 1, 5, 34, 0.00, '+', 0),
	(25, 1, 5, 36, 0.00, '+', 0),
	(26, 1, 5, 40, 0.00, '+', 0),
	(27, 1, 3, 10, 0.00, '+', 0),
	(28, 1, 3, 11, 0.00, '+', 0),
	(29, 1, 3, 12, 0.00, '+', 0),
	(30, 1, 3, 13, 0.00, '+', 0),
	(31, 1, 3, 14, 0.00, '+', 0),
	(32, 1, 3, 15, 0.00, '+', 0),
	(33, 1, 3, 16, 0.00, '+', 0),
	(34, 1, 3, 17, 0.00, '+', 0),
	(35, 1, 3, 18, 0.00, '+', 0),
	(36, 1, 6, 31, 0.00, '+', 0),
	(37, 1, 6, 33, 0.00, '+', 0),
	(38, 1, 6, 37, 0.00, '+', 0),
	(39, 1, 6, 39, 0.00, '+', 0),
	(40, 4, 4, 29, 0.00, '+', 0),
	(41, 4, 4, 32, 0.00, '+', 0),
	(42, 4, 4, 35, 0.00, '+', 0),
	(43, 4, 4, 38, 0.00, '+', 0),
	(44, 4, 5, 30, 0.00, '+', 0),
	(45, 4, 5, 34, 0.00, '+', 0),
	(46, 4, 5, 36, 0.00, '+', 0),
	(47, 4, 5, 40, 0.00, '+', 0),
	(48, 4, 6, 31, 0.00, '+', 0),
	(49, 4, 6, 33, 0.00, '+', 0),
	(50, 4, 6, 37, 0.00, '+', 0),
	(51, 4, 6, 39, 0.00, '+', 0),
	(52, 5, 4, 29, 0.00, '+', 0),
	(53, 5, 4, 32, 0.00, '+', 0),
	(54, 5, 4, 35, 0.00, '+', 0),
	(55, 5, 5, 30, 0.00, '+', 0),
	(56, 5, 5, 34, 0.00, '+', 0),
	(57, 5, 5, 36, 0.00, '+', 0),
	(58, 5, 6, 31, 0.00, '+', 0),
	(59, 5, 6, 33, 0.00, '+', 0),
	(60, 5, 6, 37, 0.00, '+', 0),
	(61, 6, 4, 29, 0.00, '+', 0),
	(62, 6, 4, 32, 0.00, '+', 0),
	(63, 6, 4, 35, 0.00, '+', 0),
	(64, 6, 4, 38, 0.00, '+', 0),
	(65, 6, 5, 30, 0.00, '+', 0),
	(66, 6, 5, 34, 0.00, '+', 0),
	(67, 6, 5, 36, 0.00, '+', 0),
	(68, 6, 5, 40, 0.00, '+', 0),
	(69, 6, 6, 31, 0.00, '+', 0),
	(70, 6, 6, 33, 0.00, '+', 0),
	(71, 6, 6, 37, 0.00, '+', 0),
	(72, 6, 6, 39, 0.00, '+', 0),
	(73, 7, 4, 32, 0.00, '+', 0),
	(74, 7, 4, 35, 0.00, '+', 0),
	(75, 7, 5, 34, 0.00, '+', 0),
	(76, 7, 5, 36, 0.00, '+', 0),
	(77, 7, 6, 33, 0.00, '+', 0),
	(78, 7, 6, 37, 0.00, '+', 0),
	(79, 8, 4, 29, 0.00, '+', 0),
	(80, 8, 4, 32, 0.00, '+', 0),
	(81, 8, 5, 30, 0.00, '+', 0),
	(82, 8, 5, 34, 0.00, '+', 0),
	(83, 8, 6, 31, 0.00, '+', 0),
	(84, 8, 6, 33, 0.00, '+', 0),
	(85, 9, 4, 32, 0.00, '+', 0),
	(86, 9, 4, 35, 0.00, '+', 0),
	(87, 9, 4, 38, 0.00, '+', 0),
	(88, 9, 5, 34, 0.00, '+', 0),
	(89, 9, 5, 36, 0.00, '+', 0),
	(90, 9, 5, 40, 0.00, '+', 0),
	(91, 9, 6, 33, 0.00, '+', 0),
	(92, 9, 6, 37, 0.00, '+', 0),
	(93, 9, 6, 39, 0.00, '+', 0),
	(94, 10, 4, 35, 0.00, '+', 0),
	(95, 10, 4, 38, 0.00, '+', 0),
	(96, 10, 5, 36, 0.00, '+', 0),
	(97, 10, 5, 40, 0.00, '+', 0),
	(98, 10, 6, 37, 0.00, '+', 0),
	(99, 10, 6, 39, 0.00, '+', 0),
	(100, 11, 7, 41, 0.00, '+', 0),
	(101, 11, 7, 42, 0.00, '+', 0),
	(102, 11, 7, 43, 0.00, '+', 0),
	(103, 11, 8, 49, 0.00, '+', 0),
	(104, 11, 8, 50, 0.00, '+', 0),
	(105, 11, 8, 51, 0.00, '+', 0),
	(106, 11, 9, 57, 0.00, '+', 0),
	(107, 11, 9, 58, 0.00, '+', 0),
	(108, 11, 9, 59, 0.00, '+', 0),
	(109, 11, 10, 65, 0.00, '+', 0),
	(110, 11, 10, 66, 0.00, '+', 0),
	(111, 11, 11, 68, 0.00, '+', 0),
	(112, 11, 11, 69, 0.00, '+', 0),
	(113, 11, 12, 71, 0.00, '+', 0),
	(114, 11, 12, 72, 0.00, '+', 0),
	(115, 12, 1, 1, 0.00, '+', 0),
	(116, 12, 1, 3, 0.00, '+', 0),
	(117, 12, 2, 20, 0.00, '+', 0),
	(118, 12, 2, 22, 0.00, '+', 0),
	(119, 12, 3, 10, 0.00, '+', 0),
	(120, 12, 3, 12, 0.00, '+', 0),
	(121, 12, 7, 44, 0.00, '+', 0),
	(122, 12, 7, 45, 0.00, '+', 0),
	(123, 12, 7, 46, 0.00, '+', 0),
	(124, 12, 7, 47, 0.00, '+', 0),
	(125, 12, 8, 52, 0.00, '+', 0),
	(126, 12, 8, 53, 0.00, '+', 0),
	(127, 12, 8, 54, 0.00, '+', 0),
	(128, 12, 8, 55, 0.00, '+', 0),
	(129, 12, 9, 60, 0.00, '+', 0),
	(130, 12, 9, 61, 0.00, '+', 0),
	(131, 12, 9, 62, 0.00, '+', 0),
	(132, 12, 9, 63, 0.00, '+', 0),
	(133, 12, 10, 65, 0.00, '+', 0),
	(134, 12, 10, 66, 0.00, '+', 0),
	(135, 12, 10, 67, 0.00, '+', 0),
	(136, 12, 11, 68, 0.00, '+', 0),
	(137, 12, 11, 69, 0.00, '+', 0),
	(138, 12, 11, 70, 0.00, '+', 0),
	(139, 12, 12, 71, 0.00, '+', 0),
	(140, 12, 12, 72, 0.00, '+', 0),
	(141, 12, 12, 73, 0.00, '+', 0),
	(142, 13, 7, 41, 0.00, '+', 0),
	(143, 13, 7, 42, 0.00, '+', 0),
	(144, 13, 8, 49, 0.00, '+', 0),
	(145, 13, 8, 50, 0.00, '+', 0),
	(146, 13, 9, 57, 0.00, '+', 0),
	(147, 13, 9, 58, 0.00, '+', 0),
	(148, 13, 10, 65, 0.00, '+', 0),
	(149, 13, 10, 67, 0.00, '+', 0),
	(150, 13, 11, 68, 0.00, '+', 0),
	(151, 13, 11, 70, 0.00, '+', 0),
	(152, 13, 12, 71, 0.00, '+', 0),
	(153, 13, 12, 73, 0.00, '+', 0),
	(154, 14, 4, 74, 0.00, '+', 0),
	(155, 14, 4, 75, 0.00, '+', 0),
	(156, 14, 4, 76, 0.00, '+', 0),
	(157, 14, 4, 77, 0.00, '+', 0),
	(158, 14, 5, 78, 0.00, '+', 0),
	(159, 14, 5, 79, 0.00, '+', 0),
	(160, 14, 5, 80, 0.00, '+', 0),
	(161, 14, 5, 81, 0.00, '+', 0),
	(162, 14, 6, 82, 0.00, '+', 0),
	(163, 14, 6, 83, 0.00, '+', 0),
	(164, 14, 6, 84, 0.00, '+', 0),
	(165, 14, 6, 85, 0.00, '+', 0),
	(166, 15, 4, 76, 0.00, '+', 0),
	(167, 15, 5, 80, 0.00, '+', 0),
	(168, 15, 6, 85, 0.00, '+', 0),
	(169, 16, 4, 75, 0.00, '+', 0),
	(170, 16, 4, 76, 0.00, '+', 0),
	(171, 16, 4, 77, 0.00, '+', 0),
	(172, 16, 5, 79, 0.00, '+', 0),
	(173, 16, 5, 80, 0.00, '+', 0),
	(174, 16, 5, 81, 0.00, '+', 0),
	(175, 16, 6, 83, 0.00, '+', 0),
	(176, 16, 6, 84, 0.00, '+', 0),
	(177, 16, 6, 85, 0.00, '+', 0),
	(178, 17, 1, 5, 0.00, '+', 0),
	(179, 17, 2, 24, 0.00, '+', 0),
	(180, 17, 3, 14, 0.00, '+', 0),
	(181, 17, 4, 74, 0.00, '+', 0),
	(182, 17, 4, 75, 0.00, '+', 0),
	(183, 17, 5, 78, 0.00, '+', 0),
	(184, 17, 5, 79, 0.00, '+', 0),
	(185, 17, 6, 82, 0.00, '+', 0),
	(186, 17, 6, 83, 0.00, '+', 0),
	(187, 19, 4, 76, 0.00, '+', 0),
	(188, 19, 4, 77, 0.00, '+', 0),
	(189, 19, 5, 80, 0.00, '+', 0),
	(190, 19, 5, 81, 0.00, '+', 0),
	(191, 19, 6, 84, 0.00, '+', 0),
	(192, 19, 6, 85, 0.00, '+', 0),
	(193, 24, 4, 29, 0.00, '+', 0),
	(194, 24, 4, 32, 0.00, '+', 0),
	(195, 24, 4, 35, 0.00, '+', 0),
	(196, 24, 5, 30, 0.00, '+', 0),
	(197, 24, 5, 34, 0.00, '+', 0),
	(198, 24, 5, 36, 0.00, '+', 0),
	(199, 24, 6, 31, 0.00, '+', 0),
	(200, 24, 6, 33, 0.00, '+', 0),
	(201, 24, 6, 37, 0.00, '+', 0),
	(202, 25, 4, 29, 0.00, '+', 0),
	(203, 25, 5, 30, 0.00, '+', 0),
	(204, 25, 6, 31, 0.00, '+', 0),
	(205, 26, 4, 32, 0.00, '+', 0),
	(206, 26, 5, 34, 0.00, '+', 0),
	(207, 26, 6, 33, 0.00, '+', 0),
	(208, 27, 4, 32, 0.00, '+', 0),
	(209, 27, 4, 35, 0.00, '+', 0),
	(210, 27, 5, 34, 0.00, '+', 0),
	(211, 27, 5, 36, 0.00, '+', 0),
	(212, 27, 6, 33, 0.00, '+', 0),
	(213, 27, 6, 37, 0.00, '+', 0),
	(214, 28, 4, 29, 0.00, '+', 0),
	(215, 28, 4, 32, 0.00, '+', 0),
	(216, 28, 5, 30, 0.00, '+', 0),
	(217, 28, 5, 34, 0.00, '+', 0),
	(218, 28, 6, 31, 0.00, '+', 0),
	(219, 28, 6, 33, 0.00, '+', 0),
	(220, 29, 4, 29, 0.00, '+', 0),
	(221, 29, 4, 32, 0.00, '+', 0),
	(222, 29, 4, 35, 0.00, '+', 0),
	(223, 29, 4, 38, 0.00, '+', 0),
	(224, 29, 5, 30, 0.00, '+', 0),
	(225, 29, 5, 34, 0.00, '+', 0),
	(226, 29, 5, 36, 0.00, '+', 0),
	(227, 29, 5, 40, 0.00, '+', 0),
	(228, 29, 6, 31, 0.00, '+', 0),
	(229, 29, 6, 33, 0.00, '+', 0),
	(230, 29, 6, 37, 0.00, '+', 0),
	(231, 29, 6, 39, 0.00, '+', 0),
	(232, 30, 4, 29, 0.00, '+', 0),
	(233, 30, 5, 30, 0.00, '+', 0),
	(234, 30, 6, 31, 0.00, '+', 0),
	(235, 31, 4, 87, 0.00, '+', 0),
	(236, 31, 4, 88, 0.00, '+', 0),
	(237, 31, 4, 89, 0.00, '+', 0),
	(238, 31, 4, 90, 0.00, '+', 0),
	(239, 31, 5, 86, 0.00, '+', 0),
	(240, 31, 5, 94, 0.00, '+', 0),
	(241, 31, 5, 95, 0.00, '+', 0),
	(242, 31, 5, 96, 0.00, '+', 0),
	(243, 31, 6, 100, 0.00, '+', 0),
	(244, 31, 6, 101, 0.00, '+', 0),
	(245, 31, 6, 102, 0.00, '+', 0),
	(246, 31, 6, 103, 0.00, '+', 0),
	(247, 32, 4, 87, 0.00, '+', 0),
	(248, 32, 4, 88, 0.00, '+', 0),
	(249, 32, 4, 89, 0.00, '+', 0),
	(250, 32, 4, 90, 0.00, '+', 0),
	(251, 32, 5, 86, 0.00, '+', 0),
	(252, 32, 5, 94, 0.00, '+', 0),
	(253, 32, 5, 95, 0.00, '+', 0),
	(254, 32, 5, 96, 0.00, '+', 0),
	(255, 32, 6, 100, 0.00, '+', 0),
	(256, 32, 6, 101, 0.00, '+', 0),
	(257, 32, 6, 102, 0.00, '+', 0),
	(258, 32, 6, 103, 0.00, '+', 0),
	(259, 33, 4, 91, 0.00, '+', 0),
	(260, 33, 4, 92, 0.00, '+', 0),
	(261, 33, 4, 93, 0.00, '+', 0),
	(262, 33, 5, 97, 0.00, '+', 0),
	(263, 33, 5, 98, 0.00, '+', 0),
	(264, 33, 5, 99, 0.00, '+', 0),
	(265, 33, 6, 104, 0.00, '+', 0),
	(266, 33, 6, 105, 0.00, '+', 0),
	(267, 33, 6, 106, 0.00, '+', 0),
	(268, 38, 4, 107, 0.00, '+', 0),
	(269, 38, 4, 110, 0.00, '+', 0),
	(270, 38, 4, 111, 0.00, '+', 0),
	(271, 38, 5, 109, 0.00, '+', 0),
	(272, 38, 5, 113, 0.00, '+', 0),
	(273, 38, 5, 114, 0.00, '+', 0),
	(274, 38, 6, 108, 0.00, '+', 0),
	(275, 38, 6, 116, 0.00, '+', 0),
	(276, 38, 6, 117, 0.00, '+', 0),
	(277, 39, 4, 111, 0.00, '+', 0),
	(278, 39, 5, 114, 0.00, '+', 0),
	(279, 39, 6, 117, 0.00, '+', 0),
	(280, 40, 4, 107, 0.00, '+', 0),
	(281, 40, 4, 110, 0.00, '+', 0),
	(282, 40, 5, 109, 0.00, '+', 0),
	(283, 40, 5, 113, 0.00, '+', 0),
	(284, 40, 6, 108, 0.00, '+', 0),
	(285, 40, 6, 116, 0.00, '+', 0),
	(286, 42, 4, 111, 0.00, '+', 0),
	(287, 42, 4, 112, 0.00, '+', 0),
	(288, 42, 5, 114, 0.00, '+', 0),
	(289, 42, 5, 115, 0.00, '+', 0),
	(290, 42, 6, 117, 0.00, '+', 0),
	(291, 42, 6, 118, 0.00, '+', 0),
	(292, 43, 4, 112, 0.00, '+', 0),
	(293, 43, 5, 115, 0.00, '+', 0),
	(294, 43, 6, 118, 0.00, '+', 0),
	(295, 44, 4, 111, 0.00, '+', 0),
	(296, 44, 4, 112, 0.00, '+', 0),
	(297, 44, 5, 114, 0.00, '+', 0),
	(298, 44, 5, 115, 0.00, '+', 0),
	(299, 44, 6, 117, 0.00, '+', 0),
	(300, 44, 6, 118, 0.00, '+', 0),
	(301, 50, 4, 125, 0.00, '+', 0),
	(302, 50, 5, 127, 0.00, '+', 0),
	(303, 50, 6, 126, 0.00, '+', 0),
	(304, 48, 1, 119, 0.00, '+', 0),
	(305, 48, 1, 122, 0.00, '+', 0),
	(306, 48, 2, 121, 0.00, '+', 0),
	(307, 48, 2, 123, 0.00, '+', 0),
	(308, 48, 3, 120, 0.00, '+', 0),
	(309, 48, 3, 124, 0.00, '+', 0),
	(310, 48, 4, 125, 0.00, '+', 0),
	(311, 48, 4, 130, 0.00, '+', 0),
	(312, 48, 5, 127, 0.00, '+', 0),
	(313, 48, 5, 129, 0.00, '+', 0),
	(314, 48, 6, 126, 0.00, '+', 0),
	(315, 48, 6, 128, 0.00, '+', 0),
	(316, 49, 4, 130, 0.00, '+', 0),
	(317, 49, 5, 129, 0.00, '+', 0),
	(318, 49, 6, 128, 0.00, '+', 0),
	(319, 51, 4, 125, 0.00, '+', 0),
	(320, 51, 5, 127, 0.00, '+', 0),
	(321, 51, 6, 126, 0.00, '+', 0),
	(322, 52, 1, 119, 0.00, '+', 0),
	(323, 52, 2, 121, 0.00, '+', 0),
	(324, 52, 3, 120, 0.00, '+', 0),
	(325, 53, 4, 132, 0.00, '+', 0),
	(326, 53, 5, 135, 0.00, '+', 0),
	(327, 53, 6, 136, 0.00, '+', 0),
	(328, 52, 4, 131, 0.00, '+', 0),
	(329, 52, 5, 133, 0.00, '+', 0),
	(330, 52, 6, 134, 0.00, '+', 0),
	(331, 54, 1, 2, 0.00, '+', 0),
	(332, 54, 1, 3, 0.00, '+', 0),
	(333, 54, 2, 21, 0.00, '+', 0),
	(334, 54, 2, 22, 0.00, '+', 0),
	(335, 54, 3, 11, 0.00, '+', 0),
	(336, 54, 3, 12, 0.00, '+', 0),
	(337, 55, 4, 29, 0.00, '+', 0),
	(338, 55, 4, 32, 0.00, '+', 0),
	(339, 55, 4, 35, 0.00, '+', 0),
	(341, 55, 5, 30, 0.00, '+', 0),
	(342, 55, 5, 34, 0.00, '+', 0),
	(343, 55, 5, 36, 0.00, '+', 0),
	(345, 55, 6, 31, 0.00, '+', 0),
	(346, 55, 6, 33, 0.00, '+', 0),
	(347, 55, 6, 37, 0.00, '+', 0),
	(351, 56, 4, 137, 0.00, '+', 0),
	(352, 56, 5, 140, 0.00, '+', 0),
	(353, 56, 6, 143, 0.00, '+', 0),
	(354, 57, 1, 2, 0.00, '+', 0),
	(355, 57, 1, 3, 0.00, '+', 0),
	(356, 57, 2, 21, 0.00, '+', 0),
	(357, 57, 2, 22, 0.00, '+', 0),
	(358, 57, 3, 11, 0.00, '+', 0),
	(359, 57, 3, 12, 0.00, '+', 0),
	(360, 57, 4, 138, 0.00, '+', 0),
	(361, 57, 5, 141, 0.00, '+', 0),
	(362, 57, 6, 145, 0.00, '+', 0),
	(363, 58, 4, 29, 0.00, '+', 0),
	(364, 58, 4, 32, 0.00, '+', 0),
	(365, 58, 4, 35, 0.00, '+', 0),
	(366, 58, 5, 30, 0.00, '+', 0),
	(367, 58, 5, 34, 0.00, '+', 0),
	(368, 58, 5, 36, 0.00, '+', 0),
	(369, 58, 6, 31, 0.00, '+', 0),
	(370, 58, 6, 33, 0.00, '+', 0),
	(371, 58, 6, 37, 0.00, '+', 0),
	(372, 59, 4, 29, 0.00, '+', 0),
	(373, 59, 5, 30, 0.00, '+', 0),
	(374, 59, 6, 31, 0.00, '+', 0),
	(375, 60, 4, 35, 0.00, '+', 0),
	(376, 60, 5, 36, 0.00, '+', 0),
	(377, 60, 6, 37, 0.00, '+', 0),
	(378, 61, 4, 32, 0.00, '+', 0),
	(379, 61, 4, 35, 0.00, '+', 0),
	(380, 61, 5, 34, 0.00, '+', 0),
	(381, 61, 5, 36, 0.00, '+', 0),
	(382, 61, 6, 33, 0.00, '+', 0),
	(383, 61, 6, 37, 0.00, '+', 0),
	(384, 63, 4, 29, 0.00, '+', 0),
	(385, 63, 4, 35, 0.00, '+', 0),
	(386, 63, 5, 30, 0.00, '+', 0),
	(387, 63, 5, 36, 0.00, '+', 0),
	(388, 63, 6, 31, 0.00, '+', 0),
	(389, 63, 6, 37, 0.00, '+', 0),
	(390, 66, 7, 41, 0.00, '+', 0),
	(391, 66, 8, 49, 0.00, '+', 0),
	(392, 66, 9, 57, 0.00, '+', 0),
	(393, 66, 10, 65, 0.00, '+', 0),
	(394, 66, 11, 68, 0.00, '+', 0),
	(395, 66, 12, 71, 0.00, '+', 0),
	(396, 67, 4, 32, 0.00, '+', 0),
	(397, 67, 4, 35, 0.00, '+', 0),
	(398, 67, 5, 34, 0.00, '+', 0),
	(399, 67, 5, 36, 0.00, '+', 0),
	(400, 67, 6, 33, 0.00, '+', 0),
	(401, 67, 6, 37, 0.00, '+', 0),
	(402, 68, 7, 42, 0.00, '+', 0),
	(403, 68, 8, 50, 0.00, '+', 0),
	(404, 68, 9, 58, 0.00, '+', 0),
	(405, 68, 10, 65, 0.00, '+', 0),
	(406, 68, 11, 68, 0.00, '+', 0),
	(407, 68, 12, 71, 0.00, '+', 0),
	(408, 69, 4, 74, 0.00, '+', 0),
	(409, 69, 4, 75, 0.00, '+', 0),
	(410, 69, 4, 76, 0.00, '+', 0),
	(411, 69, 5, 78, 0.00, '+', 0),
	(412, 69, 5, 79, 0.00, '+', 0),
	(413, 69, 5, 80, 0.00, '+', 0),
	(414, 69, 6, 82, 0.00, '+', 0),
	(415, 69, 6, 83, 0.00, '+', 0),
	(416, 69, 6, 84, 0.00, '+', 0),
	(417, 70, 4, 74, 0.00, '+', 0),
	(418, 70, 4, 75, 0.00, '+', 0),
	(419, 70, 5, 78, 0.00, '+', 0),
	(420, 70, 5, 79, 0.00, '+', 0),
	(421, 70, 6, 82, 0.00, '+', 0),
	(422, 70, 6, 83, 0.00, '+', 0),
	(427, 80, 1, 155, 2000.00, '+', 0),
	(428, 80, 1, 122, 1000.00, '+', 0),
	(429, 80, 4, 29, 6000.00, '+', 0);
/*!40000 ALTER TABLE `products_attributes` ENABLE KEYS */;

-- Dumping structure for table shop.products_attributes_download
CREATE TABLE IF NOT EXISTS `products_attributes_download` (
  `products_attributes_id` int(11) NOT NULL,
  `products_attributes_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_attributes_maxdays` int(2) DEFAULT '0',
  `products_attributes_maxcount` int(2) DEFAULT '0',
  PRIMARY KEY (`products_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_attributes_download: ~0 rows (approximately)
/*!40000 ALTER TABLE `products_attributes_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_attributes_download` ENABLE KEYS */;

-- Dumping structure for table shop.products_description
CREATE TABLE IF NOT EXISTS `products_description` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_description` text COLLATE utf8_unicode_ci,
  `products_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_viewed` int(5) DEFAULT '0',
  PRIMARY KEY (`products_id`,`language_id`),
  KEY `products_name` (`products_name`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_description: ~81 rows (approximately)
/*!40000 ALTER TABLE `products_description` DISABLE KEYS */;
INSERT INTO `products_description` (`products_id`, `language_id`, `products_name`, `products_description`, `products_url`, `products_viewed`) VALUES
	(1, 1, 'CLASSIC FIT SOFT-TOUCH POLO', '<p>- Classic Fit: our roomiest silhouette. Our Polo shirts also come in Slim Fit (our trimmest) and Custom Slim Fit (our second-slimmest). - Size medium has a 29&quot; body length, an 18&quot; shoulder, and a 22&quot; chest. - Ribbed Polo collar. Three-button placket. - Short sleeves with ribbed armbands. - Even vented hem. - Multicolored signature embroidered pony at the left chest. - 100% cotton. - Machine washable. Imported. - Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(2, 1, 'HAMPTON CLASSIC FIT SHIRT', '<p>Classic Fit. Size medium has a 29&frac12;&quot; body length, an 18&frac12;&quot; shoulder, and a 22&frac12;&quot; chest. Medium-spread collar. Four-button extended placket. Genuine mother-of-pearl buttons. Short sleeves. Left chest patch pocket. Even vented hem. 100% cotton. Machine washable. Imported. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(3, 1, 'CLASSIC FIT COTTON POLO SHIRT', 'Classic Fit: our roomiest silhouette. Cut for a lower armhole and a fuller sleeve that falls closer to the elbow.\r\nOur Polo shirts also come in Slim Fit (3" trimmer at the chest and 2" shorter at the body) and Custom Slim Fit (1½" trimmer at the chest and ½" shorter at the body).\r\nSize medium has a 29" body length, an 18" shoulder, and a 22" chest.\r\nRibbed Polo collar. Three-button placket.\r\nShort sleeves with ribbed armbands.\r\nEven vented hem.\r\nSignature embroidered pony at the left chest.\r\n100% cotton. Machine washable. Imported.\r\nModel is 6\'1"/185 cm and wears a size medium.', NULL, 0),
	(4, 1, 'CLASSIC FIT COTTON POLO SHIRT', '<p>Classic Fit: our roomiest silhouette. Cut for a lower armhole and a fuller sleeve that falls closer to the elbow. Our Polo shirts also come in Slim Fit (3&quot; trimmer at the chest and 2&quot; shorter at the body) and Custom Slim Fit (1&frac12;&quot; trimmer at the chest and &frac12;&quot; shorter at the body). Size medium has a 29&quot; body length, an 18&quot; shoulder, and a 22&quot; chest. Ribbed Polo collar. Three-button placket. Short sleeves with ribbed armbands. Even vented hem. Signature embroidered pony at the left chest. 100% cotton. Machine washable. Imported. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(5, 1, 'CUSTOM SLIM FIT COTTON SHIRT', '<p>Custom Slim Fit: our trimmest silhouette. Previously called Custom Fit. Our T-shirts also come in Classic Fit (our roomiest). Size medium has a 27&frac12;&quot; body length, an 18&frac12;&quot; shoulder, and a 21&quot; chest. Striped baseball collar. Two-button placket. Short sleeves with striped ribbed armbands. Striped ribbed piecing at the armholes. Felt &quot;Polo Ralph Lauren&quot; patch at the left chest. Even vented hem. Shell: 100% cotton. Collar: 99% cotton, 1% elastane. Machine washable. Imported. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(6, 1, 'CUSTOM SLIM FIT WEATHERED POLO', '<p>Custom Slim Fit: a middle ground between our Classic Fit and Slim Fit. Sleeve hugs the bicep. Tailored through the waist. Previously called Custom Fit. Our Polo shirts also come in Slim Fit (1&frac12;&quot; trimmer at the chest and &frac12;&quot; shorter at the body) and Classic Fit (1&frac12;&quot; wider at the chest and 1&frac12;&quot; longer at the body). Size medium has a 27&frac12;&quot; front body length, a 28&frac12;&quot; back body length, a 17&frac34;&quot; shoulder, a 20&frac34;&quot; chest, and a 34&frac12;&quot; sleeve length. Sleeve length is taken from the center back of the neck and changes 1&quot; betwe Ribbed Polo collar. Two-button placket. Long sleeves with ribbed cuffs. Tennis tail. Signature embroidered pony at the left chest. 100% cotton. Machine washable. Imported. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(7, 1, 'STANDARD FIT COTTON SHIRT', '<p>Standard Fit: a comfortable, relaxed silhouette. If you favored our Classic Fit or Custom Fit, you will like this updated version. Size medium has a 30&quot; body length. Button-down point collar. Buttoned placket. Short sleeves. Left chest buttoned pocket. 100% cotton. Machine washable. Imported. Coloring may rub off onto fabrics and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(8, 1, 'STANDARD FIT COTTON POPOVER', '<p>Standard Fit: a comfortable, relaxed silhouette. If you favored our Classic Fit or Custom Fit, you will like this updated version. Size medium has a 30&quot; back body length. Button-down point collar. Buttoned half-placket. Short sleeves. Left chest buttoned pocket. Box-pleated back yoke ensures a comfortable fit and a greater range of motion. 100% cotton. Machine washable. Imported. Japanese fabric. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(9, 1, 'STANDARD FIT MADRAS POPOVER', '<p>Standard Fit: a comfortable, relaxed silhouette. If you favored our Classic Fit or Custom Fit, you will like this updated version. Size medium has a 30&quot; body length. Button-down point collar. Three-button placket. Genuine mother-of-pearl buttons. Short sleeves. Left chest patch pocket. Split back yoke with a box pleat ensures a comfortable fit and a greater range of motion. Signature embroidered pony at the left chest pocket. 100% cotton. Machine washable. Imported. Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(10, 1, 'STANDARD FIT COTTON WORKSHIRT', '<p>Standard Fit: a comfortable, relaxed silhouette. If you favored our Classic Fit or Custom Fit, you will like this updated version. Size medium has a 32&quot; body length, an 18&frac12;&quot; shoulder, a 46&frac12;&quot; chest, and a 35&quot; sleeve length. Sleeve length is taken from the center back of the neck and changes 1&quot; between sizes. Point collar. Buttoned placket. Genuine mother-of-pearl buttons. Long sleeves with buttoned barrel cuffs. Two chest buttoned pockets. Split back yoke with a box pleat ensures a comfortable fit and a greater range of motion. 100% cotton. Machine washable. Imported. Italian fabric. Dyed with true indigo, which may rub off onto fabrics, leather, and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size medium.</p>', NULL, 0),
	(11, 1, 'ELDRIDGE SUPER SLIM JEAN', '<p>Eldridge Super Slim: Polo&#39;s skinniest fit. Sits low at the waist. Trim through the thigh and the leg. Tapered leg opening. Size 32W has an 8&frac34;&quot; rise and a 12&frac12;&quot; leg opening. Belt loops. Zip fly with our signature shank closure. Five-pocket styling with signature metal rivets. &quot;Polo&quot; label at the coin pocket. &quot;Polo Ralph Lauren&quot; leather patch at the back right waist. 94% cotton, 4% polyester, 2% elastane. Machine washable. Imported. Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size 32W x 32L.</p>', NULL, 0),
	(12, 1, 'SULLIVAN SLIM FIT JEAN', '<p>Sullivan Slim: sits slightly below the waist. Slim, tapered leg. Size 32W has a 9&quot; rise and a 14&quot; leg opening. Belt loops. Zip fly with our signature shank closure. Five-pocket styling with signature metal rivets. &quot;Polo&quot; label at the coin pocket. &quot;Polo Ralph Lauren&quot;&ndash;debossed leather patch at the back right waist. 97% cotton, 3% elastane. Machine washable. Imported. Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size 32W x 32L.</p>', NULL, 0),
	(13, 1, 'HAMPTON STRAIGHT FIT JEAN', '<p>Hampton Straight: Polo&#39;s most relaxed fit. Sits slightly below the waist. Easy fit through the thigh and slightly tapered at the ankle. Size 32W has a 9&quot; rise and a 16&quot; leg opening. Belt loops. Button fly with our signature shank closure. Five-pocket styling with signature metal rivets. &quot;Polo&quot; label at the coin pocket. &quot;Polo Ralph Lauren&quot;&ndash;debossed leather patch at the back right waist. 100% cotton. Machine washable. Imported. Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery. Model is 6&#39;1&quot;/185 cm and wears a size 32W x 32L.</p>', NULL, 0),
	(14, 1, 'LAXMAN TECH SUEDE SNEAKER', '<p>Rounded toe. Lace-up front. Woven tag with our signature pony at the tongue. Padded velvet insole. Treaded rubber outsole. &quot;Polo&quot; printed at the outer side. Upper: man-made materials. Mesh panels: 100% nylon. Imported.</p>', NULL, 0),
	(15, 1, 'VAUGHN SUEDE SLIP-ON SNEAKER', '<p>Fits true to size. Rounded toe. Slip-on styling. Elasticized side gores. &quot;Polo&quot; tag at the outer side. Padded collar. Signature pony&ndash;debossed suede heel. Padded canvas insole. Treaded rubber outsole. Leather. Imported.</p>', NULL, 0),
	(16, 1, 'WORKINGTON LEATHER DRIVER', '<p>Penny tab at the vamp. Single-needle stitching at the toe. Padded leather insole. Leather outsole with rubber-nub detailing. Debossed &quot;Polo&quot; tag at the outer side. Leather. Imported.</p>', NULL, 0),
	(17, 1, 'DILLIAN II SUEDE CHELSEA BOOT', '<p>Fits true to size. &frac12;&quot;/15 mm heel height. Rounded toe. Elasticized gores at the sides. Pull tab at the heel. Leather insole. Leather outsole with rubber treads. Leather. Imported.</p>', NULL, 0),
	(18, 1, 'BRUNEL LEATHER WORK BOOT', '<p>1&quot;/25 mm heel height. 6&quot;/152 mm shaft height. Lace-up front with speed hooks. Sewn-in tongue guard. Pull tab at the heel. Hand-cut leather insole. Vibram rubber outsole. Goodyear welt construction. Leather. Made in the USA.</p>', NULL, 0),
	(19, 1, 'LAXMAN TECH SUEDE SNEAKER', '<p>Rounded toe. Lace-up front. Woven tag with our signature pony at the tongue. Padded insole. Treaded rubber outsole. &quot;Polo&quot; printed at the outer sides. Includes our &quot;Polo Ralph Lauren&quot;&ndash;embossed box. Man-made materials. Imported.</p>', NULL, 0),
	(20, 1, 'ROUND SUNGLASSES', '<p>Lightweight lenses with 100% UV protection. Molded nose bridge. Curved arms. Our logo is discreetly etched at the left lens and interior right arm. Made in Italy.</p>', NULL, 0),
	(21, 1, 'NAUTICAL-STRIPED SUNGLASSES', '<p>Acetate frame. Metal bars accent the hinges of the arms. Striped pattern accents the interior of each arm. Tinted and polarized lenses offer 100% UV protection and have a scratch-resistant coating. Subtle &quot;Ralph Lauren&quot; text is etched at the left lens. Our metal &quot;RL&quot; monogram accents each temple. Presented in our signature soft leather case with a snapped closure. Our signature-stamped cleaning wipe is included. 55 mm eye size. 20 mm bridge size. 145 mm temple size. Imported.</p>', NULL, 0),
	(22, 1, 'POLO SQUARE SUNGLASSES', '<p>Metal frame with a double bridge. Metal signature pony at the temples. Lenses offer 100% UV protection. &quot;Polo&quot; etched at the left lens. Presented in our leather &quot;Polo Ralph Lauren&quot;&ndash;debossed case. 60 mm eye size. 17 mm bridge size. 140 mm temple size. Metal. Imported.</p>', NULL, 0),
	(23, 1, 'POLO AVIATOR SUNGLASSES', '<p>Metal frame with a double bridge. Metal signature pony at the nylon-fiber temples. Lenses offer 100% UV protection. &quot;Polo&quot; etched at the left lens. Presented in our leather &quot;Polo Ralph Lauren&quot;&ndash;debossed case. 61 mm eye size. 15 mm bridge size. 145 mm temple size. Metal, nylon. Imported.</p>', NULL, 0),
	(24, 1, 'STRAPLESS JERSEY MAXIDRESS', '<p>Strapless maxidress silhouette. Size medium has a 48&frac12;&quot; front body length and a 47&frac14;&quot; back body length. Pull-on styling. Lined at the bodice. Shell and Lining: 95% polyester, 5% elastane. Machine washable. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust, a 24&quot; waist, and 34&quot; hips. She wears a size small.</p>', NULL, 0),
	(25, 1, 'MADRAS FIT-AND-FLARE DRESS', '<p>Sleeveless fit-and-flare silhouette. US size 8 has a 40&frac12;&quot; back body length and a 37&frac14;&quot; bust. Rounded neckline. Concealed center back zipper with a hook-and-eye closure. Side on-seam pockets. Lined at the bodice. Shell and Lining: 100% cotton. Dry clean. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust, a 24&quot; waist, and 34&quot; hips. She wears a US size 2.</p>', NULL, 0),
	(26, 1, 'SILK CREPE SHIRTDRESS', '<p>Straight fit. Size 8 has a 51&frac12;&quot; back length, a 40&frac14;&quot; bust, and a 32&frac12;&quot; sleeve length. Point collar. Buttoned placket. Long sleeves with buttoned barrel cuffs. Two chest patch pockets. Comes with a self-belt with a rectangular buckle. 100% silk. Dry clean. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust, a 24&quot; waist, and 34&quot; hips. She wears a size 2.</p>', NULL, 0),
	(27, 1, 'BEADED JERSEY GOWN', '<p>Slim fit. US size 8 has a 58&quot; back length and a 35&quot; bust. Square neckline. Center back zipper with a hook-and-eye closure. Embellished shoulder straps. Sleeveless silhouette. Ruching gathers at the front left waist. Ruffle at the front left skirt. Fully lined. Shell and lining: 95% polyester, 5% elastane. Dry clean. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust, a 24&quot; waist, and 34&quot; hips. She wears a US size 2.</p>', NULL, 0),
	(28, 1, 'STRIPED COTTON CREWNECK TEE', '<p>Relaxed fit. Size medium has a 24&frac12;&quot; body length and a 38&quot; bust. Crewneck. Short sleeves. &quot;RL&quot; embroidery at the front right hem. 100% cotton. Machine washable. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust. She wears a size small.</p>', NULL, 0),
	(29, 1, 'COTTON-BLEND CREWNECK PULLOVER', '<p>Boxy fit. Intended to hit at the hip. Size medium has a 25&frac12;&quot; front body length, a 27&frac12;&quot; back body length, a 45&frac12;&quot; bust, a 49&quot; shoulder (taken from the dropped shoulder), and a 32&quot; sleeve length. Sleeve length changes &frac34;&quot; between sizes. Crewneck. Long balloon sleeves with ribbed cuffs. Dropped shoulders. Ribbed hem. &quot;Polo&quot; metal plaque at the front right hem. Hem falls longer at the back. Shell: 56% polyester, 44% cotton. Collar and cuffs: 58% cotton, 40% polyester, 2% elastane. Machine washable. Imported. Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust. She wears a size small.</p>', NULL, 0),
	(30, 1, 'CREWNECK LONG-SLEEVE TOP', '<ul>\r\n	<li>Slim fit. Intended to hit at the hip.</li>\r\n	<li>Size medium has a 26&quot; body length, a 32&quot; bust, a 28&quot; shoulder, a 29&frac12;&quot; waist, and a 24&quot; sleeve length. Sleeve length changes &frac12;&quot; between sizes.</li>\r\n	<li>Crewneck.</li>\r\n	<li>Long sleeves.</li>\r\n	<li>&quot;Polo&quot;-engraved metal plaque at the front right hem.</li>\r\n	<li>Front and back panels.</li>\r\n	<li>96% modal, 4% elastane.</li>\r\n	<li>Machine washable. Imported.</li>\r\n	<li>Model is 5&#39;10&quot;/178 cm and has a 32&quot; bust. She wears a size small.</li>\r\n</ul>', NULL, 0),
	(31, 1, 'TOMPKINS SKINNY CROP JEAN', '<ul>\r\n	<li>Tompkins Skinny Crop: mid-rise. Polo&#39;s signature skinny fit cropped to hit right above the ankle.</li>\r\n	<li>Size 28 has approx. a 7&frac14;&quot; rise, a 30&frac12;&quot; waist, a 36&quot; hip, and an 11&quot; leg opening. All sizes have a 26&quot; inseam.</li>\r\n	<li>Belt loops. Zip fly with a signature shank closure.</li>\r\n	<li>Five-pocket styling with signature &quot;P.R.L. 67&quot;&ndash;engraved metal rivets. &quot;Polo&quot; patch at the coin pocket.</li>\r\n	<li>Signature leather &quot;Polo Ralph Lauren&quot; patch at the back. Distressing throughout. Rip at the left knee.</li>\r\n	<li>93% cotton, 5% polyester, 2% elastane.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n	<li>Model is 5&#39;10&quot;/178 cm and has a 24&quot; waist and 34&quot; hips. She wears a US size 26.</li>\r\n</ul>', NULL, 0),
	(32, 1, 'TOMPKINS SKINNY CROP JEANS', '<ul>\r\n	<li>Tompkins Skinny Crop: mid-rise. Polo&#39;s signature Skinny Fit cropped to hit right above the ankle.</li>\r\n	<li>Size 28 has an approx. 7&frac14;&quot; rise, 30&frac12;&quot; waist, 36&quot; hip, and 11&quot; leg opening. All sizes have a 26&quot; inseam.</li>\r\n	<li>Belt loops. Zip fly with a signature shank closure.</li>\r\n	<li>Five-pocket styling with signature &quot;P.R.L. 67&quot;&ndash;engraved metal rivets. &quot;Polo&quot; patch at the coin pocket.</li>\r\n	<li>Signature leather &quot;Polo Ralph Lauren&quot; patch at the back.</li>\r\n	<li>92% cotton, 6% polyester, 2% elastane.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n	<li>Model is 5&#39;10&quot;/178 cm and has a 24&quot; waist and 34&quot; hips. She wears a US size 26.</li>\r\n</ul>', NULL, 0),
	(33, 1, 'TOMPKINS SKINNY JEAN', '<ul>\r\n	<li>Tompkins Skinny: mid-rise. Polo&#39;s signature skinny fit with a narrow leg and plenty of stretch for comfort.</li>\r\n	<li>Size 28 has approx. a 7&frac14;&quot; rise, a 30&quot; waist, a 35&frac12;&quot; hip, and an 11&quot; leg opening. All sizes have a 30&quot; inseam.</li>\r\n	<li>Belt loops. Zip fly with a signature shank closure.</li>\r\n	<li>Five-pocket styling with signature &quot;P.R.L. 67&quot;&ndash;engraved metal rivets. &quot;Polo&quot; patch at the coin pocket.</li>\r\n	<li>Signature leather &quot;Polo Ralph Lauren&quot; patch at the back.</li>\r\n	<li>92% cotton, 6% polyester, 2% elastane.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n	<li>Model is 5&#39;10&quot;/178 cm and has a 24&quot; waist and 34&quot; hips. She wears a size 26.</li>\r\n</ul>', NULL, 0),
	(34, 1, 'SMALL SULLIVAN SADDLE BAG', '<ul>\r\n	<li>Single top handle with a 4&quot; drop. Removable woven crossbody strap with a 23&quot; drop.</li>\r\n	<li>&quot;Polo Ralph Lauren&quot;&ndash;debossed fold-over flap. Concealed magnetic closure.</li>\r\n	<li>Leather strap with a stud closure at each side.</li>\r\n	<li>Lined with twill.</li>\r\n	<li>Exterior slip pocket at the back. Interior slip pocket.</li>\r\n	<li>8&quot; L x 9&quot; W x 4&quot; D.</li>\r\n	<li>Leather, cotton.</li>\r\n	<li>Italian leather. Imported.</li>\r\n	<li>Comes with a dust bag.</li>\r\n</ul>', NULL, 0),
	(35, 1, 'PATCHWORK CANVAS BIG PONY TOTE', '<ul>\r\n	<li>Two webbed cotton top handles. each with a 9&frac12;&quot; maximum drop.</li>\r\n	<li>Antiqued metal rivets at the front and the back.</li>\r\n	<li>Top zip closure with a leather pull tab.</li>\r\n	<li>Signature embroidered Big Pony at the front patch pocket.</li>\r\n	<li>Reinforced base.</li>\r\n	<li>Interior slip pocket.</li>\r\n	<li>11&frac14;&quot; H x 19&quot; L x 7&quot; D.</li>\r\n	<li>Shell and lining: cotton. Trim: leather.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(36, 1, 'COLOR-BLOCKED BIG PONY TOTE', '<ul>\r\n	<li>Two webbed top handles, each with a 9&frac12;&quot; drop.</li>\r\n	<li>Top zip closure with a leather pull tab.</li>\r\n	<li>Patch pocket with our signature embroidered Big Pony at the front.</li>\r\n	<li>Interior slip pocket. Interior leather logo patch.</li>\r\n	<li>Gusseted sides. Reinforced bottom.</li>\r\n	<li>Antiqued rivets at the handles.</li>\r\n	<li>12&frac34;&quot; H x 16&frac12;&quot; L x 1&frac34;&quot; D.</li>\r\n	<li>Cotton, leather.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(37, 1, 'LASER-CUT FLORAL LEATHER TOTE', '<ul>\r\n	<li>Two leather top handles, each with an 8&quot; drop.</li>\r\n	<li>Lobster-clasp closure at the opening.</li>\r\n	<li>&quot;Polo Ralph Lauren&quot;&ndash;embossed removable leather luggage tag and two ties at the top handles.</li>\r\n	<li>Bonded interior.</li>\r\n	<li>Includes a removable zip pouch.</li>\r\n	<li>Bag: 12&frac14;&quot; H x 17&quot; L x 6&quot; D. Pouch: 3&frac14;&quot; H x 6&frac14;&quot; L.</li>\r\n	<li>Leather.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(38, 1, 'BEAR-PRINT COTTON COVERALL', '', NULL, 0),
	(39, 1, 'STRIPED COTTON HENLEY COVERALL', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Henley neckline.</li>\r\n	<li>Three-button placket.</li>\r\n	<li>Long sleeves.</li>\r\n	<li>Ring snaps at the hem ensure easy on and off.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(40, 1, 'STRIPED COTTON HENLEY BODYSUIT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Henley neckline.</li>\r\n	<li>Three-button placket.</li>\r\n	<li>Short sleeves.</li>\r\n	<li>Ring snaps at the hem.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(41, 1, 'ABC BLOCK 4-PIECE GIFT BASKET', '<ul>\r\n	<li>Kimono top, pant, coverall, and printed side of blankie: 100% cotton.</li>\r\n	<li>Blankie&#39;s bear, bear fill, plush side, and plush trim: 100% polyester.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Kimono top: wrap silhouette secures with snaps and a tie; long sleeves.</li>\r\n	<li>Pant: elasticized waist; footed silhouette.</li>\r\n	<li>Coverall: crew neckline; snapped front; long sleeves; signature embroidered pony at the left chest; ring snaps at the hem; footed silhouette.</li>\r\n	<li>Blankie: plush bear head and arms at the center; 11&quot; W x 11&quot; L. Imported.</li>\r\n	<li>Elegantly presented in a cotton-lined wicker basket. 10&quot; H x 10&quot; L x 8&quot; D.</li>\r\n	<li>Comes with pre-cut cellophane and a matching ribbon for gift-giving.</li>\r\n</ul>', NULL, 0),
	(42, 1, 'COTTON POLO DRESS & BLOOMER', '<ul>\r\n	<li>Dress and bloomer: 100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Drop-waist silhouette.</li>\r\n	<li>Size 9 months has a 16&quot; body length.</li>\r\n	<li>Ribbed Polo collar. Three-button placket.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Short sleeves with ribbed armbands.</li>\r\n	<li>Ruffled hem. Comes with a matching bloomer.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(43, 1, 'STRIPED POLO DRESS & BLOOMER', '<ul>\r\n	<li>Dress and bloomer: 100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Sleeveless drop-waist silhouette.</li>\r\n	<li>Size 9 months has a 16&frac12;&quot; front body length.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Ribbed Polo collar. Three-button placket.</li>\r\n	<li>Shirred flounce with lace trim.</li>\r\n	<li>Comes with a matching bloomer.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(44, 1, 'RUFFLED FLORAL DRESS & BLOOMER', '<ul>\r\n	<li>Dress&#39; shell and bloomer: 100% viscose. Dress&#39;s lining: 100% polyester.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Sleeveless A-line silhouette.</li>\r\n	<li>Size 9 months has a 16&frac12;&quot; front body length.</li>\r\n	<li>Elasticized ruffled neckline.</li>\r\n	<li>Fully lined.</li>\r\n	<li>Comes with a matching bloomer.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(45, 1, 'STRIPED COTTON BLANKET', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Striped side reverses to a solid-hued side.</li>\r\n	<li>Signature embroidered pony at the lower right corner of the striped side.</li>\r\n	<li>Ribbed binding.</li>\r\n	<li>28&quot; L x 32&quot; W.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(46, 1, 'CREPE SWADDLING BLANKET SET', '<ul>\r\n	<li>Set of three blankets.</li>\r\n	<li>All items: 100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Signature embroidered pony at the bottom right corner.</li>\r\n	<li>47&quot; L x 47&quot; W.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(47, 1, 'PLUSH BLANKET', '<ul>\r\n	<li>100% polyester.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Tonal signature embroidered pony at one corner.</li>\r\n	<li>Grosgrain border.</li>\r\n	<li>28&quot; L x 28&quot; W.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(48, 1, 'RL Bowery Sateen Duvet Cover', '<p>A contrasting border and frame give this 624-thread-count sateen duvet cover a crisp tailored look.</p>\r\n\r\n<ul>\r\n	<li>624-thread-count cotton sateen.</li>\r\n	<li>3&quot; contrasting flange. &frac12;&quot; contrasting frame.</li>\r\n	<li>Concealed buttoned closures.</li>\r\n	<li>Full/queen: 96&quot; L x 92&quot; W.</li>\r\n	<li>King: 96&quot; L x 108&quot; W.</li>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(49, 1, 'Bedford Jacquard Duvet Cover', '<p>With an allover jacquard-woven pattern, this 400-thread-count duvet cover will add subtle texture to your bed and is perfect for mixing and matching.</p>\r\n\r\n<ul>\r\n	<li>400-thread-count cotton jacquard.</li>\r\n	<li>2&quot; flange.</li>\r\n	<li>Concealed buttoned closure.</li>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(50, 1, 'MONACO SATEEN DUVET COVER', '<ul>\r\n	<li>286-thread-count cotton sateen.</li>\r\n	<li>Concealed buttoned closures at the bottom.</li>\r\n	<li>Full/queen: 96&quot; L x 92&quot; W; 2&quot; flange.</li>\r\n	<li>King: 96&quot; L x 108&quot; W; 2&quot; flange.</li>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(51, 1, 'Pink Palmer Sham', '<p>Crafted from luxurious 464-thread-count cotton percale, our menswear-inspired Palmer sham is designed with pink contrast taping.</p>\r\n\r\n<ul>\r\n	<li>2&frac12;&quot; flange with &frac14;&quot; of contrast taping.</li>\r\n	<li>Back envelope closure.</li>\r\n	<li>100% cotton. Machine washable. Imported.</li>\r\n</ul>', NULL, 0),
	(52, 1, 'RL BEADED DYLAN PILLOW', '<ul>\r\n	<li>Vertical rib-knit design with beading at the front. Solid-colored back.</li>\r\n	<li>Concealed zipper at the bottom.</li>\r\n	<li>Comes with a pillow insert.</li>\r\n	<li>15&quot; L x 20&quot; W. Shell: 70% wool; 30% cashmere. Insert: 95% feathers; 5% down blend. Dry clean. Imported.</li>\r\n</ul>', NULL, 0),
	(53, 1, 'GREAT BASIN THROW PILLOW', '<ul>\r\n	<li>Allover embroidery at the face. Solid silk twill back.</li>\r\n	<li>Concealed zip closure at one side.</li>\r\n	<li>Comes with a pillow insert.</li>\r\n	<li>22&quot; L x 22&quot; W. Shell: 100% silk. Insert: 95% feathers; 5% down blend. Dry clean. Made in India.</li>\r\n</ul>', NULL, 0),
	(54, 1, 'CABLED CASHMERE TRAVEL SET', '<ul>\r\n	<li>Eye mask is lined with contrasting woven cotton and has two elastic straps for a secure fit.</li>\r\n	<li>Rectangular cable-knit throw blanket has ribbed edges.</li>\r\n	<li>Zippered cable-knit bag is lined with coordinating cotton fleece.</li>\r\n	<li>Bag: 15&quot; L x 12&quot; W. Blanket: 32&quot; L x 55&quot; W.</li>\r\n	<li>Cashmere and cotton. Imported.</li>\r\n</ul>', NULL, 0),
	(55, 1, 'Langdon Embroidered Bathrobe', '<p>This relaxed-fitting unisex bathrobe is tailored from plush Turkish cotton, a durable fiber that becomes softer and more absorbent after each washing. The spa-inspired style is embellished with contrasting embroidery along the shawl collar, placket and cuffs for a classic look. Add a monogram at the left chest for a personal touch.</p>\r\n\r\n<ul>\r\n	<li>Relaxed fit. Shawl collar.</li>\r\n	<li>Long sleeves with turn-back cuffs. Pocket at each hip.</li>\r\n	<li>Self-tie belt at the waist.</li>\r\n	<li>100% Turkish cotton. Machine washable. Imported.</li>\r\n	<li>Small: 47&quot; back body length; 44&quot; chest; 24&quot; sleeve length.</li>\r\n	<li>Medium: 48&quot; back body length; 46&quot; chest; 24&frac12;&quot; sleeve length.</li>\r\n	<li>Large: 49&quot; back body length; 49&quot; chest; 25&frac12;&quot; sleeve length.</li>\r\n	<li>Please note, personalized items are non-returnable. A $5 monogramming charge will be added to the retail price.</li>\r\n</ul>', NULL, 0),
	(56, 1, 'COTTON MESH POLO SHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size 4/4T has a 17&frac34;&quot; front body length and a 19&quot; back body length.</li>\r\n	<li>Striped ribbed Polo collar. Two-button placket.</li>\r\n	<li>Short sleeves with striped ribbed armbands.</li>\r\n	<li>Chenille-and-felt &quot;Polo RL Athl.&quot; patch at the left chest. Chain-stitched &quot;RL Athl. Division #5 New York&quot; embroidery at the right chest.</li>\r\n	<li>Chain-stitched &quot;New York 1967 Athletics&quot; embroidery at the center back.</li>\r\n	<li>Tennis tail. &quot;Polo Ralph Lauren MCMLXVII&quot; patch at the front right hem.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(57, 1, 'COTTON MESH POLO SHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size 4/4T has a 17&frac34;&quot; front body length and an 18&frac12;&quot; back body length.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Ribbed Polo collar.</li>\r\n	<li>Two-button placket.</li>\r\n	<li>Short sleeves with ribbed armbands.</li>\r\n	<li>Tennis tail.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(58, 1, 'BANNER COTTON MESH POLO', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 23&frac34;&quot; body length.</li>\r\n	<li>Signature embroidered Big Pony at the left chest.</li>\r\n	<li>Striped ribbed Polo collar. Two-button placket.</li>\r\n	<li>Short sleeves with striped ribbed armbands. Chenille &quot;67&quot; patch at the right sleeve.</li>\r\n	<li>&quot;Academy PRL Athl.&quot; crackle-print and a &quot;67&quot; shield patch at the center back.</li>\r\n	<li>Even vented hem.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(59, 1, 'COTTON MESH LONG-SLEEVE POLO', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 23&frac34;&quot; front body length, a 25&frac14;&quot; back body length, and a 28&frac12;&quot; sleeve length. Sleeve length is taken from the center back of the neck.</li>\r\n	<li>Signature embroidered Big Pony at the left chest.</li>\r\n	<li>Ribbed Polo collar. Two-button placket.</li>\r\n	<li>Long sleeves with ribbed cuffs. Twill &quot;3&quot; patch at the right sleeve.</li>\r\n	<li>Tennis tail.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(60, 1, 'PLAID COTTON TWILL WORKSHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 24&frac12;&quot; front body length, a 24&frac34;&quot; back body length, and a 28&frac12;&quot; sleeve length. Sleeve length is taken from the center back of the neck.</li>\r\n	<li>&quot;Polo&quot; label at the left pocket.</li>\r\n	<li>Point collar.</li>\r\n	<li>Buttoned placket.</li>\r\n	<li>Long sleeves with buttoned barrel cuffs.</li>\r\n	<li>Two chest buttoned pockets.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(61, 1, 'COTTON OXFORD SPORT SHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 24&frac12;&quot; front body length, a 24&frac34;&quot; back body length, and a 28&frac12;&quot; sleeve length. Back body length and sleeve length are taken from the center back of the neck.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Button-down point collar.</li>\r\n	<li>Buttoned placket.</li>\r\n	<li>Long sleeves with buttoned barrel cuffs.</li>\r\n	<li>Box-pleated back yoke.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(62, 1, 'COTTON MESH WORKSHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 23&frac12;&quot; front body length, a 25&quot; back body length, and a 28&frac12;&quot; sleeve length. Sleeve length is taken from the center back of the neck.</li>\r\n	<li>Point collar. Buttoned placket.</li>\r\n	<li>Long sleeves with buttoned barrel cuffs.</li>\r\n	<li>Two chest buttoned pockets.</li>\r\n	<li>Stenciled &quot;PRL67&quot; printed at the left chest.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(63, 1, 'STRIPED COTTON SHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 24&frac12;&quot; front body length and a 24&frac34;&quot; back body length.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Button-down point collar. Buttoned placket.</li>\r\n	<li>Short sleeves.</li>\r\n	<li>Box-pleated back yoke.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(64, 1, 'STRIPED COTTON SHIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 24&frac12;&quot; front body length and a 24&frac34;&quot; back body length.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Button-down point collar. Buttoned placket.</li>\r\n	<li>Short sleeves.</li>\r\n	<li>Box-pleated back yoke.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(65, 1, 'SLIM MOTT-WASH JEAN', '<ul>\r\n	<li>Belted waistband. Zip fly with our signature shank closure.</li>\r\n	<li>Five-pocket styling.</li>\r\n	<li>Fading down the leg with creased markings at the thigh and the knee.</li>\r\n	<li>100% cotton. Machine washable. Imported.</li>\r\n	<li>Belt is not included.</li>\r\n	<li>Size 12 has an average inseam of 27&frac12;&quot;.</li>\r\n</ul>', NULL, 0),
	(66, 1, 'ELDRIDGE STRETCH SKINNY JEAN', '<ul>\r\n	<li>99% cotton, 1% elastane.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Skinny Fit.</li>\r\n	<li>Size 12 has a 7&frac14;&quot; rise, a 26&frac12;&quot; inseam, and a 12&frac14;&quot; leg opening.</li>\r\n	<li>Belt loops. Zip fly with our signature shank closure.</li>\r\n	<li>Five-pocket styling with signature metal rivets.</li>\r\n	<li>&quot;Polo&quot; label at the right coin pocket.</li>\r\n	<li>&quot;Polo Ralph Lauren&quot; faux-suede patch at the back right waist.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(67, 1, 'POLO I WOOL TWILL SUIT', '<ul>\r\n	<li>Jacket features notched lapels, a two-button silhouette and long sleeves with a four-button detail at each cuff.</li>\r\n	<li>Jacket also features an angled welt pocket at the left chest, interior besom pockets at the left and right chest, flapped besom pockets at the waist and a vented back hem. Jacket is fully lined.</li>\r\n	<li>Trouser features belt loops, a zip fly with a hook-and-bar closure, a finished waistband with interior suspender buttons and a flat front.</li>\r\n	<li>Trouser also features side on-seam pockets, a button-and-loop besom pocket at the back left and a buttoned besom pocket at the back right. Unfinished hems ready for custom-tailoring.</li>\r\n	<li>Jacket and trouser: 100% wool. Jacket lining: 100% viscose. Dry clean. Made in Italy.</li>\r\n	<li>Update this classic style with a bold bow tie and cool sneakers.</li>\r\n</ul>', NULL, 0),
	(68, 1, 'BELTED STRETCH COTTON CHINO', '<ul>\r\n	<li>Pant: 98% cotton, 2% elastane. Belt: 100% viscose.</li>\r\n	<li>Pant: machine washable.</li>\r\n	<li>Pant: Size 12 has a 7&frac14;&quot; rise, a 27&frac12;&quot; inseam, and a 12&frac34;&quot; leg-opening circumference.</li>\r\n	<li>Belt: 1&frac14;&quot; wide.</li>\r\n	<li>Pant: belt loops; zip fly with a buttoned closure; side on-seam pockets; coin pocket; two back buttoned pockets; &quot;Polo&quot; label at the back right pocket.</li>\r\n	<li>Belt: silver-tone double-D-ring closure.</li>\r\n	<li>Imported.</li>\r\n	<li>Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery.</li>\r\n</ul>', NULL, 0),
	(69, 1, 'BATTEN CANVAS EZ BOAT SHOE', '<ul>\r\n	<li>Rounded toe.</li>\r\n	<li>Slip-on styling with a hook-and-loop strap closure.</li>\r\n	<li>Deck-shoe laces thread through metal grommets at the sides.</li>\r\n	<li>Rubber &quot;Polo&quot; tag at the outer side and the heel.</li>\r\n	<li>Padded insole.</li>\r\n	<li>Treaded rubber outsole.</li>\r\n	<li>Signature embroidered pony at the strap.</li>\r\n	<li>Cotton.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(70, 1, 'PROPELL II SNEAKER', '<ul>\r\n	<li>Rounded toe.</li>\r\n	<li>Hook-and-loop strap closure at the vamp.</li>\r\n	<li>&quot;Polo&quot; printed at the heel.</li>\r\n	<li>Padded insole.</li>\r\n	<li>Treaded rubber outsole.</li>\r\n	<li>Printed signature pony at the strap.</li>\r\n	<li>Man-made materials.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(71, 1, 'BANKS SANDAL', '<ul>\r\n	<li>Open toe. Hook-and-loop closure at the vamp.</li>\r\n	<li>Hook-and-loop strap closure at the heel.</li>\r\n	<li>Padded insole.</li>\r\n	<li>Treaded rubber outsole.</li>\r\n	<li>Screen-printed signature pony at the vamp.</li>\r\n	<li>Man-made materials.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(72, 1, 'MADRAS COTTON SHIRTDRESS', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Fit-and-flare silhouette.</li>\r\n	<li>Size 10 has a 30&quot; front body length and a 27&quot; sleeve length. Sleeve length is taken from the center back of the neck.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Point collar. Buttoned placket.</li>\r\n	<li>Long sleeves with buttoned barrel cuffs.</li>\r\n	<li>Comes with a self-belt with a double-D-ring closure. Shirred waist.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(73, 1, 'COTTON CHINO BELTED SHIRTDRESS', '<ul>\r\n	<li>Dress: 100% cotton. Belt: 100% polyester.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Fit-and-flare silhouette.</li>\r\n	<li>Size 10 has a 30&quot; front body length.</li>\r\n	<li>Point collar. Buttoned placket.</li>\r\n	<li>Short sleeves with buttoned cuffs.</li>\r\n	<li>Two chest buttoned pockets.</li>\r\n	<li>Shirred waist with a removable striped belt.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(74, 1, 'STRIPED OFF-THE-SHOULDER DRESS', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Off-the-shoulder silhouette.</li>\r\n	<li>Size 10 has a 26&frac14;&quot; front body length.</li>\r\n	<li>Smocked off-the-shoulder neckline with adjustable shoulder straps.</li>\r\n	<li>Short raglan sleeves.</li>\r\n	<li>Two side on-seam pockets at the hips.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(75, 1, 'BUTTON-FRONT DENIM SKIRT', '<ul>\r\n	<li>96% cotton, 3% polyester, 1% elastane.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Slight A-line silhouette.</li>\r\n	<li>Size 10 has an 11&quot; front body length and a 12&quot; back body length.</li>\r\n	<li>Belt loops. Buttoned front. Signature shank closures</li>\r\n	<li>Five-pocket styling.</li>\r\n	<li>Faux-suede &quot;Polo&quot; patch at the back right waist.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(76, 1, 'FLORAL SKIRT', '<ul>\r\n	<li>Shell: 100% viscose. Lining: 100% polyester.</li>\r\n	<li>Machine washable.</li>\r\n	<li>A-line silhouette.</li>\r\n	<li>Size medium has a 13&frac12;&quot; front body length and a 14&quot; back body length.</li>\r\n	<li>Concealed left-side-seam zipper.</li>\r\n	<li>Fully lined.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(77, 1, 'PLEATED MADRAS SKIRT', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>A-line silhouette.</li>\r\n	<li>Size 10 has a 12&frac34;&quot; length.</li>\r\n	<li>Two buckled self-straps at the left side. Buttoned and hook-and-bar closures at the interior waist.</li>\r\n	<li>Pleated sides and back.</li>\r\n	<li>Due to the natural characteristics of this material, the coloring may rub off onto fabrics and upholstery.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(78, 1, 'CABLE-KNIT CASHMERE SWEATER', '<ul>\r\n	<li>100% cashmere.</li>\r\n	<li>Hand wash.</li>\r\n	<li>Size medium has an 18&frac12;&quot; body length and a 20&quot; sleeve length.</li>\r\n	<li>Rib-knit crewneck.</li>\r\n	<li>Long sleeves with rib-knit cuffs.</li>\r\n	<li>Rib-knit hem.</li>\r\n	<li>Imported. Italian cashmere.</li>\r\n</ul>', NULL, 0),
	(79, 1, 'FAIR ISLE HOODED SWEATER', '<ul>\r\n	<li>Shell: 27% wool, 26% cotton, 23% viscose, 22% nylon, 2% cashmere. Lining: 100% cotton.</li>\r\n	<li>Hand wash.</li>\r\n	<li>Straight fit.</li>\r\n	<li>Size medium has a 19&frac14;&quot; body length and an 18&frac12;&quot; sleeve length. Sleeve length changes 2&quot; between sizes.</li>\r\n	<li>Hood. Three toggle-and-loop closures at the center front.</li>\r\n	<li>Long sleeves with ribbed cuffs.</li>\r\n	<li>Two front waist patch pockets.</li>\r\n	<li>Fully lined. Ribbed hem.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(80, 1, 'FLAG COMBED COTTON SWEATER', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Hand wash.</li>\r\n	<li>Size medium has an 18&quot; body length and a 19&frac12;&quot; sleeve length.</li>\r\n	<li>Rib-knit crewneck. Buttoned closure at the left shoulder.</li>\r\n	<li>Long sleeves with rib-knit cuffs.</li>\r\n	<li>Intarsia-knit American flag with hand-embroidered stars and &quot;RL&quot; embroidery at the center front.</li>\r\n	<li>Rib-knit hem.</li>\r\n	<li>Dyed with indigo, which may rub off onto fabrics, leather, and upholstery.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0),
	(81, 1, 'RUFFLED COTTON CARDIGAN', '<ul>\r\n	<li>100% cotton.</li>\r\n	<li>Machine washable.</li>\r\n	<li>Size medium has a 17&frac34;&quot; body length.</li>\r\n	<li>Signature embroidered pony at the left chest.</li>\r\n	<li>Rib-knit crewneck. Buttoned placket.</li>\r\n	<li>Puffed long sleeves with rib-knit cuffs.</li>\r\n	<li>Ruffled hem.</li>\r\n	<li>Imported.</li>\r\n</ul>', NULL, 0);
/*!40000 ALTER TABLE `products_description` ENABLE KEYS */;

-- Dumping structure for table shop.products_images
CREATE TABLE IF NOT EXISTS `products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `image` mediumtext COLLATE utf8_unicode_ci,
  `htmlcontent` mediumtext COLLATE utf8_unicode_ci,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_prodid` (`products_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_images: ~97 rows (approximately)
/*!40000 ALTER TABLE `products_images` DISABLE KEYS */;
INSERT INTO `products_images` (`id`, `products_id`, `image`, `htmlcontent`, `sort_order`) VALUES
	(1, 1, 'resources/assets/images/product_images/1502107398.pPOLO2-26314766_alternate1_v360x480.jpg', '', 1),
	(2, 1, 'resources/assets/images/product_images/1502107404.pPOLO2-26314766_alternate2_v360x480.jpg', '', 1),
	(3, 1, 'resources/assets/images/product_images/1502107411.pPOLO2-26314766_alternate3_v360x480.jpg', '', 1),
	(4, 4, 'resources/assets/images/product_images/1502174861.pPOLO2-26314826_alternate1_v360x480.jpg', '', 1),
	(5, 4, 'resources/assets/images/product_images/1502174868.pPOLO2-26314826_alternate2_v360x480.jpg', '', 1),
	(6, 4, 'resources/assets/images/product_images/1502174874.pPOLO2-26314826_alternate3_v360x480.jpg', '', 1),
	(7, 5, 'resources/assets/images/product_images/1502176795.pPOLO2-26316348_alternate1_v360x480.jpg', '', 1),
	(8, 5, 'resources/assets/images/product_images/1502176801.pPOLO2-26316348_alternate2_v360x480.jpg', '', 1),
	(9, 5, 'resources/assets/images/product_images/1502176808.pPOLO2-26316348_alternate3_v360x480.jpg', '', 1),
	(10, 6, 'resources/assets/images/product_images/1502177811.pPOLO2-26314634_alternate1_v360x480.jpg', '', 1),
	(11, 6, 'resources/assets/images/product_images/1502177816.pPOLO2-26314634_alternate2_v360x480.jpg', '', 1),
	(12, 6, 'resources/assets/images/product_images/1502177823.pPOLO2-26314634_alternate3_v360x480.jpg', '', 1),
	(13, 7, 'resources/assets/images/product_images/1502181296.pPOLO2-26008917_alternate1_v360x480.jpg', '', 1),
	(14, 7, 'resources/assets/images/product_images/1502181302.pPOLO2-26008917_alternate2_v360x480.jpg', '', 1),
	(15, 7, 'resources/assets/images/product_images/1502181310.pPOLO2-26008917_alternate3_v360x480.jpg', '', 1),
	(16, 8, 'resources/assets/images/product_images/1502182266.pPOLO2-26008953_alternate1_v360x480.jpg', '', 1),
	(17, 8, 'resources/assets/images/product_images/1502182272.pPOLO2-26008953_alternate2_v360x480.jpg', '', 1),
	(18, 8, 'resources/assets/images/product_images/1502182279.pPOLO2-26008953_alternate3_v360x480.jpg', '', 1),
	(19, 9, 'resources/assets/images/product_images/1502186424.pPOLO2-26008935_alternate1_v360x480.jpg', '', 1),
	(21, 9, 'resources/assets/images/product_images/1502186446.pPOLO2-26008935_alternate2_v360x480.jpg', '', 1),
	(23, 9, 'resources/assets/images/product_images/1502186468.pPOLO2-26008935_alternate3_v360x480.jpg', '', 1),
	(27, 10, 'resources/assets/images/product_images/1502187573.pPOLO2-26315018_alternate1_v360x480.jpg', '', 1),
	(28, 10, 'resources/assets/images/product_images/1502187583.pPOLO2-26315018_alternate2_v360x480.jpg', '', 1),
	(29, 10, 'resources/assets/images/product_images/1502187596.pPOLO2-26315018_alternate3_v360x480.jpg', '', 1),
	(30, 11, 'resources/assets/images/product_images/1502188797.pPOLO2-26317497_alternate1_v360x480.jpg', '', 1),
	(31, 11, 'resources/assets/images/product_images/1502188805.pPOLO2-26317497_alternate2_v360x480.jpg', '', 1),
	(32, 11, 'resources/assets/images/product_images/1502188814.pPOLO2-26317497_alternate3_v360x480.jpg', '', 1),
	(33, 12, 'resources/assets/images/product_images/1502189793.pPOLO2-26316198_alternate1_v360x480.jpg', '', 1),
	(34, 12, 'resources/assets/images/product_images/1502189798.pPOLO2-26316198_alternate2_v360x480.jpg', '', 1),
	(35, 12, 'resources/assets/images/product_images/1502189805.pPOLO2-26316198_alternate3_v360x480.jpg', '', 1),
	(36, 13, 'resources/assets/images/product_images/1502190279.pPOLO2-26315541_alternate1_v360x480.jpg', '', 1),
	(37, 13, 'resources/assets/images/product_images/1502190286.pPOLO2-26315541_alternate2_v360x480.jpg', '', 1),
	(38, 13, 'resources/assets/images/product_images/1502190295.pPOLO2-26315541_alternate3_v360x480.jpg', '', 1),
	(39, 14, 'resources/assets/images/product_images/1502190650.pPOLO2-26404754_standard_v400.jpg', '', 1),
	(40, 14, 'resources/assets/images/product_images/1502190660.pPOLO2-26404754_alternate2_v360x480.jpg', '', 1),
	(41, 14, 'resources/assets/images/product_images/1502190665.pPOLO2-26404754_alternate3_v360x480.jpg', '', 1),
	(42, 15, 'resources/assets/images/product_images/1502191226.pPOLO2-26256326_alternate1_v360x480.jpg', '', 1),
	(43, 15, 'resources/assets/images/product_images/1502191230.pPOLO2-26256326_alternate2_v360x480.jpg', '', 1),
	(44, 15, 'resources/assets/images/product_images/1502191234.pPOLO2-26256326_alternate3_v360x480.jpg', '', 1),
	(45, 16, 'resources/assets/images/product_images/1502191401.pPOLO2-21857429_alternate2_v360x480.jpg', '', 1),
	(46, 16, 'resources/assets/images/product_images/1502191406.pPOLO2-21857429_alternate3_v360x480.jpg', '', 1),
	(47, 17, 'resources/assets/images/product_images/1502191619.pPOLO2-26256404_alternate1_v360x480.jpg', '', 1),
	(48, 17, 'resources/assets/images/product_images/1502191626.pPOLO2-26256404_alternate2_v360x480.jpg', '', 1),
	(49, 17, 'resources/assets/images/product_images/1502191633.pPOLO2-26256404_alternate3_v360x480.jpg', '', 1),
	(50, 18, 'resources/assets/images/product_images/1502191918.pPOLO2-24354359_alternate1_v360x480.jpg', '', 1),
	(51, 18, 'resources/assets/images/product_images/1502191923.pPOLO2-24354359_alternate2_v360x480.jpg', '', 1),
	(52, 18, 'resources/assets/images/product_images/1502191929.pPOLO2-24354359_alternate3_v360x480.jpg', '', 1),
	(53, 19, 'resources/assets/images/product_images/1502192102.pPOLO2-25784541_alternate1_v360x480.jpg', '', 1),
	(54, 19, 'resources/assets/images/product_images/1502192107.pPOLO2-25784541_alternate2_v360x480.jpg', '', 1),
	(55, 19, 'resources/assets/images/product_images/1502192112.pPOLO2-25784541_alternate3_v360x480.jpg', '', 1),
	(56, 20, 'resources/assets/images/product_images/1502192380.pPOLO2-12181663_alternate1_v360x480.jpg', '', 1),
	(57, 21, 'resources/assets/images/product_images/1502193428.pPOLO2-19116009_alternate5_v360x480.jpg', '', 1),
	(58, 22, 'resources/assets/images/product_images/1502193592.pPOLO2-24933842_alternate1_v360x480.jpg', '', 1),
	(59, 22, 'resources/assets/images/product_images/1502193608.pPOLO2-24933842_alternate2_v360x480.jpg', '', 1),
	(60, 23, 'resources/assets/images/product_images/1502193742.pPOLO2-24128696_alternate1_v360x480.jpg', '', 1),
	(61, 23, 'resources/assets/images/product_images/1502193755.pPOLO2-24128696_alternate2_v360x480.jpg', '', 1),
	(62, 24, 'resources/assets/images/product_images/1502194983.pPOLO2-25759503_standard_v360x480.jpg', '', 1),
	(63, 24, 'resources/assets/images/product_images/1502194989.pPOLO2-25759503_alternate3_v360x480.jpg', '', 1),
	(65, 25, 'resources/assets/images/product_images/1502195162.pPOLO2-25759495_alternate8_v360x480.jpg', '', 1),
	(66, 25, 'resources/assets/images/product_images/1502195167.pPOLO2-25759495_alternate3_v360x480.jpg', '', 1),
	(67, 26, 'resources/assets/images/product_images/1502195500.pPOLO2-26059809_standard_v360x480.jpg', '', 1),
	(68, 26, 'resources/assets/images/product_images/1502195504.pPOLO2-26059809_alternate3_v360x480.jpg', '', 1),
	(69, 27, 'resources/assets/images/product_images/1502195693.pPOLO2-25854363_standard_v360x480.jpg', '', 1),
	(70, 27, 'resources/assets/images/product_images/1502195698.pPOLO2-25854363_alternate3_v360x480.jpg', '', 1),
	(71, 28, 'resources/assets/images/product_images/1502196955.pPOLO2-25759710_alternate2_v360x480.jpg', '', 1),
	(72, 28, 'resources/assets/images/product_images/1502196961.pPOLO2-25759710_alternate1_v360x480.jpg', '', 1),
	(73, 29, 'resources/assets/images/product_images/1502198243.pPOLO2-25759868_alternate1_v360x480.jpg', '', 1),
	(74, 29, 'resources/assets/images/product_images/1502198249.pPOLO2-25759868_alternate2_v360x480.jpg', '', 1),
	(75, 30, 'resources/assets/images/product_images/1502198485.pPOLO2-26060127_alternate1_v360x480.jpg', '', 1),
	(76, 30, 'resources/assets/images/product_images/1502198490.pPOLO2-26060127_alternate2_v360x480.jpg', '', 1),
	(77, 31, 'resources/assets/images/product_images/1502199708.pPOLO2-26451235_alternate1_v360x480.jpg', '', 1),
	(78, 31, 'resources/assets/images/product_images/1502199722.pPOLO2-26451235_alternate2_v360x480.jpg', '', 1),
	(79, 32, 'resources/assets/images/product_images/1502200897.pPOLO2-26328182_alternate1_v360x480.jpg', '', 1),
	(80, 32, 'resources/assets/images/product_images/1502200911.pPOLO2-26328182_alternate2_v360x480.jpg', '', 1),
	(81, 33, 'resources/assets/images/product_images/1502201134.pPOLO2-26328155_alternate1_v360x480.jpg', '', 1),
	(82, 33, 'resources/assets/images/product_images/1502201141.pPOLO2-26328155_alternate2_v360x480.jpg', '', 1),
	(83, 34, 'resources/assets/images/product_images/1502261155.pPOLO2-25480910_alternate1_v360x480.jpg', '', 1),
	(84, 34, 'resources/assets/images/product_images/1502261161.pPOLO2-25480910_standard_v400.jpg', '', 1),
	(85, 35, 'resources/assets/images/product_images/1502261680.pPOLO2-26161986_alternate1_v360x480.jpg', '', 1),
	(86, 35, 'resources/assets/images/product_images/1502261688.pPOLO2-26161986_alternate2_v360x480.jpg', '', 1),
	(87, 36, 'resources/assets/images/product_images/1502262213.pPOLO2-26161985_alternate1_v360x480.jpg', '', 1),
	(88, 36, 'resources/assets/images/product_images/1502262221.pPOLO2-26161985_alternate2_v360x480.jpg', '', 1),
	(89, 37, 'resources/assets/images/product_images/1502262505.pPOLO2-25480914_alternate1_v360x480.jpg', '', 1),
	(90, 37, 'resources/assets/images/product_images/1502262510.pPOLO2-25480914_alternate2_v360x480.jpg', '', 1),
	(91, 41, 'resources/assets/images/product_images/1502265623.pPOLO2-22839467_alternate1_v360x480.jpg', '', 1),
	(92, 56, 'resources/assets/images/product_images/1502348047.pPOLO2-25995642_alternate1_v360x480.jpg', '', 1),
	(93, 58, 'resources/assets/images/product_images/1502349087.pPOLO2-25961612_alternate1_v360x480.jpg', '', 1),
	(94, 61, 'resources/assets/images/product_images/1502351686.pPOLO2-25961083_alternate1_v360x480.jpg', '', 1),
	(95, 70, 'resources/assets/images/product_images/1502363144.pPOLO2-25464682_alternate1_v360x480.jpg', '', 1),
	(96, 70, 'resources/assets/images/product_images/1502363160.pPOLO2-25464682_alternate2_v360x480.jpg', '', 1),
	(97, 72, 'resources/assets/images/product_images/1502364167.pPOLO2-26091141_alternate1_v360x480.jpg', '', 1),
	(98, 77, 'resources/assets/images/product_images/1502366133.pPOLO2-26091049_alternate1_v360x480.jpg', '', 1),
	(99, 78, 'resources/assets/images/product_images/1502366354.pPOLO2-26090785_alternate1_v360x480.jpg', '', 1),
	(100, 79, 'resources/assets/images/product_images/1502366470.pPOLO2-26090829_alternate1_v360x480.jpg', '', 1),
	(101, 82, 'resources/assets/images/product_images/1515574821.https___rokitapp.com_version_2_mobileapp_app_output_1515409406483.jpeg', '', 1),
	(102, 82, 'resources/assets/images/product_images/1515574839.861731617_Pakistan_flag.jpg', '', 1),
	(103, 80, 'resources/assets/images/product_images/1536313070.01.jpg', 'Destri', 1);
/*!40000 ALTER TABLE `products_images` ENABLE KEYS */;

-- Dumping structure for table shop.products_notifications
CREATE TABLE IF NOT EXISTS `products_notifications` (
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`products_id`,`customers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `products_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_notifications` ENABLE KEYS */;

-- Dumping structure for table shop.products_options
CREATE TABLE IF NOT EXISTS `products_options` (
  `products_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `categories_id` int(100) NOT NULL,
  `session_regenerate_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_options_id`,`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_options: ~8 rows (approximately)
/*!40000 ALTER TABLE `products_options` DISABLE KEYS */;
INSERT INTO `products_options` (`products_options_id`, `language_id`, `products_options_name`, `categories_id`, `session_regenerate_id`) VALUES
	(1, 1, 'Colors', 0, '1502106343'),
	(4, 1, 'Size', 0, '1502106711'),
	(7, 1, 'Waist', 0, '1502187895'),
	(10, 1, 'Length', 0, '1502187939'),
	(13, 1, 'Option 1', 0, '1531844564'),
	(14, 1, 'Option 2', 0, '1531844574'),
	(15, 1, 'Option 3', 0, '1531844582'),
	(16, 1, 'Option 4', 0, '1531844588');
/*!40000 ALTER TABLE `products_options` ENABLE KEYS */;

-- Dumping structure for table shop.products_options_values
CREATE TABLE IF NOT EXISTS `products_options_values` (
  `products_options_values_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_values_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`products_options_values_id`,`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_options_values: ~54 rows (approximately)
/*!40000 ALTER TABLE `products_options_values` DISABLE KEYS */;
INSERT INTO `products_options_values` (`products_options_values_id`, `language_id`, `products_options_values_name`) VALUES
	(1, 1, 'Brown'),
	(2, 1, 'Cream'),
	(3, 1, 'Blue'),
	(4, 1, 'Multi'),
	(5, 1, 'Black'),
	(6, 1, 'Grey'),
	(7, 1, 'White'),
	(8, 1, 'Purple'),
	(9, 1, 'Navy'),
	(29, 1, 'Small'),
	(32, 1, 'Medium'),
	(35, 1, 'Large'),
	(38, 1, 'Extra Large'),
	(41, 1, '28W'),
	(42, 1, '30W'),
	(43, 1, '32W'),
	(44, 1, '34W'),
	(45, 1, '36W'),
	(46, 1, '38W'),
	(47, 1, '40W'),
	(48, 1, '42W'),
	(65, 1, '30L'),
	(66, 1, '32L'),
	(67, 1, '34L'),
	(74, 1, '7D'),
	(75, 1, '8D'),
	(76, 1, '8.5D'),
	(77, 1, '9D'),
	(87, 1, '24'),
	(88, 1, '25'),
	(89, 1, '26'),
	(90, 1, '27'),
	(91, 1, '28'),
	(92, 1, '29'),
	(93, 1, '30'),
	(107, 1, 'New Born'),
	(110, 1, '3 Mos'),
	(111, 1, '6 Mos'),
	(112, 1, '9 Mos'),
	(119, 1, 'Hollywood Cream'),
	(122, 1, 'Vintage Silver'),
	(125, 1, 'King'),
	(130, 1, 'Full'),
	(131, 1, '15"x20"'),
	(132, 1, '22"x22"'),
	(137, 1, '3T'),
	(138, 1, '4T'),
	(139, 1, '5T'),
	(146, 1, ''),
	(147, 1, '1'),
	(148, 1, '1'),
	(149, 1, '2'),
	(154, 1, 'Good'),
	(155, 1, '၀ိတ္');
/*!40000 ALTER TABLE `products_options_values` ENABLE KEYS */;

-- Dumping structure for table shop.products_options_values_to_products_options
CREATE TABLE IF NOT EXISTS `products_options_values_to_products_options` (
  `products_options_values_to_products_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_options_id` int(11) NOT NULL,
  `products_options_values_id` int(11) NOT NULL,
  PRIMARY KEY (`products_options_values_to_products_options_id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_options_values_to_products_options: ~150 rows (approximately)
/*!40000 ALTER TABLE `products_options_values_to_products_options` DISABLE KEYS */;
INSERT INTO `products_options_values_to_products_options` (`products_options_values_to_products_options_id`, `products_options_id`, `products_options_values_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 1, 4),
	(5, 1, 5),
	(6, 1, 6),
	(7, 1, 7),
	(8, 1, 8),
	(9, 1, 9),
	(10, 3, 10),
	(11, 3, 11),
	(12, 3, 12),
	(13, 3, 13),
	(14, 3, 14),
	(15, 3, 15),
	(16, 3, 16),
	(17, 3, 17),
	(18, 3, 18),
	(20, 2, 20),
	(21, 2, 21),
	(22, 2, 22),
	(23, 2, 23),
	(24, 2, 24),
	(25, 2, 25),
	(26, 2, 26),
	(27, 2, 27),
	(28, 2, 28),
	(29, 4, 29),
	(30, 5, 30),
	(31, 6, 31),
	(32, 4, 32),
	(33, 6, 33),
	(34, 5, 34),
	(35, 4, 35),
	(36, 5, 36),
	(37, 6, 37),
	(38, 4, 38),
	(39, 6, 39),
	(40, 5, 40),
	(41, 7, 41),
	(42, 7, 42),
	(43, 7, 43),
	(44, 7, 44),
	(45, 7, 45),
	(46, 7, 46),
	(47, 7, 47),
	(48, 7, 48),
	(49, 8, 49),
	(50, 8, 50),
	(51, 8, 51),
	(52, 8, 52),
	(53, 8, 53),
	(54, 8, 54),
	(55, 8, 55),
	(56, 8, 56),
	(57, 9, 57),
	(58, 9, 58),
	(59, 9, 59),
	(60, 9, 60),
	(61, 9, 61),
	(62, 9, 62),
	(63, 9, 63),
	(64, 9, 64),
	(65, 10, 65),
	(66, 10, 66),
	(67, 10, 67),
	(68, 11, 68),
	(69, 11, 69),
	(70, 11, 70),
	(71, 12, 71),
	(72, 12, 72),
	(73, 12, 73),
	(74, 4, 74),
	(75, 4, 75),
	(76, 4, 76),
	(77, 4, 77),
	(78, 5, 78),
	(79, 5, 79),
	(80, 5, 80),
	(81, 5, 81),
	(82, 6, 82),
	(83, 6, 83),
	(84, 6, 84),
	(85, 6, 85),
	(86, 5, 86),
	(87, 4, 87),
	(88, 4, 88),
	(89, 4, 89),
	(90, 4, 90),
	(91, 4, 91),
	(92, 4, 92),
	(93, 4, 93),
	(94, 5, 94),
	(95, 5, 95),
	(96, 5, 96),
	(97, 5, 97),
	(98, 5, 98),
	(99, 5, 99),
	(100, 6, 100),
	(101, 6, 101),
	(102, 6, 102),
	(103, 6, 103),
	(104, 6, 104),
	(105, 6, 105),
	(106, 6, 106),
	(107, 4, 107),
	(108, 6, 108),
	(109, 5, 109),
	(110, 4, 110),
	(111, 4, 111),
	(112, 4, 112),
	(113, 5, 113),
	(114, 5, 114),
	(115, 5, 115),
	(116, 6, 116),
	(117, 6, 117),
	(118, 6, 118),
	(119, 1, 119),
	(120, 3, 120),
	(121, 2, 121),
	(122, 1, 122),
	(123, 2, 123),
	(124, 3, 124),
	(125, 4, 125),
	(126, 6, 126),
	(127, 5, 127),
	(128, 6, 128),
	(129, 5, 129),
	(130, 4, 130),
	(131, 4, 131),
	(132, 4, 132),
	(133, 5, 133),
	(134, 6, 134),
	(135, 5, 135),
	(136, 6, 136),
	(137, 4, 137),
	(138, 4, 138),
	(139, 4, 139),
	(140, 5, 140),
	(141, 5, 141),
	(142, 5, 142),
	(143, 6, 143),
	(144, 6, 144),
	(145, 6, 145),
	(146, 13, 146),
	(147, 13, 147),
	(148, 13, 148),
	(149, 13, 149),
	(154, 14, 154),
	(155, 1, 155);
/*!40000 ALTER TABLE `products_options_values_to_products_options` ENABLE KEYS */;

-- Dumping structure for table shop.products_to_categories
CREATE TABLE IF NOT EXISTS `products_to_categories` (
  `products_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  PRIMARY KEY (`products_id`,`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.products_to_categories: ~160 rows (approximately)
/*!40000 ALTER TABLE `products_to_categories` DISABLE KEYS */;
INSERT INTO `products_to_categories` (`products_id`, `categories_id`) VALUES
	(1, 1),
	(1, 7),
	(2, 1),
	(2, 7),
	(4, 1),
	(4, 7),
	(5, 1),
	(5, 7),
	(6, 1),
	(6, 7),
	(7, 1),
	(7, 8),
	(8, 1),
	(8, 8),
	(9, 1),
	(9, 8),
	(10, 1),
	(10, 8),
	(11, 1),
	(11, 9),
	(12, 1),
	(12, 9),
	(13, 1),
	(13, 9),
	(14, 1),
	(14, 10),
	(15, 1),
	(15, 10),
	(16, 1),
	(16, 10),
	(17, 1),
	(17, 10),
	(18, 1),
	(18, 10),
	(19, 1),
	(19, 10),
	(20, 1),
	(20, 11),
	(21, 1),
	(21, 11),
	(22, 1),
	(22, 11),
	(23, 1),
	(23, 11),
	(24, 2),
	(24, 12),
	(25, 2),
	(25, 12),
	(26, 2),
	(26, 12),
	(27, 2),
	(27, 12),
	(28, 2),
	(28, 13),
	(29, 2),
	(29, 13),
	(30, 2),
	(30, 13),
	(31, 2),
	(31, 14),
	(32, 2),
	(32, 14),
	(33, 2),
	(33, 14),
	(34, 2),
	(34, 15),
	(35, 2),
	(35, 15),
	(36, 2),
	(36, 15),
	(37, 2),
	(37, 15),
	(38, 5),
	(38, 16),
	(39, 5),
	(39, 16),
	(40, 5),
	(40, 16),
	(41, 5),
	(41, 16),
	(42, 5),
	(42, 17),
	(43, 5),
	(43, 17),
	(44, 5),
	(44, 17),
	(45, 5),
	(45, 18),
	(46, 5),
	(46, 18),
	(47, 5),
	(47, 18),
	(48, 6),
	(48, 19),
	(49, 6),
	(49, 19),
	(50, 6),
	(50, 19),
	(51, 6),
	(51, 19),
	(52, 6),
	(52, 20),
	(53, 6),
	(53, 20),
	(54, 6),
	(54, 20),
	(55, 6),
	(55, 21),
	(56, 3),
	(56, 22),
	(57, 3),
	(57, 22),
	(58, 3),
	(58, 22),
	(59, 3),
	(59, 22),
	(60, 3),
	(60, 23),
	(61, 3),
	(61, 23),
	(62, 3),
	(62, 23),
	(63, 3),
	(63, 23),
	(64, 3),
	(64, 23),
	(65, 3),
	(65, 24),
	(66, 3),
	(66, 24),
	(67, 3),
	(67, 24),
	(68, 3),
	(68, 24),
	(69, 3),
	(69, 25),
	(70, 3),
	(70, 25),
	(71, 3),
	(71, 25),
	(72, 4),
	(72, 26),
	(73, 4),
	(73, 26),
	(74, 4),
	(74, 26),
	(75, 4),
	(75, 27),
	(76, 4),
	(76, 27),
	(77, 4),
	(77, 27),
	(78, 4),
	(78, 28),
	(79, 4),
	(79, 28),
	(80, 4),
	(80, 28),
	(81, 4),
	(81, 28);
/*!40000 ALTER TABLE `products_to_categories` ENABLE KEYS */;

-- Dumping structure for table shop.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_status` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reviews_id`),
  KEY `idx_reviews_products_id` (`products_id`),
  KEY `idx_reviews_customers_id` (`customers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.reviews: ~0 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Dumping structure for table shop.reviews_description
CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `reviews_text` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.reviews_description: ~0 rows (approximately)
/*!40000 ALTER TABLE `reviews_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews_description` ENABLE KEYS */;

-- Dumping structure for table shop.sec_directory_whitelist
CREATE TABLE IF NOT EXISTS `sec_directory_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.sec_directory_whitelist: ~0 rows (approximately)
/*!40000 ALTER TABLE `sec_directory_whitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `sec_directory_whitelist` ENABLE KEYS */;

-- Dumping structure for table shop.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` int(11) unsigned NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sesskey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table shop.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.settings: 78 rows
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'facebook_app_id', 'Facebook App ID', '2018-04-27 01:30:00', '2018-09-07 19:00:40'),
	(2, 'facebook_secret_id', 'Facebook Secret ID', '2018-04-27 01:30:00', '2018-09-07 19:00:40'),
	(3, 'facebook_login', '1', '2018-04-27 01:30:00', '2018-09-07 19:00:40'),
	(4, 'contact_us_email', 'ihome@gmail.com', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(5, 'address', 'address', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(6, 'city', 'City', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(7, 'state', 'State', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(8, 'zip', 'Zip', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(9, 'country', 'Country', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(10, 'latitude', 'Latitude', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(11, 'longitude', 'Longitude', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(12, 'phone_no', '+92 312 1234567', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(13, 'fcm_android', '', '2018-04-27 01:30:00', '2018-07-17 10:37:25'),
	(14, 'fcm_ios', NULL, '2018-04-27 01:30:00', NULL),
	(15, 'fcm_desktop', NULL, '2018-04-27 01:30:00', NULL),
	(16, 'website_logo', 'resources/assets/images/site_images/1536332961.logo_79.png', '2018-04-27 01:30:00', '2018-09-07 13:39:21'),
	(17, 'fcm_android_sender_id', NULL, '2018-04-27 01:30:00', NULL),
	(18, 'fcm_ios_sender_id', '', '2018-04-27 01:30:00', '2018-07-17 10:37:25'),
	(19, 'app_name', 'Ecommerce', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(20, 'currency_symbol', '$', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(21, 'new_product_duration', '20', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(22, 'notification_title', 'Ionic Ecommerce', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(23, 'notification_text', 'A bundle of products waiting for you!', '2018-04-27 01:30:00', NULL),
	(24, 'lazzy_loading_effect', 'Detail', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(25, 'footer_button', '0', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(26, 'cart_button', '0', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(27, 'featured_category', NULL, '2018-04-27 01:30:00', NULL),
	(28, 'notification_duration', 'year', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(29, 'home_style', '2', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(30, 'wish_list_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(31, 'edit_profile_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(32, 'shipping_address_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(33, 'my_orders_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(34, 'contact_us_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(35, 'about_us_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(36, 'news_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(37, 'intro_page', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(38, 'setting_page', '1', '2018-04-27 01:30:00', NULL),
	(39, 'share_app', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(40, 'rate_app', '1', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(41, 'site_url', 'https://demo.uallshop.com', '2018-04-27 01:30:00', '2018-09-07 15:15:09'),
	(42, 'admob', '0', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(43, 'admob_id', 'ID', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(44, 'ad_unit_id_banner', 'Unit ID', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(45, 'ad_unit_id_interstitial', 'Indestrial', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(46, 'category_style', '4', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(47, 'package_name', 'package name', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(48, 'google_analytic_id', 'test', '2018-04-27 01:30:00', '2018-07-17 10:28:41'),
	(49, 'themes', 'themeone', '2018-04-27 01:30:00', NULL),
	(50, 'company_name', 'VC', '2018-04-27 01:30:00', NULL),
	(51, 'facebook_url', '#', '2018-04-27 01:30:00', '2018-09-07 13:40:00'),
	(52, 'google_url', '#', '2018-04-27 01:30:00', '2018-09-07 13:40:00'),
	(53, 'twitter_url', '#', '2018-04-27 01:30:00', '2018-09-07 13:40:00'),
	(54, 'linked_in', '#', '2018-04-27 01:30:00', '2018-09-07 13:40:00'),
	(55, 'default_notification', 'onesignal', '2018-04-27 01:30:00', '2018-07-17 10:37:25'),
	(56, 'onesignal_app_id', '6053d948-b8f6-472a-87e4-379fa89f78d8', '2018-04-27 01:30:00', '2018-07-17 10:37:25'),
	(57, 'onesignal_sender_id', '50877237723', '2018-04-27 01:30:00', '2018-07-17 10:37:25'),
	(58, 'ios_admob', '1', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(59, 'ios_admob_id', 'AdMob ID', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(60, 'ios_ad_unit_id_banner', 'Unit ID Banner', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(61, 'ios_ad_unit_id_interstitial', 'ID Interstitial', '2018-04-27 01:30:00', '2018-07-17 10:22:14'),
	(62, 'google_login', '1', NULL, '2018-09-07 19:01:11'),
	(63, 'google_app_id', NULL, NULL, NULL),
	(64, 'google_secret_id', NULL, NULL, NULL),
	(65, 'google_callback_url', NULL, NULL, NULL),
	(66, 'facebook_callback_url', NULL, NULL, NULL),
	(67, 'is_app_purchased', '0', NULL, '2018-05-04 04:54:44'),
	(68, 'is_desktop_purchased', '1', NULL, '2018-05-04 04:54:44'),
	(69, 'consumer_key', '6df56cf915318431043dd7a75d', NULL, '2018-07-17 10:28:24'),
	(70, 'consumer_secret', '95032b42153184310488f5fb8f', NULL, '2018-07-17 10:28:24'),
	(71, 'order_email', 'orders@gmail.com', NULL, '2018-09-07 15:15:09'),
	(72, 'website_themes', '1', NULL, NULL),
	(73, 'seo_title', '', NULL, '2018-07-18 17:55:28'),
	(74, 'seo_metatag', '', NULL, '2018-07-18 17:55:28'),
	(75, 'seo_keyword', '', NULL, '2018-07-18 17:55:28'),
	(76, 'seo_description', '', NULL, '2018-07-18 17:55:28'),
	(77, 'before_head_tag', '', NULL, '2018-07-18 17:58:40'),
	(78, 'end_body_tag', '', NULL, '2018-07-18 17:58:40');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table shop.shipping_description
CREATE TABLE IF NOT EXISTS `shipping_description` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `table_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sub_labels` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table shop.shipping_description: 4 rows
/*!40000 ALTER TABLE `shipping_description` DISABLE KEYS */;
INSERT INTO `shipping_description` (`id`, `name`, `language_id`, `table_name`, `sub_labels`) VALUES
	(1, 'Free Shipping', 1, 'free_shipping', ''),
	(4, 'Local Pickup', 1, 'local_pickup', ''),
	(7, 'Flat Rate', 1, 'flate_rate', ''),
	(10, 'UPS Shipping', 1, 'ups_shipping', '{"nextDayAir":"Next Day Air","secondDayAir":"2nd Day Air","ground":"Ground","threeDaySelect":"3 Day Select","nextDayAirSaver":"Next Day AirSaver","nextDayAirEarlyAM":"Next Day Air Early A.M.","secondndDayAirAM":"2nd Day Air A.M."}');
/*!40000 ALTER TABLE `shipping_description` ENABLE KEYS */;

-- Dumping structure for table shop.shipping_methods
CREATE TABLE IF NOT EXISTS `shipping_methods` (
  `shipping_methods_id` int(100) NOT NULL AUTO_INCREMENT,
  `methods_type_link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `table_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shipping_methods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.shipping_methods: 4 rows
/*!40000 ALTER TABLE `shipping_methods` DISABLE KEYS */;
INSERT INTO `shipping_methods` (`shipping_methods_id`, `methods_type_link`, `isDefault`, `status`, `table_name`) VALUES
	(1, 'upsShipping', 1, 1, 'ups_shipping'),
	(2, 'freeShipping', 0, 1, 'free_shipping'),
	(3, 'localPickup', 0, 1, 'local_pickup'),
	(4, 'flateRate', 0, 1, 'flate_rate');
/*!40000 ALTER TABLE `shipping_methods` ENABLE KEYS */;

-- Dumping structure for table shop.sliders_images
CREATE TABLE IF NOT EXISTS `sliders_images` (
  `sliders_id` int(11) NOT NULL AUTO_INCREMENT,
  `sliders_title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sliders_url` varchar(255) NOT NULL,
  `sliders_image` varchar(255) NOT NULL,
  `sliders_group` varchar(64) NOT NULL,
  `sliders_html_text` mediumtext NOT NULL,
  `expires_date` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `type` varchar(64) NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `languages_id` int(100) NOT NULL,
  PRIMARY KEY (`sliders_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table shop.sliders_images: 9 rows
/*!40000 ALTER TABLE `sliders_images` DISABLE KEYS */;
INSERT INTO `sliders_images` (`sliders_id`, `sliders_title`, `sliders_url`, `sliders_image`, `sliders_group`, `sliders_html_text`, `expires_date`, `date_added`, `status`, `type`, `date_status_change`, `languages_id`) VALUES
	(1, 'Slider-1', '', 'resources/assets/images/banner_images/1531931372.banner-001.jpg', '', '', '2029-01-03 00:00:00', '2018-09-07 16:39:40', 1, 'mostliked', '2018-09-07 16:39:40', 1),
	(2, 'Slider-2', 'ruffled-cotton-cardigan', 'resources/assets/images/banner_images/1531842089.SLID3-(1).jpg', '', '', '2019-01-31 00:00:00', '2018-07-20 10:29:55', 1, 'product', '2018-07-20 10:29:55', 1),
	(3, 'Slider-3', 'ruffled-cotton-cardigan', 'resources/assets/images/banner_images/1531842038.SLID5-3.jpg', '', '', '2029-01-01 00:00:00', '2018-07-20 10:30:12', 1, 'special', '2018-07-20 10:30:12', 1),
	(6, 'Slider-4', 'ruffled-cotton-cardigan', 'resources/assets/images/banner_images/1531842053.BANNAR_4_5.jpg', '', '', '2029-01-01 00:00:00', '2018-07-20 10:33:07', 1, 'product', '2018-07-20 10:33:07', 1),
	(10, 'المنزلق-1', '', 'resources/assets/images/slider_images/1531931450.Slide_1.jpg', '', '', '2030-01-01 00:00:00', '2018-07-20 10:33:23', 1, 'special', '2018-07-20 10:33:23', 4),
	(11, 'المنزلق-2', 'ruffled-cotton-cardigan', 'resources/assets/images/slider_images/1531931534.SLIDE_3.jpg', '', '', '2030-01-01 00:00:00', '2018-07-20 10:33:27', 1, 'product', '2018-07-20 10:33:27', 4),
	(12, 'المنزلق-3', '', 'resources/assets/images/slider_images/1531931568.Slide-5_1.jpg', '', '', '2030-01-01 00:00:00', '2018-07-20 10:33:32', 1, 'topsellerr', '2018-07-20 10:33:32', 4),
	(13, 'المنزلق-4', 'men-polo-shirts', 'resources/assets/images/banner_images/1531931918.BANNAR_4_1.jpg', '', '', '2030-01-01 00:00:00', '2018-07-20 10:33:37', 1, 'category', '2018-07-20 10:33:37', 4),
	(15, 'TitleOne', 'women-dresses', 'resources/assets/images/slider_images/1536338206.3.jpg', '', '', '2018-09-28 00:00:00', '2018-09-07 16:36:46', 1, 'category', NULL, 1);
/*!40000 ALTER TABLE `sliders_images` ENABLE KEYS */;

-- Dumping structure for table shop.specials
CREATE TABLE IF NOT EXISTS `specials` (
  `specials_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `specials_new_products_price` decimal(15,2) NOT NULL,
  `specials_date_added` int(100) NOT NULL,
  `specials_last_modified` int(100) NOT NULL,
  `expires_date` int(100) NOT NULL,
  `date_status_change` int(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`specials_id`),
  KEY `idx_specials_products_id` (`products_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.specials: ~22 rows (approximately)
/*!40000 ALTER TABLE `specials` DISABLE KEYS */;
INSERT INTO `specials` (`specials_id`, `products_id`, `specials_new_products_price`, `specials_date_added`, `specials_last_modified`, `expires_date`, `date_status_change`, `status`) VALUES
	(1, 25, 150.00, 1502195102, 1530892400, 1667174400, 1530892400, 0),
	(2, 39, 27.85, 1502264917, 1530892284, 1640995200, 1530892284, 0),
	(3, 43, 21.99, 1502268005, 1530892260, 1640995200, 1530892260, 0),
	(4, 44, 23.55, 1502268706, 1530892229, 1640995200, 1530892229, 0),
	(5, 48, 450.00, 1502274870, 1530892200, 1640995200, 1530892200, 0),
	(6, 62, 22.20, 1502351882, 1530892080, 1659398400, 1530892080, 0),
	(7, 65, 23.50, 1502353123, 1530891995, 1646092800, 1530891995, 0),
	(8, 67, 445.00, 1502362089, 1530891985, 1640995200, 1530891985, 0),
	(9, 70, 23.99, 1502363119, 1530891969, 1640995200, 1530891969, 0),
	(10, 73, 23.50, 1502364697, 1530892005, 1640995200, 1530892005, 0),
	(11, 80, 99.99, 1502366586, 1530891416, 1640995200, 1530891416, 0),
	(14, 80, 99.99, 1530891416, 0, 1640995200, 0, 1),
	(15, 70, 23.99, 1530891969, 0, 1640995200, 0, 1),
	(16, 67, 445.00, 1530891985, 0, 1640995200, 0, 1),
	(17, 65, 23.50, 1530891995, 0, 1646092800, 0, 1),
	(18, 73, 23.50, 1530892005, 0, 1640995200, 0, 1),
	(19, 62, 22.20, 1530892080, 0, 1659398400, 0, 1),
	(20, 48, 450.00, 1530892200, 0, 1640995200, 0, 1),
	(21, 44, 23.55, 1530892229, 0, 1640995200, 0, 1),
	(22, 43, 21.99, 1530892260, 0, 1640995200, 0, 1),
	(23, 39, 27.85, 1530892284, 0, 1640995200, 0, 1),
	(24, 25, 150.00, 1530892400, 0, 1667174400, 0, 1);
/*!40000 ALTER TABLE `specials` ENABLE KEYS */;

-- Dumping structure for table shop.tax_class
CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.tax_class: ~0 rows (approximately)
/*!40000 ALTER TABLE `tax_class` DISABLE KEYS */;
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES
	(1, 'Sale Tax', 'This tax apply on products related to USA item.', NULL, '2017-08-07 07:06:53');
/*!40000 ALTER TABLE `tax_class` ENABLE KEYS */;

-- Dumping structure for table shop.tax_rates
CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,2) NOT NULL,
  `tax_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.tax_rates: ~0 rows (approximately)
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`) VALUES
	(1, 43, 1, 1, 7.00, '', NULL, '2017-08-07 07:07:45');
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;

-- Dumping structure for table shop.units
CREATE TABLE IF NOT EXISTS `units` (
  `unit_id` int(100) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `languages_id` int(100) NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table shop.units: 3 rows
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` (`unit_id`, `unit_name`, `date_added`, `last_modified`, `is_active`, `languages_id`) VALUES
	(1, 'Gram', NULL, NULL, 1, 0),
	(2, 'Kilogram', NULL, NULL, 1, 0),
	(4, 'GB', NULL, NULL, 1, 0);
/*!40000 ALTER TABLE `units` ENABLE KEYS */;

-- Dumping structure for table shop.ups_shipping
CREATE TABLE IF NOT EXISTS `ups_shipping` (
  `ups_id` int(100) NOT NULL AUTO_INCREMENT,
  `pickup_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isDisplayCal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serviceType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shippingEnvironment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_package` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_height` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parcel_width` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.ups_shipping: 1 rows
/*!40000 ALTER TABLE `ups_shipping` DISABLE KEYS */;
INSERT INTO `ups_shipping` (`ups_id`, `pickup_method`, `isDisplayCal`, `serviceType`, `shippingEnvironment`, `user_name`, `access_key`, `password`, `person_name`, `company_name`, `phone_number`, `address_line_1`, `address_line_2`, `country`, `state`, `post_code`, `city`, `no_of_package`, `parcel_height`, `parcel_width`, `title`) VALUES
	(1, '07', '', 'US_01,US_02,US_03,US_12,US_13,US_14,US_59', '0', 'nyblueprint', 'FCD7C8F94CB5EF46', 'delfia11', '', '', '', 'D Ground', '', 'US', 'NY', '10312', 'New York City', '', '', '', '');
/*!40000 ALTER TABLE `ups_shipping` ENABLE KEYS */;

-- Dumping structure for table shop.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `rememberToken` int(100) NOT NULL,
  `timestamps` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table shop.vendors
CREATE TABLE IF NOT EXISTS `vendors` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendors_gender` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `vendors_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendors_lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `available_qty` int(11) NOT NULL DEFAULT '3',
  `used_qty` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendors_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendors_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `vendors_newsletter` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `fb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendors_picture` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(100) NOT NULL,
  `updated_at` int(100) NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vendor_id`),
  KEY `idx_vendors_email_address` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.vendors: ~3 rows (approximately)
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` (`vendor_id`, `vendors_gender`, `vendors_firstname`, `vendors_lastname`, `company`, `available_qty`, `used_qty`, `email`, `user_name`, `vendors_telephone`, `vendors_fax`, `password`, `vendors_newsletter`, `isActive`, `fb_id`, `google_id`, `vendors_picture`, `address`, `city`, `state`, `zip`, `country`, `created_at`, `updated_at`, `is_seen`, `remember_token`) VALUES
	(4, '1', 'Aye Chan', 'Thaw', NULL, 3, 0, 'ayechanthaw@gmail.com', '', '', NULL, '$2y$10$HVbQ8gEXPvGmn/ZUiOuh0ODzuA6H2Xq7izTsLBTWZokAu/EtMCmT.', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/1537605219.empty.jpg', NULL, NULL, NULL, NULL, NULL, 18, 18, 0, ''),
	(5, '1', 'Maria', 'Hmue', 'Shwe Lamin Nagar', 3, 0, 'mariahmue@gmail.com', '', '', NULL, '$2y$10$mbo/TPjztiunnEHzvJ09l./loU7dPbYIlbG0P.Qmzk4tyk5hEqsUq', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/1537629760.empty.jpg', 'Mandalay', 'Mandalay', 'Mandalay', NULL, 'Myanmar', 18, 18, 0, ''),
	(6, '0', 'Yan Hmue', 'Aung', 'LoT', 3, 0, 'yanhmueaung@gmail.com', '', '', NULL, '$2y$10$RH6Z.PKWvtvqcE2rU5mFpus0nBChM4osGGj4CuDxYT54j7jK5T30u', NULL, 0, NULL, NULL, 'resources/assets/images/user_profile/default_user.png', 'HlaingTarYar', 'HlaingTarYar', 'Yangon', '8798', 'Myanmar', 18, 18, 0, '');
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;

-- Dumping structure for table shop.whos_online
CREATE TABLE IF NOT EXISTS `whos_online` (
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `time_entry` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `time_last_click` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `last_page_url` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.whos_online: ~36 rows (approximately)
/*!40000 ALTER TABLE `whos_online` DISABLE KEYS */;
INSERT INTO `whos_online` (`customer_id`, `full_name`, `session_id`, `ip_address`, `time_entry`, `time_last_click`, `last_page_url`) VALUES
	(1, 'Nasir Ali', '', '', '2018-07-10 16:', '', ''),
	(2, 'Rabia Saqib', '', '', '2017-08-25 20:', '', ''),
	(3, 'Rosemar Habitat', '', '', '2018-06-10 20:', '', ''),
	(4, 'Hassan Mehboob', '', '', '2017-12-13 05:', '', ''),
	(5, 'Heggd Hdggd', '', '', '2017-10-25 06:', '', ''),
	(8, 'Rosemond Faustin', '', '', '2018-04-05 23:', '', ''),
	(10, 'Test Test', '', '', '2017-10-12 10:', '', ''),
	(11, 'Manohar Veera', '', '', '2017-10-14 05:', '', ''),
	(13, 'Ali Hassan', '', '', '2018-07-13 14:', '', ''),
	(14, 'Hafiz Muneeb', '', '', '2017-10-18 11:', '', ''),
	(15, 'Saqib Ali', '', '', '2018-06-01 12:', '', ''),
	(16, 'Muzammil Younas', '', '', '2018-06-01 12:', '', ''),
	(17, 'Rosemond Faustin', '', '', '2018-06-02 18:', '', ''),
	(18, 'Saqib Ali', '', '', '2018-06-04 10:', '', ''),
	(21, 'Ruli Setiawan', '', '', '2017-11-01 16:', '', ''),
	(22, 'android ecommerce', '', '', '2018-06-20 13:', '', ''),
	(23, 'Yazeed Ayyash', '', '', '2017-11-02 19:', '', ''),
	(24, 'Abood Ayyash', '', '', '2017-12-16 20:', '', ''),
	(29, 'Standard Account', '', '', '2018-07-09 15:', '', ''),
	(32, 'Luca Mellano', '', '', '2017-11-29 21:', '', ''),
	(34, 'hassan mehboob', '', '', '2018-06-29 13:', '', ''),
	(37, 'dfg dfg', '', '', '2017-11-20 14:', '', ''),
	(38, 'KISHORE S', '', '', '2018-07-09 15:', '', ''),
	(39, 'Amir Hassan', '', '', '2018-07-10 16:', '', ''),
	(40, 'Amir Saleem', '', '', '2018-07-09 16:', '', ''),
	(41, 'Aseem Kumar', '', '', '2017-11-24 13:', '', ''),
	(44, 'Roadsel Roadsel', '', '', '2018-07-18 23:', '', ''),
	(45, 'David Moder', '', '', '2018-07-17 13:', '', ''),
	(47, 'Saqib Ali', '', '', '2018-07-18 16:', '', ''),
	(48, 'hassan mehboob', '', '', '2017-12-11 09:', '', ''),
	(49, 'hassan mehboob', '', '', '2017-12-11 10:', '', ''),
	(50, 'hassan mehboob', '', '', '2017-12-11 10:', '', ''),
	(51, 'H N', '', '', '2017-12-11 10:', '', ''),
	(52, 'hassan mehboob', '', '', '2017-12-11 15:', '', ''),
	(53, 'Muhammad Muzammel', '', '', '2017-12-12 08:', '', ''),
	(55, 'معصوم محمود', '', '', '2017-12-18 07:', '', '');
/*!40000 ALTER TABLE `whos_online` ENABLE KEYS */;

-- Dumping structure for table shop.zones
CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.zones: ~181 rows (approximately)
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES
	(1, 223, 'AL', 'Alabama'),
	(2, 223, 'AK', 'Alaska'),
	(3, 223, 'AS', 'American Samoa'),
	(4, 223, 'AZ', 'Arizona'),
	(5, 223, 'AR', 'Arkansas'),
	(6, 223, 'AF', 'Armed Forces Africa'),
	(7, 223, 'AA', 'Armed Forces Americas'),
	(8, 223, 'AC', 'Armed Forces Canada'),
	(9, 223, 'AE', 'Armed Forces Europe'),
	(10, 223, 'AM', 'Armed Forces Middle East'),
	(11, 223, 'AP', 'Armed Forces Pacific'),
	(12, 223, 'CA', 'California'),
	(13, 223, 'CO', 'Colorado'),
	(14, 223, 'CT', 'Connecticut'),
	(15, 223, 'DE', 'Delaware'),
	(16, 223, 'DC', 'District of Columbia'),
	(17, 223, 'FM', 'Federated States Of Micronesia'),
	(18, 223, 'FL', 'Florida'),
	(19, 223, 'GA', 'Georgia'),
	(20, 223, 'GU', 'Guam'),
	(21, 223, 'HI', 'Hawaii'),
	(22, 223, 'ID', 'Idaho'),
	(23, 223, 'IL', 'Illinois'),
	(24, 223, 'IN', 'Indiana'),
	(25, 223, 'IA', 'Iowa'),
	(26, 223, 'KS', 'Kansas'),
	(27, 223, 'KY', 'Kentucky'),
	(28, 223, 'LA', 'Louisiana'),
	(29, 223, 'ME', 'Maine'),
	(30, 223, 'MH', 'Marshall Islands'),
	(31, 223, 'MD', 'Maryland'),
	(32, 223, 'MA', 'Massachusetts'),
	(33, 223, 'MI', 'Michigan'),
	(34, 223, 'MN', 'Minnesota'),
	(35, 223, 'MS', 'Mississippi'),
	(36, 223, 'MO', 'Missouri'),
	(37, 223, 'MT', 'Montana'),
	(38, 223, 'NE', 'Nebraska'),
	(39, 223, 'NV', 'Nevada'),
	(40, 223, 'NH', 'New Hampshire'),
	(41, 223, 'NJ', 'New Jersey'),
	(42, 223, 'NM', 'New Mexico'),
	(43, 223, 'NY', 'New York'),
	(44, 223, 'NC', 'North Carolina'),
	(45, 223, 'ND', 'North Dakota'),
	(46, 223, 'MP', 'Northern Mariana Islands'),
	(47, 223, 'OH', 'Ohio'),
	(48, 223, 'OK', 'Oklahoma'),
	(49, 223, 'OR', 'Oregon'),
	(50, 223, 'PW', 'Palau'),
	(51, 223, 'PA', 'Pennsylvania'),
	(52, 223, 'PR', 'Puerto Rico'),
	(53, 223, 'RI', 'Rhode Island'),
	(54, 223, 'SC', 'South Carolina'),
	(55, 223, 'SD', 'South Dakota'),
	(56, 223, 'TN', 'Tennessee'),
	(57, 223, 'TX', 'Texas'),
	(58, 223, 'UT', 'Utah'),
	(59, 223, 'VT', 'Vermont'),
	(60, 223, 'VI', 'Virgin Islands'),
	(61, 223, 'VA', 'Virginia'),
	(62, 223, 'WA', 'Washington'),
	(63, 223, 'WV', 'West Virginia'),
	(64, 223, 'WI', 'Wisconsin'),
	(65, 223, 'WY', 'Wyoming'),
	(66, 38, 'AB', 'Alberta'),
	(67, 38, 'BC', 'British Columbia'),
	(68, 38, 'MB', 'Manitoba'),
	(69, 38, 'NF', 'Newfoundland'),
	(70, 38, 'NB', 'New Brunswick'),
	(71, 38, 'NS', 'Nova Scotia'),
	(72, 38, 'NT', 'Northwest Territories'),
	(73, 38, 'NU', 'Nunavut'),
	(74, 38, 'ON', 'Ontario'),
	(75, 38, 'PE', 'Prince Edward Island'),
	(76, 38, 'QC', 'Quebec'),
	(77, 38, 'SK', 'Saskatchewan'),
	(78, 38, 'YT', 'Yukon Territory'),
	(79, 81, 'NDS', 'Niedersachsen'),
	(80, 81, 'BAW', 'Baden-Württemberg'),
	(81, 81, 'BAY', 'Bayern'),
	(82, 81, 'BER', 'Berlin'),
	(83, 81, 'BRG', 'Brandenburg'),
	(84, 81, 'BRE', 'Bremen'),
	(85, 81, 'HAM', 'Hamburg'),
	(86, 81, 'HES', 'Hessen'),
	(87, 81, 'MEC', 'Mecklenburg-Vorpommern'),
	(88, 81, 'NRW', 'Nordrhein-Westfalen'),
	(89, 81, 'RHE', 'Rheinland-Pfalz'),
	(90, 81, 'SAR', 'Saarland'),
	(91, 81, 'SAS', 'Sachsen'),
	(92, 81, 'SAC', 'Sachsen-Anhalt'),
	(93, 81, 'SCN', 'Schleswig-Holstein'),
	(94, 81, 'THE', 'Thüringen'),
	(95, 14, 'WI', 'Wien'),
	(96, 14, 'NO', 'Niederösterreich'),
	(97, 14, 'OO', 'Oberösterreich'),
	(98, 14, 'SB', 'Salzburg'),
	(99, 14, 'KN', 'Kärnten'),
	(100, 14, 'ST', 'Steiermark'),
	(101, 14, 'TI', 'Tirol'),
	(102, 14, 'BL', 'Burgenland'),
	(103, 14, 'VB', 'Voralberg'),
	(104, 204, 'AG', 'Aargau'),
	(105, 204, 'AI', 'Appenzell Innerrhoden'),
	(106, 204, 'AR', 'Appenzell Ausserrhoden'),
	(107, 204, 'BE', 'Bern'),
	(108, 204, 'BL', 'Basel-Landschaft'),
	(109, 204, 'BS', 'Basel-Stadt'),
	(110, 204, 'FR', 'Freiburg'),
	(111, 204, 'GE', 'Genf'),
	(112, 204, 'GL', 'Glarus'),
	(113, 204, 'JU', 'Graubünden'),
	(114, 204, 'JU', 'Jura'),
	(115, 204, 'LU', 'Luzern'),
	(116, 204, 'NE', 'Neuenburg'),
	(117, 204, 'NW', 'Nidwalden'),
	(118, 204, 'OW', 'Obwalden'),
	(119, 204, 'SG', 'St. Gallen'),
	(120, 204, 'SH', 'Schaffhausen'),
	(121, 204, 'SO', 'Solothurn'),
	(122, 204, 'SZ', 'Schwyz'),
	(123, 204, 'TG', 'Thurgau'),
	(124, 204, 'TI', 'Tessin'),
	(125, 204, 'UR', 'Uri'),
	(126, 204, 'VD', 'Waadt'),
	(127, 204, 'VS', 'Wallis'),
	(128, 204, 'ZG', 'Zug'),
	(129, 204, 'ZH', 'Zürich'),
	(130, 195, 'A Coruña', 'A Coruña'),
	(131, 195, 'Alava', 'Alava'),
	(132, 195, 'Albacete', 'Albacete'),
	(133, 195, 'Alicante', 'Alicante'),
	(134, 195, 'Almeria', 'Almeria'),
	(135, 195, 'Asturias', 'Asturias'),
	(136, 195, 'Avila', 'Avila'),
	(137, 195, 'Badajoz', 'Badajoz'),
	(138, 195, 'Baleares', 'Baleares'),
	(139, 195, 'Barcelona', 'Barcelona'),
	(140, 195, 'Burgos', 'Burgos'),
	(141, 195, 'Caceres', 'Caceres'),
	(142, 195, 'Cadiz', 'Cadiz'),
	(143, 195, 'Cantabria', 'Cantabria'),
	(144, 195, 'Castellon', 'Castellon'),
	(145, 195, 'Ceuta', 'Ceuta'),
	(146, 195, 'Ciudad Real', 'Ciudad Real'),
	(147, 195, 'Cordoba', 'Cordoba'),
	(148, 195, 'Cuenca', 'Cuenca'),
	(149, 195, 'Girona', 'Girona'),
	(150, 195, 'Granada', 'Granada'),
	(151, 195, 'Guadalajara', 'Guadalajara'),
	(152, 195, 'Guipuzcoa', 'Guipuzcoa'),
	(153, 195, 'Huelva', 'Huelva'),
	(154, 195, 'Huesca', 'Huesca'),
	(155, 195, 'Jaen', 'Jaen'),
	(156, 195, 'La Rioja', 'La Rioja'),
	(157, 195, 'Las Palmas', 'Las Palmas'),
	(158, 195, 'Leon', 'Leon'),
	(159, 195, 'Lleida', 'Lleida'),
	(160, 195, 'Lugo', 'Lugo'),
	(161, 195, 'Madrid', 'Madrid'),
	(162, 195, 'Malaga', 'Malaga'),
	(163, 195, 'Melilla', 'Melilla'),
	(164, 195, 'Murcia', 'Murcia'),
	(165, 195, 'Navarra', 'Navarra'),
	(166, 195, 'Ourense', 'Ourense'),
	(167, 195, 'Palencia', 'Palencia'),
	(168, 195, 'Pontevedra', 'Pontevedra'),
	(169, 195, 'Salamanca', 'Salamanca'),
	(170, 195, 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife'),
	(171, 195, 'Segovia', 'Segovia'),
	(172, 195, 'Sevilla', 'Sevilla'),
	(173, 195, 'Soria', 'Soria'),
	(174, 195, 'Tarragona', 'Tarragona'),
	(175, 195, 'Teruel', 'Teruel'),
	(176, 195, 'Toledo', 'Toledo'),
	(177, 195, 'Valencia', 'Valencia'),
	(178, 195, 'Valladolid', 'Valladolid'),
	(179, 195, 'Vizcaya', 'Vizcaya'),
	(180, 195, 'Zamora', 'Zamora'),
	(181, 195, 'Zaragoza', 'Zaragoza');
/*!40000 ALTER TABLE `zones` ENABLE KEYS */;

-- Dumping structure for table shop.zones_to_geo_zones
CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`association_id`),
  KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table shop.zones_to_geo_zones: ~0 rows (approximately)
/*!40000 ALTER TABLE `zones_to_geo_zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones_to_geo_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
