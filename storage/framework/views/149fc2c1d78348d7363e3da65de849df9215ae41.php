<div class="container-fuild">
    <div class="container">
        <div class="products-area"> 
            <!-- heading -->
            <div class="heading">
                <h2><?php echo app('translator')->getFromJson('website.Vendors'); ?> <small class="pull-right"><a href="<?php echo e(URL::to('/vendor?id=1')); ?>" ><?php echo app('translator')->getFromJson('website.View All'); ?></a></small></h2>
                <hr>
            </div>
            <div class="row">         
                
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <!-- Items -->
                        <div class="products products-5x">
                            <!-- Product --> 
                            <?php $__currentLoopData = $result['vendors']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$vendor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($key<=4): ?>
                            <div class="product">
                            <article>
                                <div class="thumb"><a href="<?php echo e(URL::to('/vendor-profile/'.$vendor->vendor_id)); ?>"><img class="img-fluid" src="<?php echo e(asset('').$vendor->vendors_picture); ?>" alt="<?php echo e($vendor->user_name); ?>"></a></div>
                                <?php
                                    $current_date = date("Y-m-d", strtotime("now"));
                                        
                                ?>
                                <h2 class="title text-center"><?php echo e($vendor->user_name); ?></h2> 
                                <div class="price text-center">
                                <?php echo e($vendor->city); ?></div>
                                <div class="text-center"><?php echo e($vendor->address); ?></div>
                            </article>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>