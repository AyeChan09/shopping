<?php $__env->startSection('content'); ?>
<section class="site-content">
	<div class="container">
		<div class="breadcum-area">
			<div class="row">
				<div class="breadcum-inner">
					<h3><?php echo app('translator')->getFromJson('website.Vendor Detail'); ?></h3>
					<?php if($result['vendor']): ?>
					<img class="img-fluid" src="<?php echo e(asset('').$result['vendor']->vendors_picture); ?>" alt="<?php echo e($result['vendor']->user_name); ?>">
					<h2><?php echo e($result['vendor']->vendors_firstname); ?></h2>
					<?php endif; ?>
				</div>
			</div>			

			<div class="row">        
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <!-- Items -->
                        <div class="products products-5x">
                            <!-- Product --> 
                            <?php $__currentLoopData = $result['detail']['products']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($key<=4): ?>
                            <div class="product">
                            <article>
                                <div class="thumb"><img class="img-fluid" src="<?php echo e(asset('').$product->products_image); ?>" alt=""></div>
                                <?php
                                    $current_date = date("Y-m-d", strtotime("now"));
                                        
                                ?>
                                <div class="price">
									<?php echo e($web_setting[19]->value); ?><?php echo e($product->products_price+0); ?>

								</div>
                            </article>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                    </div>
                </div>
            </div>

			<div class="row">        
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <!-- Items -->
                        <div class="products products-5x">
                            <!-- Product --> 
                            <?php $__currentLoopData = $result['detail']['brands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($key<=4): ?>
                            <div class="product">
                            <article>
                                <div class="thumb"><img class="img-fluid" src="<?php echo e(asset('').$brand->manufacturers_image); ?>" alt=""></div>
                                <?php
                                    $current_date = date("Y-m-d", strtotime("now"));
                                        
                                ?>
								<h2 class="title"><?php echo e($brand->manufacturers_name); ?></h2>
                            </article>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                    </div>
                </div>
            </div>
			
		</div>
	</div>
</section>
		
<?php $__env->stopSection(); ?> 	



<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>