<?php $__env->startSection('content'); ?>
<section class="site-content">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-2"></div>
            <div class="col-md-auto" style="text-align:center;">
            <h3 style="color:gold;"><?php echo app('translator')->getFromJson('website.Vendor Detail'); ?></h3>
                            <?php if($result['vendor']): ?>
                            
                                <img class="img-responsive" src="<?php echo e(asset('').$result['vendor']->vendors_picture); ?>" alt="<?php echo e($result['vendor']->user_name); ?>" style="height:100px; width=100px;">
                        
                                <h4><?php echo e($result['vendor']->vendors_firstname); ?> <?php echo e($result['vendor']->vendors_lastname); ?></h4>
                            
                                <h6><?php echo e($result['vendor']->company); ?></h6>
                                <h6><?php echo e($result['vendor']->address); ?></h6>
                            <?php endif; ?>

            </div>
            <div class="col col-lg-2"></div>
    
        </div>



<div class="row">        
    <div class="col-xs-12 col-sm-12">
            <div class="row">
    <!-- Items -->
                <div class="products products-5x">
        <!-- Product --> 
        <h3 style="color:gold;"><?php echo app('translator')->getFromJson('website.products'); ?></h3>
                        <?php $__currentLoopData = $result['detail']['products']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key<=4): ?>
                        <div class="product">
                            <article>
                                <div class="thumb"><img class="img-fluid" src="<?php echo e(asset('').$product->products_image); ?>" alt=""></div>
                                    <?php
                                            $current_date = date("Y-m-d", strtotime("now"));
                    
                                    ?>
                                    <div class="price">
                        <?php echo e($web_setting[19]->value); ?><?php echo e($product->products_price+0); ?>

                    </div>
                            </article>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
                </div>
            </div>
    </div>
</div>





</div>
</section>







<div class="container-fuild">
    <div class="container">
        <div class="products-area"> 
            <!-- heading -->
            <div class="heading">
                <h3 style="color:gold;"><?php echo app('translator')->getFromJson('website.Brands'); ?></h3>
                        
                <hr>
            </div>
            <div class="row">         
                
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <!-- Items -->
                        <div class="products products-5x">
                            <!-- Product --> 
                
                            <div id="owl-demo" class="owl-carousel owl-theme">
                         <?php $__currentLoopData = $result['detail']['brands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key<=4): ?>
                        <!-- <div class="product"> -->
                            
                            

                                 <div class="thumb"><img class="img-fluid" src="<?php echo e(asset('').$brand->manufacturers_image); ?>" alt=""></div>
                                
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

          <script>
            $(document).ready(function() {
               var owl = $('.owl-carousel');
              owl.owlCarousel({
               loop: true,
                margin: 30,
                autoplay: true,
                autoplayTimeout: 3000,
                nav: true,
                
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                 "<i class='fa fa-angle-left'></i>",
                 "<i class='fa fa-angle-right'></i>"
             ],
				
				
				 responsiveClass: true,
                 responsive: {
            0: {items: 2},
            479: {items: 3},
            768: {items: 3},
            991: {items: 6},
            1024: {items: 6}
        }

				
				
              });
             
            })





          </script>
<?php $__env->stopSection(); ?> 	



<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>