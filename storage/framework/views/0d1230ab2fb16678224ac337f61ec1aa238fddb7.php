<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="<?php echo e(asset('').'public/images/logo.png'); ?>" sizes="2x2">
  <title>Vendor Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  <meta name="Vectorcoder" content="http://ionicecommerce.com">
  <!-- Bootstrap 3.3.6 -->
  <link href="<?php echo asset('resources/views/vendor/bootstrap/css/bootstrap.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <link href="<?php echo asset('resources/views/vendor/bootstrap/css/styles.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- Font Awesome -->
  <link href="<?php echo asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/vendor/plugins/select2/select2.min.css'); ?>">
  
    <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/vendor/plugins/colorpicker/bootstrap-colorpicker.min.css'); ?>">
  <!-- Ionicons -->
  <link href="<?php echo asset('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/vendor/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
   <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/vendor/plugins/datepicker/datepicker3.css'); ?>">
  <!-- jvectormap -->
  <link href="<?php echo asset('resources/views/vendor/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo asset('resources/views/vendor/dist/css/AdminLTE.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link href="<?php echo asset('resources/views/vendor/dist/css/skins/_all-skins.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?php echo asset('resources/views/vendor/plugins/iCheck/all.css'); ?>" media="all" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
