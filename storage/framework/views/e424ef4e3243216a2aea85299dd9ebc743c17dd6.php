<style>
    .footer{
        color: #ffffff;
        background-color:#324e5c;
        padding: 30px 0;
    }

    *{
        box-sizing: border-box;
    }

    .clearfix {
        zoom: 1;
    }

    .clearfix:after {
        clear: both;
        content: ".";
        display: block;
        height: 0;
        line-height: 0;
        visibility: hidden;
    }

    .col{
        width: 25%;
        float: left;
        position: relative;
        border-right: 1px solid #ffffff;
    }

    .col:last-child{
        border: 0;
    }
    
    .banner-icon{
        position:absolute;
        left: 35px !important;
        color: #F0CA4D;
        font-size:35px !important;
        margin:0 !important;
    }

    .info{
        padding-left: 70px;
    }

    .title{
        font-size:1.2em;
        margin:0;
    }

    p{
        font-size:1em;
        margin: 0;
    }

    /* For Tablet View */
    @media  only screen and (max-width: 768px) {
        .col{
            width: 50%;
            margin-bottom: 15px;
        }
        .col:nth-child(2){
            border: 0;
        }
        .col:nth-child(n+3){
            margin: 0;
        }
    }
    /* For Mobile View */
    @media  only screen and (max-width: 480px) {
        .col{
            width: 100%;
            margin-bottom: 15px !important;
            border: 0;
        }
        .col:last-child{
            margin: 0 !important;
        }
    }
</style>
    
    
    <div class="container">
		<div class="footer row clearfix">
			<div class="col">
                <h3 class="fa fa-truck banner-icon"></h3>
                <div class="info">
                    <h4 class="title"><?php echo app('translator')->getFromJson('website.bannerLabel1'); ?></h4>
                    <p><?php echo app('translator')->getFromJson('website.bannerLabel1Text'); ?></p>
                </div>
            </div>
            <div class="col">
                <h3 class="fa fa-money banner-icon"></h3>
                <div class="info">
                    <h4 class="title"><?php echo app('translator')->getFromJson('website.bannerLabel2'); ?></h4>
                    <p><?php echo app('translator')->getFromJson('website.bannerLabel2Text'); ?></p>
                </div>
            </div>
            <div class="col">
                <h3 class="fa fa-life-ring banner-icon"></h3>
                <div class="info">
                    <h4 class="title"><?php echo app('translator')->getFromJson('website.bannerLabel3'); ?></h4>
                    <p><?php echo app('translator')->getFromJson('website.hotline'); ?>&nbsp;:&nbsp;<?php echo e($result['commonContent']['setting'][11]->value); ?></p>
                </div>
            </div>
            <div class="col">
                <h3 class="fa fa-credit-card banner-icon"></h3>
                <div class="info">
                    <h4 class="title"><?php echo app('translator')->getFromJson('website.bannerLabel4'); ?></h4>
                    <p><?php echo app('translator')->getFromJson('website.bannerLabel4Text'); ?></p>
                </div>
            </div>
        </div>
	</div>
