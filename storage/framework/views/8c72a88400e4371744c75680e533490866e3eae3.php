<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="editManufacturerLabel"><?php echo e(trans('labels.EditAddress')); ?></h4>
</div>
  <?php echo Form::open(array('url' =>'admin/updateAddress', 'name'=>'editAddressFrom', 'id'=>'editAddressFrom', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

		  <?php echo Form::hidden('customers_id',  $data['customers_id'], array('class'=>'form-control')); ?>

          <?php echo Form::hidden('address_book_id',  $data['customer_addresses'][0]->address_book_id, array('class'=>'form-control')); ?>

<div class="modal-body">
    <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Company')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_company',  $data['customer_addresses'][0]->entry_company, array('class'=>'form-control', 'id'=>'entry_company')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.FirstName')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_firstname',  $data['customer_addresses'][0]->entry_firstname, array('class'=>'form-control', 'id'=>'entry_firstname')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.LastName')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_lastname',  $data['customer_addresses'][0]->entry_lastname, array('class'=>'form-control', 'id'=>'entry_lastname')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.StreetAddress')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_street_address',  $data['customer_addresses'][0]->entry_street_address, array('class'=>'form-control', 'id'=>'entry_street_address')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Suburb')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_suburb',  $data['customer_addresses'][0]->entry_suburb, array('class'=>'form-control', 'id'=>'entry_suburb')); ?>

      </div>
   </div>
   
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Postcode')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_postcode',  $data['customer_addresses'][0]->entry_postcode, array('class'=>'form-control', 'id'=>'entry_postcode')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.City')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_city',  $data['customer_addresses'][0]->entry_city, array('class'=>'form-control', 'id'=>'entry_city')); ?>

      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.State')); ?></label>
      <div class="col-sm-10 col-md-8">
        <?php echo Form::text('entry_state',  $data['customer_addresses'][0]->entry_state, array('class'=>'form-control', 'id'=>'entry_state')); ?>

      </div>
   </div>
   
   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Country')); ?></label>
      <div class="col-sm-10 col-md-8">
          <select id="entry_country_id" class="form-control" name="entry_country_id">								 
             <?php $__currentLoopData = $data['countries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countries_data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option 
              <?php if($data['customer_addresses'][0]->entry_country_id == $countries_data->countries_id ): ?>
                selected
              <?php endif; ?>
               value="<?php echo e($countries_data->countries_id); ?>"><?php echo e($countries_data->countries_name); ?></option>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>										 
          </select>
      </div>
    </div>

   <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Zone')); ?></label>
      <div class="col-sm-10 col-md-8">
          <select class="form-control zoneContent" name="entry_zone_id">									 
                 <?php $__currentLoopData = $data['zones']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $zones_data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option 
                  <?php if($data['customer_addresses'][0]->entry_zone_id == $zones_data->zone_id ): ?>
                    selected
                  <?php endif; ?>
                   value="<?php echo e($zones_data->zone_id); ?>"><?php echo e($zones_data->zone_name); ?></option>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>										 
          </select>	
      </div>
    </div>
    
    <div class="form-group">
      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.DefaultShippingAddress')); ?></label>
      <div class="col-sm-10 col-md-8">
          <select id="is_default" class="form-control" name="is_default">	
              <option
                  <?php if($data['customers'][0]->customers_default_address_id != $data['customer_addresses'][0]->address_book_id ): ?>
                    selected
                  <?php endif; ?>
               value="0">No</option>
              <option
              <?php if($data['customers'][0]->customers_default_address_id == $data['customer_addresses'][0]->address_book_id ): ?>
                    selected
                  <?php endif; ?>
               value="1">Yes</option>								 
          </select>
      </div>
    </div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.Close')); ?></button>
	<button type="button" class="btn btn-primary" id="updateAddress"><?php echo e(trans('labels.Update')); ?></button>
</div>
  <?php echo Form::close(); ?>