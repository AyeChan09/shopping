<?php $__env->startSection('content'); ?>
<section class="site-content">
<div class="container">
<div class="breadcum-area">
    <div class="breadcum-inner">
        <h3><?php echo app('translator')->getFromJson('website.Vendor Signup'); ?></h3>
        <ol class="breadcrumb">
            
            <li class="breadcrumb-item"><a href="<?php echo e(URL::to('/')); ?>"><?php echo app('translator')->getFromJson('website.Home'); ?></a></li>
            <li class="breadcrumb-item active"><?php echo app('translator')->getFromJson('website.Signup'); ?></li>
        </ol>
    </div>
</div>

<div class="registration-area">

        <div class="heading">
            <h2><?php echo app('translator')->getFromJson('website.Create An Account'); ?></h2>
            <hr>
        </div>
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 new-customers">
				<h5 class="title-h5"><?php echo app('translator')->getFromJson('website.Vendor Information'); ?></h5>
				<!-- <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p> -->

				<hr class="featurette-divider">
				<?php if( count($errors) > 0): ?>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only"><?php echo app('translator')->getFromJson('website.Error'); ?>:</span>
                            <?php echo e($error); ?>

                          	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
						</div>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>

				<?php if(Session::has('error')): ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						  <span class="sr-only"><?php echo app('translator')->getFromJson('website.Error'); ?>:</span>
						  <?php echo session('error'); ?>

                          
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
					</div>
				<?php endif; ?>

				<?php if(Session::has('success')): ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						  <span class="sr-only"><?php echo app('translator')->getFromJson('website.Success'); ?>:</span>
						  <?php echo session('success'); ?>

                          
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          		<span aria-hidden="true">&times;</span>
                          </button>
					</div>
				<?php endif; ?>

				<form name="signup" enctype="multipart/form-data" class="form-validate" action="<?php echo e(URL::to('/vendorSignupProcess')); ?>" method="post">
                
                	<div class="form-group row justify-content-center">
						
                        <div class="uploader">
                        	<h5 class="title-h5"><?php echo app('translator')->getFromJson('website.Upload Vendor Photo'); ?></h5>
                            <div class="upload-picture">
                                <div class="uploaded-image" id="uploaded_image"></div>
                                <?php if(empty('picture')): ?>
                                <img class="upload-choose-icon" src="<?php echo e(asset('').'public/images/default.png'); ?>" />
                                <?php endif; ?>
                                <div class="upload-choose-icon">
                                	<input name="picture" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
					</div>
                    
                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.First Name'); ?></label>
                            <div>
                                <input type="text" name="firstName" id="firstName" class="form-control field-validate" value="<?php echo e(old('firstName')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your first name'); ?></span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inputPassword" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Last Name'); ?></label>
                            <div>
                                <input type="text" name="lastName" id="lastName" class="form-control field-validate"  value="<?php echo e(old('lastName')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your last name'); ?></span> 
                            </div>
                        </div>
					</div>

					<div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="inputPassword" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Email Adrress'); ?></label>
                            <div>
                                <input type="text" name="email" id="email" class="form-control email-validate" value="<?php echo e(old('email')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your valid email address'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inlineFormCustomSelect" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Gender'); ?></label>
                            <div>
                                <select class="custom-select field-validate" name="gender" id="inlineFormCustomSelect">
                                    <option selected value=""><?php echo app('translator')->getFromJson('website.Choose...'); ?></option>
                                    <option value="0" <?php if(!empty(old('gender')) and old('gender')==0): ?> selected <?php endif; ?>)><?php echo app('translator')->getFromJson('website.Male'); ?></option>
                                    <option value="1" <?php if(!empty(old('gender')) and old('gender')==1): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('website.Female'); ?></option>
                                </select>
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please select your gender'); ?></span>
                            </div>
                        </div>
					</div>
					
					<div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="inputPassword4" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Password'); ?></label>
                            <div>
                                <input type="password" class="form-control password" name="password" id="password">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your password'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inputPassword5" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Confirm Password'); ?></label>
                            <div class="re-password-content">
                                <input type="password" class="form-control password" name="re_password" id="re_password">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please re-enter your password'); ?></span>
                                <span class="help-block error-content-password" hidden><?php echo app('translator')->getFromJson('website.Password does not match the confirm password'); ?></span>
                            </div>				  	
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-12">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Company'); ?></label>
                            <div>
                                <input type="text" name="company" id="company" class="form-control field-validate" value="<?php echo e(old('company')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your company'); ?></span> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-12">
                            <label for="staticEmail" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Address'); ?></label>
                            <div>
                                <input type="text" name="address" id="address" class="form-control field-validate" value="<?php echo e(old('address')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your address'); ?></span> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="city" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.City'); ?></label>
                            <div>
                                <input type="text" name="city" id="city" class="form-control field-validate" value="<?php echo e(old('city')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your city'); ?></span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="state" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.State'); ?></label>
                            <div>
                                <input type="text" name="state" id="state" class="form-control field-validate"  value="<?php echo e(old('state')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your state'); ?></span> 
                            </div>
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
                        <div class="col-sm-6">
                            <label for="country" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Country'); ?></label>
                            <div>
                                <input type="text" name="country" id="country" class="form-control field-validate" value="<?php echo e(old('country')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your country'); ?></span> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="zip" class="col-form-label"><strong>*</strong><?php echo app('translator')->getFromJson('website.Zip'); ?></label>
                            <div>
                                <input type="text" name="zip" id="zip" class="form-control field-validate"  value="<?php echo e(old('zip')); ?>">
                                <span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please enter your zip'); ?></span> 
                            </div>
                        </div>
					</div>

                    <div class="form-group row col-sm-10 offset-md-1">
						<!-- <label class="col-sm-4 col-form-label"></label> -->
						<div class="col-sm-12">
							<div class="form-check checkbox-parent">
								<label class="form-check-label">
									<input class="form-check-input checkbox-validate" type="checkbox"><?php echo app('translator')->getFromJson('website.Creating an account means you are okay with our'); ?>  <?php if(!empty($result['commonContent']['pages'][3]->slug)): ?><a href="<?php echo e(URL::to('/page?name='.$result['commonContent']['pages'][3]->slug)); ?>"><?php endif; ?> <?php echo app('translator')->getFromJson('website.Terms and Services'); ?><?php if(!empty($result['commonContent']['pages'][3]->slug)): ?></a><?php endif; ?>, <?php if(!empty($result['commonContent']['pages'][1]->slug)): ?><a href="<?php echo e(URL::to('/page?name='.$result['commonContent']['pages'][1]->slug)); ?>"><?php endif; ?> <?php echo app('translator')->getFromJson('website.Privacy Policy'); ?><?php if(!empty($result['commonContent']['pages'][1]->slug)): ?></a> <?php endif; ?> and <?php if(!empty($result['commonContent']['pages'][2]->slug)): ?><a href="<?php echo e(URL::to('/page?name='.$result['commonContent']['pages'][2]->slug)); ?>"><?php endif; ?> <?php echo app('translator')->getFromJson('website.Refund Policy'); ?> <?php if(!empty($result['commonContent']['pages'][3]->slug)): ?></a><?php endif; ?>.
								</label>
								<span class="help-block error-content" hidden><?php echo app('translator')->getFromJson('website.Please accept our terms and conditions'); ?></span>
							</div>
                            
						</div>
					</div>
					<div class="button">
                    	<button type="submit" class="btn btn-dark pull-right"><?php echo app('translator')->getFromJson('website.Sign Up'); ?></button>
                    </div>
				</form>
			</div>
		</div>
	</div>		
	</div>
   </section>
		
<?php $__env->stopSection(); ?> 	



<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>