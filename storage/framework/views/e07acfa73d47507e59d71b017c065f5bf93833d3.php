<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- head -->
    <meta charset="utf-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Autoplay usage demo">
    <meta name="author" content="David Deutsch">
    <title>
      Autoplay Demo | Owl Carousel | 2.3.4
    </title>

    <!-- Stylesheets -->
    
    <link href="acss/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="docs.theme.css">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="owl.carousel.css">
    <link rel="stylesheet" href="owl.theme.default.css">




    <style type="text/css">
      #owl-demo .item{
  background: #3fbf79;
  padding: 10px 10px;
  margin: 5px;
  color: #FFF;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  text-align: center;
}
.customNavigation{
  text-align: center;
}
//use styles below to disable ugly selection
.customNavigation a{
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
    </style>


    <!-- Favicons -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Yeah i know js should not be in header. Its required for demos.-->

    <!-- javascript -->
    <script src="jquery.min.js"></script>
    <script src="owl.carousel.js"></script>
  </head>
  <body>

    
    <!-- title -->
    <section class="title">
      <div class="row">
        <div class="large-12 columns">
          <h1></h1>
        </div>
      </div>
    </section>

    <!--  Demos -->
   <!--  <section id="demos">
      <div class="row">
        <div class="large-12 columns">
          <div class="owl-carousel owl-theme">
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
            <img src="hm.jpg" class="item"></img>
          </div>
        </div>
      </div>
    </section> -->






    <div id="owl-demo" class="owl-carousel owl-theme">
  <!-- <div class="item"><h1>1</h1></div>
  <div class="item"><h1>2</h1></div>
  <div class="item"><h1>3</h1></div> -->
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
  <img class="item" src="<?php echo e(asset('').'public/images/itune.png'); ?>"></img>
   <!-- <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img>
   <img src="hm.jpg" class="item"></img> -->
  <!-- <div class="item"><h1>12</h1></div>
  <div class="item"><h1>13</h1></div>
  <div class="item"><h1>14</h1></div>
  <div class="item"><h1>15</h1></div>
  <div class="item"><h1>16</h1></div> -->
</div>
 
<div class="customNavigation">
  <a class="btn prev">Previous</a>
  <a class="btn next">Next</a>
  <a class="btn play">Autoplay</a>
  <a class="btn stop">Stop</a>
</div>






    <!--      <a class="button secondary play">Play</a> 
          <a class="button secondary stop">Stop</a> -->
       
          <script>
            $(document).ready(function() {
              set_featured_product_box_height();
              var owl = $('.owl-carousel');
              owl.owlCarousel({
               // items: 4,
               // loop: true,
               loop: true,
                margin: 30,
                autoplay: true,
                autoplayTimeout: 30000,
                autoplayHoverPause: true,

            //      nav: true,
            //     navText: [
            //     "<i class='fa fa-angle-left'></i>",
            //     "<i class='fa fa-angle-right'></i>"
            // ],
				
				
				 responsiveClass: true,
                 responsive: {
            0: {items: 2},
            479: {items: 3},
            768: {items: 3},
            991: {items: 6},
            1024: {items: 6}
        }

				
				
              });
              // $('.play').on('click', function() {
              //   owl.trigger('play.owl.autoplay', [1000])
              // })
              // $('.stop').on('click', function() {
              //   owl.trigger('stop.owl.autoplay')
              // })
            })



            function set_featured_product_box_height(){
    var max_title=0;
    $('.featured-products .caption-title').each(function(){
        var current_height= parseInt($(this).css('height'));
        if(current_height >= max_title){
            max_title = current_height;
        }
    });
    $('.featured-products .caption-title').css('height',max_title);
}

          </script>
      

      <script src="ajs/bootstrap.min.js"></script>

    <!-- vendors -->
    <script src="ahighlight.js"></script>
    <script src="aapp.js"></script>

  </body>
</html>