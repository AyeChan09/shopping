<?php
	/*
	Project Name: IonicEcommerce
	Project URI: http://ionicecommerce.com
	Author: VectorCoder Team
	Author URI: http://vectorcoder.com/
	Version: 1 -desktop
	*/
	header("Cache-Control: no-cache, must-revalidate");
	header('Access-Control-Allow-Origin:  *');
	header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
	header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Admin controller Routes
|--------------------------------------------------------------------------
|
| This section contains all admin Routes
| 
|
*/

Route::group(['prefix' => 'admin'], function () {
	
	Route::group(['namespace' => 'Admin'], function () {

		Route::group(['middleware' => 'admin'], function () {
			Route::get('/dashboard/{reportBase}', 'AdminController@dashboard');
			Route::get('/post', 'AdminController@myPost');
			//show admin personal info record
			Route::get('/adminInfo', 'AdminController@adminInfo');

		/*
		|--------------------------------------------------------------------------
		| categories/Product Controller Routes
		|--------------------------------------------------------------------------
		|
		| This section contains categories/Product Controller Routes
		| 
		|
		*/
			//main listingManufacturer
			Route::get('/manufacturers', 'AdminManufacturerController@manufacturers');
			Route::get('/addmanufacturer', 'AdminManufacturerController@addmanufacturer');
			Route::post('/addnewmanufacturer', 'AdminManufacturerController@addnewmanufacturer');
			Route::get('/editmanufacturer/{id}', 'AdminManufacturerController@editmanufacturer');
			Route::post('/updatemanufacturer', 'AdminManufacturerController@updatemanufacturer');
			Route::post('/deletemanufacturer', 'AdminManufacturerController@deletemanufacturer');

			//main categories
			Route::get('/categories', 'AdminCategoriesController@categories');
			Route::get('/addcategory', 'AdminCategoriesController@addcategory');
			Route::post('/addnewcategory', 'AdminCategoriesController@addnewcategory');
			Route::get('/editcategory/{id}', 'AdminCategoriesController@editcategory');
			Route::post('/updatecategory', 'AdminCategoriesController@updatecategory');
			Route::get('/deletecategory/{id}', 'AdminCategoriesController@deletecategory');

			//sub categories
			Route::get('/subcategories', 'AdminCategoriesController@subcategories');
			Route::get('/addsubcategory', 'AdminCategoriesController@addsubcategory');
			Route::post('/addnewsubcategory', 'AdminCategoriesController@addnewsubcategory');
			Route::get('/editsubcategory/{id}', 'AdminCategoriesController@editsubcategory');
			Route::post('/updatesubcategory', 'AdminCategoriesController@updatesubcategory');
			Route::get('/deletesubcategory/{id}', 'AdminCategoriesController@deletesubcategory');
			
			Route::post('/getajaxcategories', 'AdminCategoriesController@getajaxcategories');

			//products
			Route::get('/products', 'AdminProductsController@products');
			Route::get('/addproduct', 'AdminProductsController@addproduct');
			Route::post('/addnewproduct', 'AdminProductsController@addnewproduct');

			//add attribute against newly added product
			Route::get('/addproductattribute/{id}/', 'AdminProductsController@addproductattribute');
			Route::get('/addproductimages/{id}/', 'AdminProductsController@addproductimages');
			Route::post('/addnewdefaultattribute', 'AdminProductsController@addnewdefaultattribute');
			Route::post('/addnewproductattribute', 'AdminProductsController@addnewproductattribute');
			Route::post('/updateproductattribute', 'AdminProductsController@updateproductattribute');
			Route::post('/updatedefaultattribute', 'AdminProductsController@updatedefaultattribute');
			Route::post('/deleteproduct', 'AdminProductsController@deleteproduct');
			Route::post('/deleteproductattribute', 'AdminProductsController@deleteproductattribute');
			Route::post('/addnewproductimage', 'AdminProductsController@addnewproductimage');
			Route::post('/deletedefaultattribute', 'AdminProductsController@deletedefaultattribute');
			Route::post('editproductattribute', 'AdminProductsController@editproductattribute');
			Route::post('editdefaultattribute', 'AdminProductsController@editdefaultattribute');
			Route::post('addnewproductimagemodal', 'AdminProductsController@addnewproductimagemodal');
			Route::post('deleteproductattributemodal', 'AdminProductsController@deleteproductattributemodal');
			Route::post('deletedefaultattributemodal', 'AdminProductsController@deletedefaultattributemodal');

			//product attribute
			Route::post('/addnewproductimage', 'AdminProductsController@addnewproductimage');
			Route::post('editproductimage', 'AdminProductsController@editproductimage');
			Route::post('/updateproductimage', 'AdminProductsController@updateproductimage');
			Route::post('/deleteproductimagemodal', 'AdminProductsController@deleteproductimagemodal');
			Route::post('/deleteproductimage', 'AdminProductsController@deleteproductimage');
			Route::get('/editproduct/{id}', 'AdminProductsController@editproduct');
			Route::post('/updateproduct', 'AdminProductsController@updateproduct');	
			Route::post('/getOptions', 'AdminProductsController@getOptions');	
			Route::post('/getOptionsValue', 'AdminProductsController@getOptionsValue');	


			//Attribute
			Route::get('/attributes', 'AdminProductsController@attributes');
			Route::get('/addattributes', 'AdminProductsController@addattributes');
			Route::post('/addnewattributes', 'AdminProductsController@addnewattributes');
			Route::get('/editattributes/{id}/{language_id}', 'AdminProductsController@editattributes');
			Route::post('/updateattributes/', 'AdminProductsController@updateattributes');
			Route::post('/deleteattribute', 'AdminProductsController@deleteattribute');
			Route::post('/addattributevalue', 'AdminProductsController@addattributevalue');
			Route::post('/updateattributevalue', 'AdminProductsController@updateattributevalue');
			Route::post('/checkattributeassociate', 'AdminProductsController@checkattributeassociate');
			Route::post('/checkvalueassociate', 'AdminProductsController@checkvalueassociate');
			Route::post('/deletevalue', 'AdminProductsController@deletevalue');


			//manageAppLabel
			Route::get('/listingAppLabels', 'AdminAppLabelsController@listingAppLabels');
			Route::get('/addappkey', 'AdminAppLabelsController@addappkey');
			Route::post('/addNewAppLabel', 'AdminAppLabelsController@addNewAppLabel');
			Route::get('/editAppLabel/{id}', 'AdminAppLabelsController@editAppLabel');
			Route::post('/updateAppLabel/', 'AdminAppLabelsController@updateAppLabel');
			Route::get('/applabel', 'AdminAppLabelsController@manageAppLabel');


			//customers
			Route::get('/customers', 'AdminCustomersController@customers');
			Route::get('/addcustomers', 'AdminCustomersController@addcustomers');
			Route::post('/addnewcustomers', 'AdminCustomersController@addnewcustomers');


			//add adddresses against customers
			Route::get('/addaddress/{id}/', 'AdminCustomersController@addaddress');
			Route::post('/addNewCustomerAddress', 'AdminCustomersController@addNewCustomerAddress');
			Route::post('/editAddress', 'AdminCustomersController@editAddress');
			Route::post('/updateAddress', 'AdminCustomersController@updateAddress');
			Route::post('/deleteAddress', 'AdminCustomersController@deleteAddress');
			Route::post('/getZones', 'AdminCustomersController@getZones');
			//edit customer
			Route::get('/editcustomers/{id}', 'AdminCustomersController@editcustomers');
			Route::post('/updatecustomers', 'AdminCustomersController@updatecustomers');
			Route::post('/deletecustomers', 'AdminCustomersController@deletecustomers');

			//orders
			Route::get('/orders', 'AdminOrdersController@orders');		
			Route::get('/vieworder/{id}', 'AdminOrdersController@vieworder');
			Route::post('/updateOrder', 'AdminOrdersController@updateOrder');
			Route::post('/deleteOrder', 'AdminOrdersController@deleteOrder');
			
			//alert setting
			Route::get('/alertsetting', 'AdminSiteSettingController@alertSetting');
			Route::post('/updateAlertSetting', 'AdminSiteSettingController@updateAlertSetting');
			
			//generate application key
			Route::get('/generateKey', 'AdminSiteSettingController@generateKey');

			//countries
			Route::get('/countries', 'AdminTaxController@countries');
			Route::get('/addcountry', 'AdminTaxController@addcountry');
			Route::post('/addnewcountry', 'AdminTaxController@addnewcountry');
			Route::get('/editcountry/{id}', 'AdminTaxController@editcountry');
			Route::post('/updatecountry', 'AdminTaxController@updatecountry');
			Route::post('/deletecountry', 'AdminTaxController@deletecountry');

			//zones
			Route::get('/listingZones', 'AdminTaxController@listingZones');
			Route::get('/addZone', 'AdminTaxController@addZone');
			Route::post('/addNewZone', 'AdminTaxController@addNewZone');
			Route::get('/editZone/{id}', 'AdminTaxController@editZone');
			Route::post('/updateZone', 'AdminTaxController@updateZone');
			Route::post('/deleteZone', 'AdminTaxController@deleteZone');

			//tax class
			Route::get('/taxclass', 'AdminTaxController@taxclass');
			Route::get('/addtaxclass', 'AdminTaxController@addtaxclass');
			Route::post('/addnewtaxclass', 'AdminTaxController@addnewtaxclass');
			Route::get('/edittaxclass/{id}', 'AdminTaxController@edittaxclass');
			Route::post('/updatetaxclass', 'AdminTaxController@updatetaxclass');
			Route::post('/deletetaxclass', 'AdminTaxController@deletetaxclass');

			//tax rate
			Route::get('/taxrates', 'AdminTaxController@taxrates');
			Route::get('/addtaxrate', 'AdminTaxController@addtaxrate');
			Route::post('/addnewtaxrate', 'AdminTaxController@addnewtaxrate');
			Route::get('/edittaxrate/{id}', 'AdminTaxController@edittaxrate');
			Route::post('/updatetaxrate', 'AdminTaxController@updatetaxrate');
			Route::post('/deletetaxrate', 'AdminTaxController@deletetaxrate');

			//shipping setting
			Route::get('/shippingmethods', 'AdminShippingController@shippingmethods');
			Route::get('/upsShipping', 'AdminShippingController@upsShipping');
			Route::post('/updateupsshipping', 'AdminShippingController@updateupsshipping');
			Route::get('/flateRate', 'AdminShippingController@flateRate');
			Route::post('/updateflaterate', 'AdminShippingController@updateflaterate');
			Route::post('/defaultShippingMethod', 'AdminShippingController@defaultShippingMethod');
			Route::get('/shippingDetail/{table_name}', 'AdminShippingController@shippingDetail');
			Route::post('/updateShipping', 'AdminShippingController@updateShipping');
			//Payment setting
			Route::get('/paymentsetting', 'AdminPaymentController@paymentsetting');
			Route::post('/updatePaymentSetting', 'AdminPaymentController@updatePaymentSetting');

			//orders
			Route::get('/orderstatus', 'AdminSiteSettingController@orderstatus');
			Route::get('/addorderstatus', 'AdminSiteSettingController@addorderstatus');
			Route::post('/addNewOrderStatus', 'AdminSiteSettingController@addNewOrderStatus');
			Route::get('/editorderstatus/{id}', 'AdminSiteSettingController@editorderstatus');
			Route::post('/updateOrderStatus', 'AdminSiteSettingController@updateOrderStatus');
			Route::post('/deleteOrderStatus', 'AdminSiteSettingController@deleteOrderStatus');
			
			//units
			Route::get('/units', 'AdminSiteSettingController@units');
			Route::get('/addunit', 'AdminSiteSettingController@addunit');
			Route::post('/addnewunit', 'AdminSiteSettingController@addnewunit');
			Route::get('/editunit/{id}', 'AdminSiteSettingController@editunit');
			Route::post('/updateunit', 'AdminSiteSettingController@updateunit');
			Route::post('/deleteunit', 'AdminSiteSettingController@deleteunit');
			
			//setting page
			Route::get('/setting', 'AdminSiteSettingController@setting');
			Route::post('/updateSetting', 'AdminSiteSettingController@updateSetting');
			
			Route::get('/websettings', 'AdminSiteSettingController@webSettings');	
			Route::get('/themeSettings', 'AdminSiteSettingController@themeSettings');			
			Route::get('/appsettings', 'AdminSiteSettingController@appSettings');			
			Route::get('/admobSettings', 'AdminSiteSettingController@admobSettings');		
			Route::get('/facebooksettings', 'AdminSiteSettingController@facebookSettings');
			Route::get('/googlesettings', 'AdminSiteSettingController@googleSettings');	
			Route::get('/applicationapi', 'AdminSiteSettingController@applicationApi');	
			Route::get('/webthemes', 'AdminSiteSettingController@webThemes');
			Route::get('/seo', 'AdminSiteSettingController@seo');		
			Route::get('/customstyle', 'AdminSiteSettingController@customstyle');	
			Route::post('/updateWebTheme', 'AdminSiteSettingController@updateWebTheme');			
			
			//pushNotification
			Route::get('/pushnotification', 'AdminSiteSettingController@pushNotification');	
			
			//language setting
			Route::get('/getlanguages', 'AdminSiteSettingController@getlanguages');
			Route::get('/languages', 'AdminSiteSettingController@languages');
			Route::post('/defaultlanguage', 'AdminSiteSettingController@defaultlanguage');			
			Route::get('/addlanguages', 'AdminSiteSettingController@addlanguages');
			Route::post('/addnewlanguages', 'AdminSiteSettingController@addnewlanguages');
			Route::get('/editlanguages/{id}', 'AdminSiteSettingController@editlanguages');
			Route::post('/updatelanguages', 'AdminSiteSettingController@updatelanguages');
			Route::post('/deletelanguage', 'AdminSiteSettingController@deletelanguage');

			//banners
			Route::get('/banners', 'AdminBannersController@banners');
			Route::get('/addbanner', 'AdminBannersController@addbanner');
			Route::post('/addNewBanner', 'AdminBannersController@addNewBanner');
			Route::get('/editbanner/{id}', 'AdminBannersController@editbanner');
			Route::post('/updateBanner', 'AdminBannersController@updateBanner');
			Route::post('/deleteBanner/', 'AdminBannersController@deleteBanner');
			
			//sliders
			Route::get('/sliders', 'AdminSlidersController@sliders');
			Route::get('/addsliderimage', 'AdminSlidersController@addsliderimage');
			Route::post('/addNewSlide', 'AdminSlidersController@addNewSlide');
			Route::get('/editslide/{id}', 'AdminSlidersController@editslide');
			Route::post('/updateSlide', 'AdminSlidersController@updateSlide');
			Route::post('/deleteSlider/', 'AdminSlidersController@deleteSlider');

			//profile setting
			Route::get('/adminProfile', 'AdminController@adminProfile');
			Route::post('/updateProfile', 'AdminController@updateProfile');
			Route::post('/updateAdminPassword', 'AdminController@updateAdminPassword');

			//reports 
			Route::get('/statscustomers', 'AdminReportsController@statsCustomers');
			Route::get('/statsproductspurchased', 'AdminReportsController@statsProductsPurchased');
			Route::get('/statsproductsliked', 'AdminReportsController@statsProductsLiked');
			Route::get('/productsstock', 'AdminReportsController@productsStock');
			Route::post('/productSaleReport', 'AdminReportsController@productSaleReport');

			//Devices and send notification
			Route::get('/devices', 'AdminNotificationController@devices');
			Route::get('/viewdevices/{id}', 'AdminNotificationController@viewdevices');
			Route::post('/notifyUser/', 'AdminNotificationController@notifyUser');
			Route::get('/notifications/', 'AdminNotificationController@notifications');
			Route::post('/sendNotifications/', 'AdminNotificationController@sendNotifications');
			Route::post('/customerNotification/', 'AdminNotificationController@customerNotification');
			Route::post('/singleUserNotification/', 'AdminNotificationController@singleUserNotification');
			Route::post('/deletedevice/', 'AdminNotificationController@deletedevice');

			//coupons
			Route::get('/coupons', 'AdminCouponsController@coupons');
			Route::get('/addcoupons', 'AdminCouponsController@addcoupons');
			Route::post('/addnewcoupons', 'AdminCouponsController@addnewcoupons');
			Route::get('/editcoupons/{id}', 'AdminCouponsController@editcoupons');
			Route::post('/updatecoupons', 'AdminCouponsController@updatecoupons');
			Route::post('/deletecoupon', 'AdminCouponsController@deletecoupon');
			Route::post('/couponProducts', 'AdminCouponsController@couponProducts');

			//news categories
			Route::get('/newscategories', 'AdminNewsCategoriesController@newscategories');
			Route::get('/addnewscategory', 'AdminNewsCategoriesController@addnewscategory');
			Route::post('/addnewsnewcategory', 'AdminNewsCategoriesController@addnewsnewcategory');
			Route::get('/editnewscategory/{id}', 'AdminNewsCategoriesController@editnewscategory');
			Route::post('/updatenewscategory', 'AdminNewsCategoriesController@updatenewscategory');
			Route::post('/deletenewscategory', 'AdminNewsCategoriesController@deletenewscategory');

			//news
			Route::get('/news', 'AdminNewsController@news');
			Route::get('/addnews', 'AdminNewsController@addnews');
			Route::post('/addnewnews', 'AdminNewsController@addnewnews');
			Route::get('/editnews/{id}', 'AdminNewsController@editnews');
			Route::post('/updatenews', 'AdminNewsController@updatenews');
			Route::post('/deletenews', 'AdminNewsController@deletenews');

			//app pages controller
			Route::get('/pages', ' PagesController@pages');
			Route::get('/addpage', 'AdminPagesController@addpage');
			Route::post('/addnewpage', 'AdminPagesController@addnewpage');
			Route::get('/editpage/{id}', 'AdminPagesController@editpage');
			Route::post('/updatepage', 'AdminPagesController@updatepage');
			Route::get('/pageStatus', 'AdminPagesController@pageStatus');
			
			//site pages controller
			Route::get('/webpages', 'AdminPagesController@webpages');
			Route::get('/addwebpage', 'AdminPagesController@addwebpage');
			Route::post('/addnewwebpage', 'AdminPagesController@addnewwebpage');
			Route::get('/editwebpage/{id}', 'AdminPagesController@editwebpage');
			Route::post('/updatewebpage', 'AdminPagesController@updatewebpage');
			Route::get('/pageWebStatus', 'AdminPagesController@pageWebStatus');
			

			

		});

		
		//log in
		Route::get('/login', 'AdminController@login');
		Route::post('/checkLogin', 'AdminController@checkLogin');

		//log out
		Route::get('/logout', 'AdminController@logout');
});

});





/*
|--------------------------------------------------------------------------
| Vendor controller Routes
|--------------------------------------------------------------------------
|
| This section contains all vendor Routes
| 
|
*/

Route::group(['prefix' => 'vendor'], function () {
	
	Route::group(['namespace' => 'Vendor'], function () {

		Route::group(['middleware' => 'vendor'], function () {
			Route::get('/dashboard/{reportBase}', 'VendorController@dashboard');
			Route::get('/post', 'VendorController@myPost');
			//show admin personal info record
			Route::get('/vendorInfo', 'VendorController@vendorInfo');

			/*
			|--------------------------------------------------------------------------
			| categories/Product Controller Routes
			|--------------------------------------------------------------------------
			|
			| This section contains categories/Product Controller Routes
			| 
			|
			*/
			//main listingManufacturer
			Route::get('/manufacturers', 'AdminManufacturerController@manufacturers');
			Route::get('/addmanufacturer', 'AdminManufacturerController@addmanufacturer');
			Route::post('/addnewmanufacturer', 'AdminManufacturerController@addnewmanufacturer');
			Route::get('/editmanufacturer/{id}', 'AdminManufacturerController@editmanufacturer');
			Route::post('/updatemanufacturer', 'AdminManufacturerController@updatemanufacturer');
			Route::post('/deletemanufacturer', 'AdminManufacturerController@deletemanufacturer');

			//main categories
			Route::get('/categories', 'AdminCategoriesController@categories');
			Route::get('/addcategory', 'AdminCategoriesController@addcategory');
			Route::post('/addnewcategory', 'AdminCategoriesController@addnewcategory');
			Route::get('/editcategory/{id}', 'AdminCategoriesController@editcategory');
			Route::post('/updatecategory', 'AdminCategoriesController@updatecategory');
			Route::get('/deletecategory/{id}', 'AdminCategoriesController@deletecategory');

			//sub categories
			Route::get('/subcategories', 'AdminCategoriesController@subcategories');
			Route::get('/addsubcategory', 'AdminCategoriesController@addsubcategory');
			Route::post('/addnewsubcategory', 'AdminCategoriesController@addnewsubcategory');
			Route::get('/editsubcategory/{id}', 'AdminCategoriesController@editsubcategory');
			Route::post('/updatesubcategory', 'AdminCategoriesController@updatesubcategory');
			Route::get('/deletesubcategory/{id}', 'AdminCategoriesController@deletesubcategory');
			
			Route::post('/getajaxcategories', 'AdminCategoriesController@getajaxcategories');

			//products
			Route::get('/products', 'VendorProductsController@products');
			Route::get('/addproduct', 'VendorProductsController@addproduct');
			Route::post('/addnewproduct', 'VendorProductsController@addnewproduct');

			//add attribute against newly added product
			Route::get('/addproductattribute/{id}/', 'VendorProductsController@addproductattribute');
			Route::get('/addproductimages/{id}/', 'VendorProductsController@addproductimages');
			Route::post('/addnewdefaultattribute', 'VendorProductsController@addnewdefaultattribute');
			Route::post('/addnewproductattribute', 'VendorProductsController@addnewproductattribute');
			Route::post('/updateproductattribute', 'VendorProductsController@updateproductattribute');
			Route::post('/updatedefaultattribute', 'VendorProductsController@updatedefaultattribute');
			Route::post('/deleteproduct', 'VendorProductsController@deleteproduct');
			Route::post('/deleteproductattribute', 'VendorProductsController@deleteproductattribute');
			Route::post('/addnewproductimage', 'VendorProductsController@addnewproductimage');
			Route::post('/deletedefaultattribute', 'VendorProductsController@deletedefaultattribute');
			Route::post('editproductattribute', 'VendorProductsController@editproductattribute');
			Route::post('editdefaultattribute', 'VendorProductsController@editdefaultattribute');
			Route::post('addnewproductimagemodal', 'VendorProductsController@addnewproductimagemodal');
			Route::post('deleteproductattributemodal', 'VendorProductsController@deleteproductattributemodal');
			Route::post('deletedefaultattributemodal', 'VendorProductsController@deletedefaultattributemodal');

			//product attribute
			Route::post('/addnewproductimage', 'VendorProductsController@addnewproductimage');
			Route::post('editproductimage', 'VendorProductsController@editproductimage');
			Route::post('/updateproductimage', 'VendorProductsController@updateproductimage');
			Route::post('/deleteproductimagemodal', 'VendorProductsController@deleteproductimagemodal');
			Route::post('/deleteproductimage', 'VendorProductsController@deleteproductimage');
			Route::get('/editproduct/{id}', 'VendorProductsController@editproduct');
			Route::post('/updateproduct', 'VendorProductsController@updateproduct');	
			Route::post('/getOptions', 'VendorProductsController@getOptions');	
			Route::post('/getOptionsValue', 'VendorProductsController@getOptionsValue');	


			//Attribute
			Route::get('/attributes', 'VendorProductsController@attributes');
			Route::get('/addattributes', 'VendorProductsController@addattributes');
			Route::post('/addnewattributes', 'VendorProductsController@addnewattributes');
			Route::get('/editattributes/{id}/{language_id}', 'VendorProductsController@editattributes');
			Route::post('/updateattributes/', 'VendorProductsController@updateattributes');
			Route::post('/deleteattribute', 'VendorProductsController@deleteattribute');
			Route::post('/addattributevalue', 'VendorProductsController@addattributevalue');
			Route::post('/updateattributevalue', 'VendorProductsController@updateattributevalue');
			Route::post('/checkattributeassociate', 'VendorProductsController@checkattributeassociate');
			Route::post('/checkvalueassociate', 'VendorProductsController@checkvalueassociate');
			Route::post('/deletevalue', 'VendorProductsController@deletevalue');


			//manageAppLabel
			Route::get('/listingAppLabels', 'VendorAppLabelsController@listingAppLabels');
			Route::get('/addappkey', 'VendorAppLabelsController@addappkey');
			Route::post('/addNewAppLabel', 'VendorAppLabelsController@addNewAppLabel');
			Route::get('/editAppLabel/{id}', 'VendorAppLabelsController@editAppLabel');
			Route::post('/updateAppLabel/', 'VendorAppLabelsController@updateAppLabel');
			Route::get('/applabel', 'VendorAppLabelsController@manageAppLabel');


			//customers
			Route::get('/customers', 'VendorCustomersController@customers');
			Route::get('/addcustomers', 'VendorCustomersController@addcustomers');
			Route::post('/addnewcustomers', 'VendorCustomersController@addnewcustomers');


			//add adddresses against customers
			Route::get('/addaddress/{id}/', 'VendorCustomersController@addaddress');
			Route::post('/addNewCustomerAddress', 'VendorCustomersController@addNewCustomerAddress');
			Route::post('/editAddress', 'VendorCustomersController@editAddress');
			Route::post('/updateAddress', 'VendorCustomersController@updateAddress');
			Route::post('/deleteAddress', 'VendorCustomersController@deleteAddress');
			Route::post('/getZones', 'VendorCustomersController@getZones');
			//edit customer
			Route::get('/editcustomers/{id}', 'VendorCustomersController@editcustomers');
			Route::post('/updatecustomers', 'VendorCustomersController@updatecustomers');
			Route::post('/deletecustomers', 'VendorCustomersController@deletecustomers');

			//orders
			Route::get('/orders', 'VendorOrdersController@orders');		
			Route::get('/vieworder/{id}', 'VendorOrdersController@vieworder');
			Route::post('/updateOrder', 'VendorOrdersController@updateOrder');
			Route::post('/deleteOrder', 'VendorOrdersController@deleteOrder');
			
			//alert setting
			Route::get('/alertsetting', 'VendorSiteSettingController@alertSetting');
			Route::post('/updateAlertSetting', 'VendorSiteSettingController@updateAlertSetting');
			
			//generate application key
			Route::get('/generateKey', 'VendorSiteSettingController@generateKey');

			//countries
			Route::get('/countries', 'VendorTaxController@countries');
			Route::get('/addcountry', 'VendorTaxController@addcountry');
			Route::post('/addnewcountry', 'VendorTaxController@addnewcountry');
			Route::get('/editcountry/{id}', 'VendorTaxController@editcountry');
			Route::post('/updatecountry', 'VendorTaxController@updatecountry');
			Route::post('/deletecountry', 'VendorTaxController@deletecountry');

			//zones
			Route::get('/listingZones', 'VendorTaxController@listingZones');
			Route::get('/addZone', 'VendorTaxController@addZone');
			Route::post('/addNewZone', 'VendorTaxController@addNewZone');
			Route::get('/editZone/{id}', 'VendorTaxController@editZone');
			Route::post('/updateZone', 'VendorTaxController@updateZone');
			Route::post('/deleteZone', 'VendorTaxController@deleteZone');

			//tax class
			Route::get('/taxclass', 'VendorTaxController@taxclass');
			Route::get('/addtaxclass', 'VendorTaxController@addtaxclass');
			Route::post('/addnewtaxclass', 'VendorTaxController@addnewtaxclass');
			Route::get('/edittaxclass/{id}', 'VendorTaxController@edittaxclass');
			Route::post('/updatetaxclass', 'VendorTaxController@updatetaxclass');
			Route::post('/deletetaxclass', 'VendorTaxController@deletetaxclass');

			//tax rate
			Route::get('/taxrates', 'VendorTaxController@taxrates');
			Route::get('/addtaxrate', 'VendorTaxController@addtaxrate');
			Route::post('/addnewtaxrate', 'VendorTaxController@addnewtaxrate');
			Route::get('/edittaxrate/{id}', 'VendorTaxController@edittaxrate');
			Route::post('/updatetaxrate', 'VendorTaxController@updatetaxrate');
			Route::post('/deletetaxrate', 'VendorTaxController@deletetaxrate');

			//shipping setting
			Route::get('/shippingmethods', 'VendorShippingController@shippingmethods');
			Route::get('/upsShipping', 'VendorShippingController@upsShipping');
			Route::post('/updateupsshipping', 'VendorShippingController@updateupsshipping');
			Route::get('/flateRate', 'VendorShippingController@flateRate');
			Route::post('/updateflaterate', 'VendorShippingController@updateflaterate');
			Route::post('/defaultShippingMethod', 'VendorShippingController@defaultShippingMethod');
			Route::get('/shippingDetail/{table_name}', 'VendorShippingController@shippingDetail');
			Route::post('/updateShipping', 'VendorShippingController@updateShipping');
			//Payment setting
			Route::get('/paymentsetting', 'VendorPaymentController@paymentsetting');
			Route::post('/updatePaymentSetting', 'VendorPaymentController@updatePaymentSetting');

			//orders
			Route::get('/orderstatus', 'VendorSiteSettingController@orderstatus');
			Route::get('/addorderstatus', 'VendorSiteSettingController@addorderstatus');
			Route::post('/addNewOrderStatus', 'VendorSiteSettingController@addNewOrderStatus');
			Route::get('/editorderstatus/{id}', 'VendorSiteSettingController@editorderstatus');
			Route::post('/updateOrderStatus', 'VendorSiteSettingController@updateOrderStatus');
			Route::post('/deleteOrderStatus', 'VendorSiteSettingController@deleteOrderStatus');
			
			//units
			Route::get('/units', 'VendorSiteSettingController@units');
			Route::get('/addunit', 'VendorSiteSettingController@addunit');
			Route::post('/addnewunit', 'VendorSiteSettingController@addnewunit');
			Route::get('/editunit/{id}', 'VendorSiteSettingController@editunit');
			Route::post('/updateunit', 'VendorSiteSettingController@updateunit');
			Route::post('/deleteunit', 'VendorSiteSettingController@deleteunit');
			
			//setting page
			Route::get('/setting', 'VendorSiteSettingController@setting');
			Route::post('/updateSetting', 'VendorSiteSettingController@updateSetting');
			
			Route::get('/websettings', 'VendorSiteSettingController@webSettings');	
			Route::get('/themeSettings', 'VendorSiteSettingController@themeSettings');			
			Route::get('/appsettings', 'VendorSiteSettingController@appSettings');			
			Route::get('/admobSettings', 'VendorSiteSettingController@admobSettings');		
			Route::get('/facebooksettings', 'VendorSiteSettingController@facebookSettings');
			Route::get('/googlesettings', 'VendorSiteSettingController@googleSettings');	
			Route::get('/applicationapi', 'VendorSiteSettingController@applicationApi');	
			Route::get('/webthemes', 'VendorSiteSettingController@webThemes');
			Route::get('/seo', 'VendorSiteSettingController@seo');		
			Route::get('/customstyle', 'VendorSiteSettingController@customstyle');	
			Route::post('/updateWebTheme', 'VendorSiteSettingController@updateWebTheme');			
			
			//pushNotification
			Route::get('/pushnotification', 'VendorSiteSettingController@pushNotification');	
			
			//language setting
			Route::get('/getlanguages', 'VendorSiteSettingController@getlanguages');
			Route::get('/languages', 'VendorSiteSettingController@languages');
			Route::post('/defaultlanguage', 'VendorSiteSettingController@defaultlanguage');			
			Route::get('/addlanguages', 'VendorSiteSettingController@addlanguages');
			Route::post('/addnewlanguages', 'VendorSiteSettingController@addnewlanguages');
			Route::get('/editlanguages/{id}', 'VendorSiteSettingController@editlanguages');
			Route::post('/updatelanguages', 'VendorSiteSettingController@updatelanguages');
			Route::post('/deletelanguage', 'VendorSiteSettingController@deletelanguage');

			//banners
			Route::get('/banners', 'VendorBannersController@banners');
			Route::get('/addbanner', 'VendorBannersController@addbanner');
			Route::post('/addNewBanner', 'VendorBannersController@addNewBanner');
			Route::get('/editbanner/{id}', 'VendorBannersController@editbanner');
			Route::post('/updateBanner', 'VendorBannersController@updateBanner');
			Route::post('/deleteBanner/', 'VendorBannersController@deleteBanner');
			
			//sliders
			Route::get('/sliders', 'VendorSlidersController@sliders');
			Route::get('/addsliderimage', 'VendorSlidersController@addsliderimage');
			Route::post('/addNewSlide', 'VendorSlidersController@addNewSlide');
			Route::get('/editslide/{id}', 'VendorSlidersController@editslide');
			Route::post('/updateSlide', 'VendorSlidersController@updateSlide');
			Route::post('/deleteSlider/', 'VendorSlidersController@deleteSlider');

			//profile setting
			Route::get('/vendorProfile', 'VendorController@vendorProfile');
			Route::post('/updateProfile', 'AdminController@updateProfile');
			Route::post('/updateAdminPassword', 'AdminController@updateAdminPassword');

			//reports 
			Route::get('/statscustomers', 'AdminReportsController@statsCustomers');
			Route::get('/statsproductspurchased', 'AdminReportsController@statsProductsPurchased');
			Route::get('/statsproductsliked', 'AdminReportsController@statsProductsLiked');
			Route::get('/productsstock', 'AdminReportsController@productsStock');
			Route::post('/productSaleReport', 'AdminReportsController@productSaleReport');

			//Devices and send notification
			Route::get('/devices', 'AdminNotificationController@devices');
			Route::get('/viewdevices/{id}', 'AdminNotificationController@viewdevices');
			Route::post('/notifyUser/', 'AdminNotificationController@notifyUser');
			Route::get('/notifications/', 'AdminNotificationController@notifications');
			Route::post('/sendNotifications/', 'AdminNotificationController@sendNotifications');
			Route::post('/customerNotification/', 'AdminNotificationController@customerNotification');
			Route::post('/singleUserNotification/', 'AdminNotificationController@singleUserNotification');
			Route::post('/deletedevice/', 'AdminNotificationController@deletedevice');

			//coupons
			Route::get('/coupons', 'AdminCouponsController@coupons');
			Route::get('/addcoupons', 'AdminCouponsController@addcoupons');
			Route::post('/addnewcoupons', 'AdminCouponsController@addnewcoupons');
			Route::get('/editcoupons/{id}', 'AdminCouponsController@editcoupons');
			Route::post('/updatecoupons', 'AdminCouponsController@updatecoupons');
			Route::post('/deletecoupon', 'AdminCouponsController@deletecoupon');
			Route::post('/couponProducts', 'AdminCouponsController@couponProducts');

			//news categories
			Route::get('/newscategories', 'AdminNewsCategoriesController@newscategories');
			Route::get('/addnewscategory', 'AdminNewsCategoriesController@addnewscategory');
			Route::post('/addnewsnewcategory', 'AdminNewsCategoriesController@addnewsnewcategory');
			Route::get('/editnewscategory/{id}', 'AdminNewsCategoriesController@editnewscategory');
			Route::post('/updatenewscategory', 'AdminNewsCategoriesController@updatenewscategory');
			Route::post('/deletenewscategory', 'AdminNewsCategoriesController@deletenewscategory');

			//news
			Route::get('/news', 'AdminNewsController@news');
			Route::get('/addnews', 'AdminNewsController@addnews');
			Route::post('/addnewnews', 'AdminNewsController@addnewnews');
			Route::get('/editnews/{id}', 'AdminNewsController@editnews');
			Route::post('/updatenews', 'AdminNewsController@updatenews');
			Route::post('/deletenews', 'AdminNewsController@deletenews');

			//app pages controller
			Route::get('/pages', ' PagesController@pages');
			Route::get('/addpage', 'AdminPagesController@addpage');
			Route::post('/addnewpage', 'AdminPagesController@addnewpage');
			Route::get('/editpage/{id}', 'AdminPagesController@editpage');
			Route::post('/updatepage', 'AdminPagesController@updatepage');
			Route::get('/pageStatus', 'AdminPagesController@pageStatus');
			
			//site pages controller
			Route::get('/webpages', 'AdminPagesController@webpages');
			Route::get('/addwebpage', 'AdminPagesController@addwebpage');
			Route::post('/addnewwebpage', 'AdminPagesController@addnewwebpage');
			Route::get('/editwebpage/{id}', 'AdminPagesController@editwebpage');
			Route::post('/updatewebpage', 'AdminPagesController@updatewebpage');
			Route::get('/pageWebStatus', 'AdminPagesController@pageWebStatus');
			

			

		});

		
		//log in
		Route::get('/login', 'AdminController@login');
		Route::post('/checkLogin', 'AdminController@checkLogin');

		//log out
		Route::get('/logout', 'AdminController@logout');
	});

});


/*
|--------------------------------------------------------------------------
| front-end Controller Routes
|--------------------------------------------------------------------------
|
| This section contains all Routes of front-end content
| 
|
*/

/********* setting themes dynamically *********/
// default setting
$routeSetting = DB::table('settings')->get();
Theme::set($routeSetting[48]->value);

Route::get('welcome/{locale}', function ($locale) {
    App::setLocale($locale);
	print $locale;
    //
});

Route::group(['namespace' => 'Web'], function () {	
	
//language route
Route::post('/language-chooser', 'WebSettingController@changeLanguage');
Route::post('/language/', array(
	//'before' => 'csrf',
	'as' => 'language-chooser',
	'uses' => 'WebSettingController@changeLanguage'
	));
		
	Route::get('/setStyle', 'DefaultController@setStyle');
	Route::get('/settheme', 'DefaultController@settheme');
	Route::get('/page', 'DefaultController@page');
	Route::post('/subscribeNotification/', 'CustomersController@subscribeNotification');
	
	Route::get('/', 'DefaultController@index');
	Route::get('/index', 'DefaultController@index');
	
	Route::get('/contact-us', 'DefaultController@ContactUs');
	Route::post('/processContactUs', 'DefaultController@processContactUs');
	
	//news section
	Route::get('/news', 'NewsController@news');
	Route::get('/news-detail/{slug}', 'NewsController@newsDetail');
	Route::post('/loadMoreNews', 'NewsController@loadMoreNews');	
	
	
	Route::get('/clear-cache', function() {
		$exitCode = Artisan::call('cache:clear');
	});
	
	/*
	|--------------------------------------------------------------------------
	| categories / products Controller Routes
	|--------------------------------------------------------------------------
	|
	| This section contains all Routes of categories page, products/shop page, product detail. 
	| 
	|
	*/
	
	Route::get('/shop', 'ProductsController@shop');
	Route::post('/shop', 'ProductsController@shop');
	Route::get('/product-detail/{slug}', 'ProductsController@productDetail');
	Route::post('/filterProducts', 'ProductsController@filterProducts');
	
	
	
	/*
	|--------------------------------------------------------------------------
	| Cart Controller Routes
	|--------------------------------------------------------------------------
	|
	| This section contains customer cart products
	| 
	*/

	Route::get('/getCart', 'DataController@getCart');
	Route::post('/addToCart', 'CartController@addToCart');
	Route::post('/updatesinglecart', 'CartController@updatesinglecart');
	Route::get('/cartButton', 'CartController@cartButton');
	
	Route::get('/viewcart', 'CartController@viewcart');
	Route::get('/editcart', 'CartController@editcart');
	
	Route::post('/updateCart', 'CartController@updateCart');
	Route::get('/deleteCart', 'CartController@deleteCart');
	Route::post('/apply_coupon', 'CartController@apply_coupon');
	Route::get('/removeCoupon/{id}', 'CartController@removeCoupon');
	
	
	
	/*
	|--------------------------------------------------------------------------
	| customer registrations Controller Routes
	|--------------------------------------------------------------------------
	|
	| This section contains all Routes of signup page, login page, forgot password 
	| facebook login , google login, shipping address etc.
	|
	*/
	
	Route::get('/login', 'CustomersController@login');
	Route::get('/signup', 'CustomersController@signup');
	Route::post('/process-login', 'CustomersController@processLogin');
	Route::get('/logout', 'CustomersController@logout');
	Route::post('/signupProcess', 'CustomersController@signupProcess');
	Route::get('/forgotPassword', 'CustomersController@forgotPassword');
	Route::get('/recoverPassword', 'CustomersController@recoverPassword');
	Route::post('/processPassword', 'CustomersController@processPassword');

	//Vendor
	Route::get('/vendor-login', 'VendorsController@login');
	Route::get('/vendor-signup', 'VendorsController@signup');
	Route::get('/vendor-profile/{id}', 'VendorsController@profile');
	Route::get('/logout', 'CustomersController@logout');
	Route::post('/vendor-process-login', 'VendorsController@processLogin');
	Route::post('/vendorSignupProcess', 'VendorsController@signupProcess');
	
	Route::get('login/{social}', 'CustomersController@socialLogin');
	Route::get('login/{social}/callback', 'CustomersController@handleSocialLoginCallback');
	Route::post('/commentsOrder', 'OrdersController@commentsOrder');
	
	//zones
	Route::post('/ajaxZones', 'ShippingAddressController@ajaxZones');
	
	//likeMyProduct
	Route::post('likeMyProduct', 'CustomersController@likeMyProduct');
	
	/*
	|--------------------------------------------------------------------------
	| WEbiste auth path Controller Routes
	|--------------------------------------------------------------------------
	|
	| This section contains all Routes of After login 
	| 
	|
	*/
		
	Route::group(['middleware' => 'Customer'], function () {						
		Route::get('/wishlist', 'CustomersController@wishlist');
		Route::post('/loadMoreWishlist', 'CustomersController@loadMoreWishlist');
		Route::get('/profile', 'CustomersController@profile');
		Route::post('/updateMyProfile', 'CustomersController@updateMyProfile');
		Route::post('/updateMyPassword', 'CustomersController@updateMyPassword');		
		
		Route::get('/shipping-address', 'ShippingAddressController@shippingAddress');
		Route::post('/addMyAddress', 'ShippingAddressController@addMyAddress');
		Route::post('/myDefaultAddress', 'ShippingAddressController@myDefaultAddress');		
		
		Route::post('/update-address', 'ShippingAddressController@updateAddress');
		Route::post('/delete-address', 'ShippingAddressController@deleteAddress');
		
		Route::get('/checkout', 'OrdersController@checkout');	
		Route::post('/checkout_shipping_address', 'OrdersController@checkout_shipping_address');
		Route::post('/checkout_billing_address', 'OrdersController@checkout_billing_address');
		Route::post('/checkout_payment_method', 'OrdersController@checkout_payment_method');
		Route::post('/paymentComponent', 'OrdersController@paymentComponent');	
		Route::post('/place_order', 'OrdersController@place_order');	
		Route::get('/orders', 'OrdersController@orders');	
		Route::post('/myorders', 'OrdersController@myorders');	
		Route::get('/stripeForm', 'OrdersController@stripeForm');	
		Route::get('/view-order/{id}', 'OrdersController@viewOrder');
	});
});
